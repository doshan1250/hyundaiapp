//
//  LocationPoint.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationPoint : NSObject
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lng;
-(void)setLat:(NSString *)lat_string andLng:(NSString *)lng_string;
@end
