//
//  NSArray+Plus.h
//  Wyeth
//
//  Created by Wei-Po Chuang on 12/4/22.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Plus)

- (NSInteger)binarySearch:(id)searchItem;

@end
