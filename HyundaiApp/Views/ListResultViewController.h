//
//  ListResultViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/14.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol ListResultDelegate <NSObject>

-(void)clickedItem:(CLLocationCoordinate2D)dest;

@end

//提供加油與停車兩個頁面的list列表繼承使用
@interface ListResultViewController : UIViewController{
    NSArray *_areaDatas;
    UIImage *_image;
    CLLocation *_mCLLocation;
}
@property(nonatomic ,strong) id<ListResultDelegate> delegate;
-(void)setAreaDatas:(NSArray *)areaDatas andImage:(UIImage *)image;
-(void)setLocation:(CLLocation *)mCLLocation;
-(void)reloadData;
@end
