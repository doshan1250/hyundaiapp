//
//  SOPViewCell.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/5.
//  Copyright (c) 2013年 Corma. All rights reserved.
//

#import "SOPViewCell.h"

@implementation SOPViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
