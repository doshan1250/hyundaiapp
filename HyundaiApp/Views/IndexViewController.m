//
//  IndexViewController.m
//  Ma2Club
//
//  Created by Webber Chuang on 13/11/7.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "IndexViewController.h"
#import "CubeButton.h"
#import "SettingController.h"
#import "NewsListController.h"
#import "ParkingPlaceController.h"
#import "GasStationController.h"
#import "CarInfoController.h"
#import "MaintenanceController.h"
#import "SOPController.h"
#import "ContactController.h"
#import "ServiceController.h"
#import "FansController.h"
#import "CardController.h"
#import "LoginTools.h"
#import "ProgressViewController.h"
#import "FactoryIdParser.h"
#import "NewsDetailController.h"
#import "BuyCarController.h"

#import "QuarterlyViewController.h"
#import "VideoViewController.h"

#import "Reservation2View.h"
#import "Reservation1View.h"

#define LOGOUT_MSG      @"確定要登出嗎？"

@interface IndexViewController () <CubeButtonDelegate,ProgressDelegate,UIAlertViewDelegate,LoginStatus,UIScrollViewDelegate>
{
    IBOutlet UIView *_btnContainer;
    NSMutableArray *_arrBtn;
    LoginTools *mLoginTools;
    UIView *view;
    int mProgressCount;
    ProgressViewController *mProgressViewController;
    UIBarButtonItem *navSpace;
    
    NSArray *scrollDataArray;
    
    NSMutableArray *scrollViewContents;
    
    NSInteger oldPage;
    
    UIView *lastView, *firstView;
    UIView *c_news_view;
    NSArray *newsArray;
    NavButton *btn2;
    FactoryIdParser *mFacParser;
    //作為推播通知圖示的參考位置
    CGRect newsViewFrame;
}

@property (strong, nonatomic) IBOutlet UILabel *apnsCountView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_news;
@property (strong, nonatomic) IBOutlet UIImageView *img_left;
@property (strong, nonatomic) IBOutlet UIImageView *img_right;

@end

@implementation IndexViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrBtn = [[NSMutableArray alloc] initWithCapacity:9];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    navSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    navSpace.width = NAV_SPACING;
    
    // 給個left按鈕覆蓋過去
    UIView *nav_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 220, 44)];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(7, 12, 213, 19)];
    [imageView setImage:[UIImage imageNamed: @"new-logo.png"]];
    [nav_view addSubview:imageView];
    self.navigationItem.leftBarButtonItems = @[navSpace, [[UIBarButtonItem alloc] initWithCustomView:nav_view]];
    
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady:"];
    mLoginTools=[LoginTools new];
    mProgressCount=0;
    if([mLoginTools shouldLogin]){
        mProgressCount=1;
        //登入的時候要獲得下次的保修記錄
    }else{
        //如果不需要登入 則呼叫API獲取最新保養時間
        [currentApp.dataMgr fireRequest:Request_UserLogin postData:[NSMutableDictionary dictionaryWithObjectsAndKeys:  [mLoginTools getCarId],@"cid",[mLoginTools getUserId] ,@"pid", nil]];
    }
    
    [self buildBtn];
    
    mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
    [mProgressViewController setDelegate:self];
    mProgressViewController.view.frame=currentApp.window.frame;
    [currentApp.window addSubview:mProgressViewController.view];
    
    [currentApp.dataMgr fireRequest:Request_getfans postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: @"",@"", nil]];
    
    // 拉活動訊息資料
//    [currentApp.dataMgr fireRequest:Request_News postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: @"10",@"limit", nil]];
    
    //
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"10",@"limit", nil];
    NSString *srcUrl = [NSString stringWithFormat:API_HOST, @"GetNews2.ashx"];
    [currentApp.dataEngine getDataFromUrl:srcUrl params:params isPost:YES onCompletion:^(NSString *response) {
        
        NSLog(@"%@", response);
        newsArray = [Utilities convertJSONStringToArray:response];
        currentApp.newsArray = newsArray;
        [self setAPNsView:newsViewFrame];
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
        
    } onError:^(NSError *error) {
        
        NSLog(@"ERROR: News");
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
    }];
    
    [currentApp.dataMgr fireRequest:Request_ParkingArea postData:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"", @"", nil]];
    
    [currentApp.dataMgr fireRequest:Request_GetFixPlace postData:nil];
    mFacParser=[FactoryIdParser new];
    
    //for 4-inch screen resize
    if(currentApp.window.frame.size.height>500){
        //4 inch
        CGRect frame=self.img_left.frame;
        [self.img_left setFrame:CGRectMake(frame.origin.x, 80, frame.size.width, frame.size.height)];
        frame=self.img_right.frame;
        [self.img_right setFrame:CGRectMake(frame.origin.x, 80, frame.size.width, frame.size.height)];
    }//end
    
    [self.img_left setHidden:YES];
    [self.img_right setHidden:YES];
    
    
    //Reservation2View *view11 = [[[NSBundle mainBundle] loadNibNamed:@"Reservation2View" owner:nil options:nil] objectAtIndex:0];

    
    //Reservation1View *view11 = [[[NSBundle mainBundle] loadNibNamed:@"Reservation1View" owner:nil options:nil] objectAtIndex:0];
    
    //[self.view addSubview:view11];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [currentApp.dataMgr delObserverData:self];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self setAPNsView:newsViewFrame];
    [super viewDidAppear:animated];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    app.reveal.recognizesPanningOnFrontView = NO;
    
    self.navigationItem.rightBarButtonItems=nil;
    btn2 = [NavButton buttonWithType:UIButtonTypeCustom];
    [btn2 setFrame:CGRectMake(0, 0, 44, 44)];
    if(![mLoginTools shouldLogin]){
        [btn2 setImage:[UIImage imageNamed:@"nav_setting_logout.png"] forState:UIControlStateNormal];
        [btn2 setBackgroundColor:[UIColor clearColor]];
        [btn2 addTarget:self action:@selector(clickNavSettingLogout:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [btn2 setImage:[UIImage imageNamed:@"nav_setting.png"] forState:UIControlStateNormal];
        [btn2 addTarget:self action:@selector(clickNavSetting:) forControlEvents:UIControlEventTouchUpInside];
    }
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:btn2], navSpace];
}

-(void)getDataFail:(NSInteger )errorMsg{
    
}

#pragma mark - Data Notify
-(void)checkAndDismissProgress{
    if([mProgressViewController.view superview]&&mProgressCount>=4){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
        [self checkAPNsPage];
    }//enf if
}

#pragma mark - ProgressDelegate
-(void)timeout{
    if([mProgressViewController.view superview]){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
        [self checkAPNsPage];
    }
}

-(void)checkAPNsPage{
    if(currentApp.detaildataFromAPNs && currentApp.newsArray){
        NewsDetailController *newsDetail = [[NewsDetailController alloc]initWithNibName:@"NewsDetailController" bundle:nil];
        NSDictionary *data=nil;
        
        for(NSDictionary *obj in currentApp.newsArray){
            if([currentApp.detaildataFromAPNs isEqualToString:[NSString stringWithFormat:@"%@",[obj objectForKey:@"sno"]]]){
                data=[obj copy];
                break;
            }
        }
        
        [newsDetail setNewsData:data atIndex:-1 withList:nil];
        [currentApp.nav pushViewController:newsDetail animated:NO];
        newsDetail = nil;
        currentApp.detaildataFromAPNs=nil;
    }
}


/**注意這裡在之前要檢查有沒有錯誤的JSON格式*/
#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_News)
    {
        newsArray = [notify objectForKey:API_NotifyData];
        //        NSLog(@"newsArray: %@",newsArray);
        currentApp.newsArray=newsArray;
        [self setAPNsView:newsViewFrame];
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
        
    }else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_UserLogin){
        NSDictionary *dic = [notify objectForKey:API_NotifyData];
        NSString *mString=[dic objectForKey:@"ERROR"];
        if((!mString||[mString isEqualToString:@""])&&(![mLoginTools getUserData]||(![[dic objectForKey:@"NextServiceDate"] isEqualToString:[[mLoginTools getUserData] objectForKey:@"NextServiceDate"]])||(![[dic objectForKey:@"cEngNo"] isEqualToString:[[mLoginTools getUserData] objectForKey:@"cEngNo"]])) ){
            //如果回傳ＯＫ 而且回傳的NextServiceDate 與上次不相符合 或是換了一個人登入，代表需要添加行事曆
            [mLoginTools updateUserData:[dic copy]];
        }else{
            // 登入失敗
        }
        [self checkAndDismissProgress];
    }else if([[notify objectForKey:API_NotifyKey] intValue] == Request_ParkingArea){
        currentApp.areaDataArray = [notify objectForKey:API_NotifyData];
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
    }else if([[notify objectForKey:API_NotifyKey] intValue] == Request_GetFixPlace){
        if([mFacParser putArrayValue:[notify objectForKey:API_NotifyData]])
            currentApp.fids = [mFacParser getFactoryDic];
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
    }else if([[notify objectForKey:API_NotifyKey] intValue] == Request_getfans){
        NSArray *fansArry = [notify objectForKey:API_NotifyData];
        scrollDataArray=fansArry;
        [self putFansView:fansArry];
        mProgressCount=mProgressCount+1;
        [self checkAndDismissProgress];
    }//end else if
}

-(void)putFansView:(NSArray *)fansArry{
    [self.scrollView_news setContentSize:CGSizeMake(fansArry.count*self.scrollView_news.frame.size.width, self.scrollView_news.frame.size.height)];
    [self.scrollView_news  setPagingEnabled:YES];
    for(int n=0;n< fansArry.count;n++){
        NSDictionary *dic=[[fansArry objectAtIndex:n]copy];
        //---------------------------------
        //infter view
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"NewsView" owner:nil options:nil];
        UIView *news_view = [arr objectAtIndex:0];
        
        UIButton *button=(UIButton *)[news_view viewWithTag:1987];
        button.tag=n;
        [button addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imageView=(UIImageView *)[news_view viewWithTag:2005];
        NSString *img_url=[dic objectForKey:@"picture"];
        img_url=[img_url stringByReplacingOccurrencesOfString:@"s.jpg" withString:@"n.jpg"];
        UIImage *image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img_url]]];
        
        UILabel *label=(UILabel *)[news_view viewWithTag:1999];
        NSString *msg=[dic objectForKey:@"message"];
        
        UIImageView *img_bg=(UIImageView *)[news_view viewWithTag:9999];
        [img_bg setBackgroundColor:[[UIColor alloc]initWithRed:0.18 green:0.48 blue:0.77 alpha:0.8]];
        
        [label setText:msg];
        
        [imageView setImage:image];
        [imageView setFrame:CGRectMake(0, 0, news_view.frame.size.width,  news_view.frame.size.height)];
        [news_view setFrame:CGRectMake(self.scrollView_news.frame.size.width*n,0, self.scrollView_news.frame.size.width, self.scrollView_news.frame.size.height)];
        [news_view setBackgroundColor:[UIColor blackColor]];
        [self.scrollView_news addSubview:news_view];
        //---------------------------------
    }//end for
    [self.scrollView_news setDelegate:self];
    [self.view bringSubviewToFront: self.img_right];
    [self.view bringSubviewToFront: self.img_left];
    [self.img_left setHidden:YES];
    [self.img_right setHidden:NO];
}//end addFansView function

- (void)clickItem:(UIButton *)sender
{
    NSDictionary *mDictionary=[scrollDataArray objectAtIndex:sender.tag];
    NSString *link=[mDictionary objectForKey:@"link"];
    //    NSLog(@"link in index: %@",link);
    if([self loadWithRequest:link]){
        FansController *vc = [[FansController alloc] initWithNibName:@"FansController" bundle:nil andLink:link andtitle:[mDictionary objectForKey:@"viewtitle"]];
        [self.navigationController pushViewController:vc animated:YES];
        vc = nil;
    }
}

#pragma mark - Private API
- (void)buildBtn
{
    int btnX = 0;
    int btnY = 0;
    for (int i=1; i<=12; ++i)
    {
        NSString *imgName = [NSString stringWithFormat:(i<10) ? @"menu_0%d.png" : @"menu_%d.png", i];
        UIImage *icon=[UIImage imageNamed:imgName];
        CGRect rect=CGRectMake(btnX, btnY, 80, 92);
        CubeButton *btn = [[CubeButton alloc] initWithFrame:rect];
        
        [btn setTag:i];
        [btn setDelegate:self];
        [btn setImgIcon:icon imgText:nil];
        
        [_btnContainer addSubview:btn];
        [_arrBtn addObject:btn];
        
        switch (i % 4) {
            case 0:
            {
                btnX = 0;
                btnY += 92;
                break;
            }
            case 1:
                
                btnX = 80;
                break;
                
            case 2:
                btnX = 160;
                break;
            
            case 3:
                btnX = 240;
                break;
                
            default:
                break;
        }
        
        if(i==1){
            newsViewFrame=rect;
        }
    }
}

#pragma 顯示未讀訊息圖標
-(void)setAPNsView:(CGRect)underViewRect{
    int apnsCount=[self getNotificationNumber];
    if(apnsCount<=0){
        [self.apnsCountView setAlpha:0];
    }else{
        [self.apnsCountView setAlpha:1];
        
        CGFloat top,left,width,height;
        top=underViewRect.origin.y;
        left=underViewRect.origin.x;
        width=underViewRect.size.width;
        height=underViewRect.size.height;
        NSString *numbetString=[NSString stringWithFormat:@"%d",apnsCount];
        
        [self.apnsCountView setText:numbetString];
        CGSize size= self.apnsCountView.frame.size;
        
        [self.apnsCountView setFrame:CGRectMake(left+width*0.57, top+height*0.15, size.width, size.height)];
        
        self.apnsCountView.layer.cornerRadius = size.width/2;
        
        [_btnContainer bringSubviewToFront:self.apnsCountView];
    }
}

#pragma 獲取apns 數字
-(int)getNotificationNumber{
    
    if(!currentApp.newsArray){
        //如果尚未獲取newsArray則返回空值
        return 0;
    }
    int unreadCount=0;
    NSMutableDictionary *alreadyReadNews=[AppDelegate getAlreadyReadNews];
    NSMutableDictionary *newAlreadyReadNews=[NSMutableDictionary new];
    for(int n=0;n<currentApp.newsArray.count;n++){
        NSDictionary *dict=[currentApp.newsArray objectAtIndex:n];
        NSString *key= [NSString stringWithFormat:@"%@", [dict objectForKey:@"sno"]];
        
        NSString *value=[alreadyReadNews objectForKey:key];
        if(!value){
            //如果之前沒有這則內容，則填入未讀
            [newAlreadyReadNews setObject:@"0" forKey:key];
            unreadCount++;
        }else if([value isEqualToString:@"1"]){
            //如果之前有這則內容，並且已讀，填入已讀
            [newAlreadyReadNews setObject:@"1" forKey:key];
        }else{
            //如果之前有這內容，並且未讀，則填入未讀
            [newAlreadyReadNews setObject:@"0" forKey:key];
            unreadCount++;
        }
    }
    //設定userDefaults
    [AppDelegate setAlreadyReadNews:newAlreadyReadNews];
    return unreadCount;
    /*
     NSNumber *sno=[currentApp.newsArray objectAtIndex:n];
     alreadyReadNews*/
}



#pragma mark - Click Event
- (IBAction)clickNavSetting:(id)sender
{
    SettingController *setting = [[SettingController alloc] initWithNibName:@"SettingController" bundle:nil];
    [setting setDelegate:self];
    [setting setMode:YES];
    [self.navigationController pushViewController:setting animated:YES];
    setting = nil;
}


- (IBAction)clickNavSettingLogout:(id)sender
{
    //登出
    [[[UIAlertView alloc] initWithTitle:nil message:LOGOUT_MSG delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"好", nil] show];
}

#pragma mark - CubeButtonDelegate
- (void)clickCubeButton:(CubeButton *)cubeBtn
{
    UIViewController *vc = nil;
    
    switch ([cubeBtn tag])
    {
        case 1: // 最新消息
        {
            NewsListController *newList = [[NewsListController alloc] initWithNibName:@"NewsListController" bundle:nil];
            [newList setContent:newsArray];
            [newList reloadData];
            
            vc = newList;
            
            newList = nil;
            break;
        }
            
        case 2: // 粉絲團 (New)
        {
            vc = [[FansController alloc] initWithNibName:@"FansController" bundle:nil andLink:@"https://www.facebook.com/hyundai.tw" andtitle:@"粉絲團"];
            break;
        }
            
        case 3: // 影音專區 (New)
        {
            vc = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
            break;
        }
            
        case 4: // 車主季刊 (New)
        {
            vc = [[QuarterlyViewController alloc] initWithNibName:@"QuarterlyViewController" bundle:nil];
            break;
        }
            
        case 5: // 聯絡營業員
        {
            vc = [[ContactController alloc] initWithNibName:@"ContactController" bundle:nil];
            break;
        }
            
        case 6: // 維修紀錄
        {
            vc = [[MaintenanceController alloc] initWithNibName:@"MaintenanceController" bundle:nil];
            break;
        }
            
        case 7: // 服務據點
        {
            vc = [[ServiceController alloc] initWithNibName:@"ServiceController" bundle:nil];
            break;
        }
            
        case 8: // 道路救援
        {
            vc = [[SOPController alloc] initWithNibName:@"SOPController" bundle:nil];
            break;
        }
            
        case 9: // 新車資訊
        {
            vc = [[CarInfoController alloc] initWithNibName:@"CarInfoController" bundle:nil];
            break;
        }
            
        case 10: // 購車優惠
        {
            vc = [[BuyCarController alloc] initWithNibName:@"BuyCarController" bundle:nil];
            break;
        }
            
        case 11: // 加油站
        {
            vc = [[GasStationController alloc] initWithNibName:@"GasStationController" bundle:nil];
            break;
        }
            
        case 12: // 停車場
        {
            vc = [[ParkingPlaceController alloc] initWithNibName:@"ParkingPlaceController" bundle:nil];
            break;
        }
            
        default:
            break;
    }
    
    if (vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
        vc = nil;
    }
}

-(void)tempTouch:(UIButton *)sender{
    [view removeFromSuperview];
    view=nil;
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1)
    {
        if([alertView.message isEqualToString:LOGOUT_MSG]){
            [mLoginTools logout];
            
            [btn2 removeTarget:self action:@selector(clickNavSettingLogout:) forControlEvents:UIControlEventTouchUpInside];
            [btn2 setImage:[UIImage imageNamed:@"nav_setting.png"] forState:UIControlStateNormal];
            [btn2 addTarget:self action:@selector(clickNavSetting:) forControlEvents:UIControlEventTouchUpInside];
            
        }
    }
}

#pragma mark - LoginStatus
-(BOOL)success{
    //donothing
    return YES;
}
-(BOOL)fail{
    //donothing
    return YES;
}
#pragma - mark UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat width = scrollView.frame.size.width;
    NSInteger currentPage = ((scrollView.contentOffset.x - width / 2) / width) + 1;
    [self.img_left setHidden:NO];
    [self.img_right setHidden:NO];
    if(currentPage==0){
        [self.img_left setHidden:YES];
    }else if((scrollView.contentOffset.x+width)==scrollView.contentSize.width){
        [self.img_right setHidden:YES];
    }
    [self.view bringSubviewToFront: self.img_right];
    [self.view bringSubviewToFront: self.img_left];
}

@end
