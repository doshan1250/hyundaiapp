//
//  recordTools.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/22.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordTools : NSObject

-(void)setRecordOfReservation:(NSDictionary *)dic;
-(NSString *)getRecordOfReservation;
@end
