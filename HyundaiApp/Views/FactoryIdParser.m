//
//  FactoryIdParser.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "FactoryIdParser.h"

#define KEY_FIDS @"key_fids"
@interface FactoryIdParser(){
     NSDictionary *fidAreaTable;
    NSArray *areas;
    NSUserDefaults *mNSUserDefaults;
}

@end

@implementation FactoryIdParser

-(id)init{
    mNSUserDefaults=[NSUserDefaults standardUserDefaults];
    areas=[[NSArray alloc]initWithObjects:@"台北市",@"新北市",@"桃竹苗",@"中彰投",@"雲嘉南",@"高屏",@"宜花東", nil];
    fidAreaTable=[[NSDictionary alloc]initWithObjectsAndKeys:@"台北市",@"1",@"新北市",@"2",@"桃竹苗",@"3",@"中彰投",@"4",@"雲嘉南",@"5",@"高屏",@"6",@"宜花東",@"7", nil];
    return [super init];
}

-(NSArray *)getFacAreaNames{
    return areas;
}

-(NSArray *)getFacAreaIDs{
    return [fidAreaTable allKeys];
}

-(BOOL)putArrayValue:(NSArray *)object{
    NSMutableDictionary *murtableDic=[NSMutableDictionary new];
    for(int n=0;n<object.count;n++){
        NSDictionary *dic=[object objectAtIndex:n];
        NSString *value=[fidAreaTable objectForKey:[dic objectForKey:@"areanum"]];
        [murtableDic setObject:[dic objectForKey:@"data"] forKey:value];
    }
    [mNSUserDefaults setValue:[[NSDictionary alloc]initWithDictionary:murtableDic] forKey:KEY_FIDS];
   return  [mNSUserDefaults synchronize];
}


-(NSDictionary *)getFactoryDic{
    return (NSDictionary *)[mNSUserDefaults objectForKey:KEY_FIDS];;
}
@end
