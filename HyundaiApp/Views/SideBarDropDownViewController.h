//
//  SideBarDropDownViewController
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/18.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SideBarDelegate

-(void)exitWithAnimation:(NSInteger)tag;
-(void)dismiss;

@end

@interface SideBarDropDownViewController : UIViewController
-(void)animationWithView;
-(void)setDelegate:(id<SideBarDelegate>) delegate;
@end
