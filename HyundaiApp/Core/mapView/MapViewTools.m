//
//  MapViewViewController.m
//  MapViewTest
//
//  Created by ShuHsien Lin on 2013/11/10.
//  Copyright (c) 2013年 Linkinedge. All rights reserved.
//

#import "MapViewTools.h"

#define XX  0.00
#define YY  0.00


@interface MapViewTools ()<CLLocationManagerDelegate>{
    CLLocationManager *_locMgr;
    CLLocation *_currLoc;
    PlacePin *userLoc;
}

@end

@implementation MapViewTools
@synthesize delegate;

-(id)init{
    mapView=[MKMapView new];
    
	_locMgr = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [_locMgr requestWhenInUseAuthorization];
	[_locMgr setDelegate:self];
	[_locMgr setDesiredAccuracy:kCLLocationAccuracyBest];
	[_locMgr stopUpdatingLocation];
    
	[_locMgr startUpdatingLocation];
    
    [self mapInit];
    return self;
}

-(void)dealloc{
    [_locMgr stopUpdatingLocation];
    [_locMgr setDelegate:nil];
    
    _locMgr = nil;
}

-(MKMapView *)getMapView{
    return mapView;
}

-(void)mapInit{
    
    //顯示目前位置（藍色圓點）
    mapView.showsUserLocation = NO;
    //MapView的環境設置
    mapView.mapType = MKMapTypeStandard;
    mapView.scrollEnabled = YES;
    mapView.zoomEnabled = YES;
    //    - See more at: http://furnacedigital.blogspot.tw/2010/12/mapkit.html#sthash.5fATBHvF.dpuf
}

//放置點
-(PlacePin *)putPoint:(NSString *)title andSubTitle:(NSString *)subTitle andLat:(CLLocationDegrees )latitude andLng:(CLLocationDegrees)longitude{
    
    CLLocationCoordinate2D pinCenter;
    
    pinCenter.latitude = latitude;
    pinCenter.longitude = longitude;
    
    return [self putPoint:pinCenter andTitle:title andSubTitle:subTitle];
}

//放置點
-(PlacePin *)putPoint:(CLLocationCoordinate2D )coordinate andTitle:(NSString *)title andSubTitle:(NSString *)subTitle {
    PlacePin *annotation = [[PlacePin alloc]initWithCoordinate:coordinate];
    annotation.title =title;
    annotation.subtitle = subTitle;
    [mapView addAnnotation:annotation];
    
    return annotation;
}

//放置點
-(PlacePin *)putUserPoint:(NSString *)title andSubTitle:(NSString *)subTitle andLat:(CLLocationDegrees )latitude andLng:(CLLocationDegrees)longitude{
    CLLocationCoordinate2D pinCenter;
    
    pinCenter.latitude = latitude;
    pinCenter.longitude = longitude;
    
    return [self putUserPoint:pinCenter andTitle:title andSubTitle:subTitle];
}

//放置點
-(PlacePin *)putUserPoint:(CLLocationCoordinate2D )coordinate andTitle:(NSString *)title andSubTitle:(NSString *)subTitle {
    
    PlacePin *userLoc2;
    userLoc2= [[PlacePin alloc]initWithCoordinate:coordinate];
    userLoc2.title =title;
    userLoc2.subtitle = subTitle;
    
    [mapView addAnnotation:userLoc2];
    
    if(userLoc){
        [mapView removeAnnotation:userLoc];
    }
    userLoc=userLoc2;
    return userLoc;
}


//移動到指定位置
-(void)moveToPostionWithLat:(double)lat andLng:(double)lng{
    [self setMapRegionLongitudeWithAnimation:lng andLatitude:lat withLongitudeSpan:0.01 andLatitudeSpan:0.01];
}


//移動到目前位置
- (void)moveToNow {
    //取得現在位置
    double X=XX;
    double Y =YY;
    if(_currLoc){
        X = _currLoc.coordinate.latitude;
        Y= _currLoc.coordinate.longitude;
    }else{
        X = mapView.userLocation.location.coordinate.latitude;
        Y = mapView.userLocation.location.coordinate.longitude;
    }
    if(X==0.00&&Y==0.00){
        X=XX;
        Y=YY;
    }
    
    //自行定義的設定地圖函式
    [self setMapRegionLongitude:Y andLatitude:X withLongitudeSpan:0.01 andLatitudeSpan:0.01];
}

-(CLLocation *)getNowLocation{
    CLLocation *loc=nil;
    if(_currLoc){
        loc=_currLoc;
    }else
        loc= mapView.userLocation.location;
    if(!loc){
        loc=[[CLLocation alloc]initWithLatitude:XX longitude:YY];
    }
    return [loc copy];
}

//獲得目前的位置
-(CLLocationCoordinate2D )getNowLocation2D{
    
    double X=XX;
    double Y =YY;
    if(_currLoc){
        X = _currLoc.coordinate.latitude;
        Y= _currLoc.coordinate.longitude;
    }else{
        X = mapView.userLocation.location.coordinate.latitude;
        Y = mapView.userLocation.location.coordinate.longitude;
    }
    if(X==0.00&&Y==0.00){
        X=XX;
        Y=YY;
    }else{
        
        return CLLocationCoordinate2DMake(X, Y);
    }
    CLLocationCoordinate2D mCLLocationCoordinate2D=CLLocationCoordinate2DMake(X, Y);
    return mCLLocationCoordinate2D;
    
    
}

//自行定義的設定地圖函式
- (void)setMapRegionLongitude:(double)Y andLatitude:(double)X withLongitudeSpan:(double)SY andLatitudeSpan:(double)SX {
    [self go:[self getMKCoordinateRegion:Y andLatitude:X withLongitudeSpan:SY andLatitudeSpan:SX] andAnimation:NO];
}

- (void)setMapRegionLongitudeWithAnimation:(double)Y andLatitude:(double)X withLongitudeSpan:(double)SY andLatitudeSpan:(double)SX {
    [self go:[self getMKCoordinateRegion:Y andLatitude:X withLongitudeSpan:SY andLatitudeSpan:SX] andAnimation:YES];
}

-(MKCoordinateRegion)getMKCoordinateRegion:(double)Y andLatitude:(double)X withLongitudeSpan:(double)SY andLatitudeSpan:(double)SX{
    //設定經緯度
    CLLocationCoordinate2D mapCenter;
    mapCenter.latitude = X;
    mapCenter.longitude = Y;
    //Map Zoom設定
    MKCoordinateSpan mapSpan;
    mapSpan.latitudeDelta = SX;
    mapSpan.longitudeDelta = SY;
    //設定地圖顯示位置
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapCenter;
    mapRegion.span = mapSpan;
    return mapRegion;
}


-(void)go:(MKCoordinateRegion)mapRegion andAnimation:(BOOL)animation{
    //前往顯示位置
    [mapView setRegion:mapRegion animated:animation];
    [mapView regionThatFits:mapRegion];
}

#pragma mark - CLLocationManagerDelegate Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    _currLoc = [newLocation copy];
    if(delegate){
        [delegate locMag:manager didUpdateToLocation:newLocation fromLocation:oldLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"尚未設定定位服務"
                                                        message:@"請確認您的定位設定是否正確。"
                                                       delegate:self
                                              cancelButtonTitle:@"好，我知道了"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied) {
        // permission denied
        NSLog(@"permission denied");
    }
    else if (status == kCLAuthorizationStatusAuthorized) {
        // permission granted
        NSLog(@" permission granted");
    }
}

@end
