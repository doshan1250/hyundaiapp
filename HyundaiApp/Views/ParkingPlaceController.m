//
//  ParkingPlaceController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ParkingPlaceController.h"
#import "MapViewTools.h"
#import "ListOptionViewController.h"
#import <MapKit/MapKit.h>
#import "PlacePin.h"
#import "ListResultViewController.h"
#import "ViewWithMapViewController.h"
#import "SideBarDropDownViewController.h"
#import "MapForGoogleAPIViewController.h"


@interface ParkingPlaceController ()<UIAlertViewDelegate,LStableCellSelectDelegate,MKMapViewDelegate,NavMapDelegate,ListResultDelegate,mapToolDelegate>{
    MapViewTools *mMapViewTools;
    
    ListOptionViewController *mListSearchAreaOptionViewController;
    //地圖的data從網路上抓下來以後就先暫時存著，下次開啟頁面再重讀
    
    NSArray *stationDataArray;
    NSArray *areaDataArray;
    NSString *_areaName;
    NSArray *_areaIDArray;
    //作為列表查詢用的id
    NSString *_areaId;
    ListResultViewController *mListResultViewController;
    //地圖點擊後產生的資料，在資料和點之間建立一個連結的索引表
    NSMutableDictionary *mapDicts;
    ViewWithMapViewController *mViewWithMapViewController;
    
    CLLocation *mCLLocation;
    NSString *_tel;//infowindow的電話號碼
    UIButton *nav_button;
}
@property (strong, nonatomic) IBOutlet UIView *listPage;
@property (strong, nonatomic) IBOutlet UIButton *bt_area_listPage;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *bt_map;
@property (strong, nonatomic) IBOutlet UIButton *bt_list;
@property (strong, nonatomic) IBOutlet UIButton *bt_submit;

@end

@implementation ParkingPlaceController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //先隱藏起來，因為第一個畫面應該是地圖
    _listPage.hidden=YES;
    _titleView.text = @"停車場";
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady:"];
    areaDataArray=[currentApp.areaDataArray copy];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    _tel=nil;
    nav_button=nil;
    [currentApp.dataMgr delObserverData:self];
    mListSearchAreaOptionViewController =nil;
}

-(BOOL)clickedHomeEvent{
    if(mListResultViewController &&[mListResultViewController.view superview]){
        [mListResultViewController.view removeFromSuperview];
    }
    [mListResultViewController setDelegate:nil];
    mListResultViewController=nil;
    
    if(mViewWithMapViewController){
        if([mViewWithMapViewController.view superview]){
            [mViewWithMapViewController.view removeFromSuperview];
        }
        [mViewWithMapViewController setDelegate:nil];
        mViewWithMapViewController=nil;
    }
    mMapViewTools=nil;
    [mListSearchAreaOptionViewController setDelegate:nil];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!mMapViewTools){
        mMapViewTools=[[MapViewTools alloc]init];
        [mMapViewTools setDelegate:self];
    }
    [self addMapView];
    
    _listPage.hidden=YES;
    [self.bt_list setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    
}

//添加地圖畫面
-(void)addMapView{
    MKMapView *view=[mMapViewTools getMapView];
    if(![view superview]){
        view.frame=self.contentView.frame;
        [self.contentView addSubview:view];
    }else{
        [self.contentView bringSubviewToFront:view];
    }
    view.delegate=self;
}

-(void)loadDataWithMap{
    [mMapViewTools moveToNow];
    CLLocationCoordinate2D mCLLocationCoordinate2D=[mMapViewTools getNowLocation2D];
    //公里數需要確定
    [currentApp.dataMgr fireRequest:Request_ParkingDataMap postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%g",mCLLocationCoordinate2D.latitude],@"lat",[NSString stringWithFormat:@"%g",mCLLocationCoordinate2D.longitude],@"lng",@"1",@"ran", nil]];
}



//地圖
- (IBAction)selectMap:(UIButton *)sender {
    NSLog(@"1");
    _listPage.hidden=YES;
    [self.bt_list setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [self addMapView];
    
}

//列表
- (IBAction)selectList:(UIButton *)sender {
    [self.bt_list setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    _listPage.hidden=NO;
    [self.contentView bringSubviewToFront:_listPage];
}

- (IBAction)listAreaChoose:(UIButton *)sender {
    [self.bt_area_listPage setEnabled:NO];
    
    [self showData];
}


-(void) showData{
    _areaName=@"";
    NSMutableArray *mNSMutableArray = [[NSMutableArray alloc]init];
    for(int n=0;n<[areaDataArray count];n++){
        NSDictionary * mNSDictionary=[areaDataArray objectAtIndex:n];
        [mNSMutableArray addObject:[mNSDictionary objectForKey:@"city"]];
    }
    NSArray *areaNames=[NSArray arrayWithArray:mNSMutableArray];
    mListSearchAreaOptionViewController=[[ListOptionViewController alloc]initWithNibName:@"ListOptionViewController" bundle:nil];
    [mListSearchAreaOptionViewController setDatas:areaNames];
    [mListSearchAreaOptionViewController setDelegate:self];
    //[self.view  addSubview:mListSearchAreaOptionViewController.view];
    mListSearchAreaOptionViewController.view.frame=currentApp.self.window.frame;
    [currentApp.self.window addSubview:mListSearchAreaOptionViewController.view];
    [self.bt_area_listPage setEnabled:YES];
    
}

-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)areaName{
    //點選了地區之後回傳地區參數
    if(_areaIDArray){
        [view removeFromSuperview];
        [self.bt_area_listPage setTitle:[NSString stringWithFormat:@"%@-%@",_areaName,areaName] forState:UIControlStateNormal];
        _areaId=[[_areaIDArray objectAtIndex:[row integerValue]] objectForKey:@"areaid"];
        //最後消除這個內容
        _areaIDArray=nil;
        //選擇完畢之後可以提供送出
        [self.bt_submit setEnabled:YES];
        mListSearchAreaOptionViewController =nil;
    }else{
        [self.bt_area_listPage setTitle:areaName forState:UIControlStateNormal];
        [self.bt_area_listPage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        _areaName=areaName;
        
        _areaIDArray =[[[areaDataArray objectAtIndex:[row integerValue]] objectForKey:@"area"] copy];
        //跳第二層選擇細節
        
        
        NSMutableArray *mNSMutableArray = [[NSMutableArray alloc]init];
        for(int n=0;n<[_areaIDArray count];n++){
            NSDictionary * mNSDictionary=[_areaIDArray objectAtIndex:n];
            [mNSMutableArray addObject:[mNSDictionary objectForKey:@"name"]];
        }
        NSArray *areaNames=[NSArray arrayWithArray:mNSMutableArray];
        
        [mListSearchAreaOptionViewController setDatas:areaNames];
        [mListSearchAreaOptionViewController setDelegate:self];
        mListSearchAreaOptionViewController.view.frame=currentApp.self.window.frame;
        [mListSearchAreaOptionViewController reloadData];
    }
}

-(void)touchOutSide:(UIView *)view{
    [view removeFromSuperview];
    mListSearchAreaOptionViewController =nil;
}

- (IBAction)listSubmit:(UIButton *)sender {
    //列表的頁面中，點選送出跳轉頁面
    if(_areaName &&![_areaName isEqualToString:@""]){
        [currentApp.dataMgr fireRequest:Request_ParkingData postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: _areaId,@"areaid", nil]];
    }
}

#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_ParkingData){
        NSArray *array = [[notify objectForKey:API_NotifyData]copy];
        
        if(array.count>0){
            BOOL superView=YES;
            if(!mListResultViewController){
                mListResultViewController=[[ListResultViewController alloc]initWithNibName:@"ListResultViewController" bundle:nil];
                mListResultViewController.view.frame=self.contentView.frame;
                superView=NO;
            }
            [mListResultViewController setDelegate:self];
            [mListResultViewController setAreaDatas:array andImage:[UIImage imageNamed:@"icon_p.png"]];
            [mListResultViewController setLocation:[mMapViewTools getNowLocation]];
            if(superView){
                [self.contentView bringSubviewToFront:mListResultViewController.view];
                [mListResultViewController reloadData];
            }else{
                [self.contentView addSubview:mListResultViewController.view];
            }
        }else{
            UIAlertView *alertView;
            alertView=[[UIAlertView alloc]initWithTitle:nil message:@"查無該地區停車場資料！" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_ParkingDataMap){
        stationDataArray = [notify objectForKey:API_NotifyData];
        MKMapView *map=[mMapViewTools getMapView];
        [map removeAnnotations:map.annotations];
        mapDicts=[[NSMutableDictionary alloc]init];
        MKMapView *mapView=[mMapViewTools getMapView];
        [mapView removeAnnotations:mapView.annotations];
        for(int n=0;n<stationDataArray.count;n++){
            NSDictionary *data=[stationDataArray objectAtIndex:n];
            /**
             為了讀取重複資料的錯誤避免 這裡把資料做一份hash map
             用標點的title作為key 點擊button的時候依據key取得hash map的內容
             */
            [mapDicts setObject:[data copy] forKey:[data objectForKey:@"name"]];
            NSString *addressAndTel;
            if([data objectForKey:@"tel"]==nil){
                addressAndTel=@"沒有電話號碼";
            }else{
                addressAndTel =[data objectForKey:@"tel"];
                if([[addressAndTel stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
                    addressAndTel=@"沒有電話號碼";
                }else{
                    addressAndTel =[NSString stringWithFormat:@"電話: %@",[data objectForKey:@"tel"]];
                }
            }
            [mMapViewTools putPoint:[data objectForKey:@"name"] andSubTitle:addressAndTel andLat:[[data objectForKey:@"lat"] doubleValue] andLng:[[data objectForKey:@"lng"] doubleValue]];
        }
    }//end if else
    
}

#pragma mark - methods for MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    static NSString *currentloc=@"currentloc";
    if ([[annotation title] isEqualToString:@"now"]) {
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:currentloc];
        if(!pinView){
            pinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:currentloc];
            [pinView setImage:[UIImage imageNamed:@"icon_my_location.png"]];
            pinView.canShowCallout = NO;
        }
        else{
            pinView.annotation = annotation;
        }
        return pinView;
    }else{
        
        
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if (!pinView && ![annotation isKindOfClass:[MKUserLocation class]])
        {
            //這裡針對大頭針做各種設定
            MKAnnotationView* customPinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:AnnotationViewID];
            //設置圖釘圖案
            customPinView.image=[UIImage imageNamed:@"icon_map_p.png"];
            //顯示視窗
            customPinView.canShowCallout = YES;
            NSDictionary *dict=[mapDicts objectForKey:[annotation title]];
            
            _tel=[dict objectForKey:@"tel"];
            //右邊的info內容
            UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            if(!_tel||[_tel isEqualToString:@""]){
                [rightBtn setEnabled:NO];
            }
            [rightBtn setFrame:CGRectMake(0, 0, 30, 30)];
            [rightBtn setImage:[UIImage imageNamed:@"icon_tel.png"] forState:UIControlStateNormal];
            
            customPinView.rightCalloutAccessoryView = rightBtn;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
        return pinView;
    }
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    if ([[[view annotation] title] isEqualToString:@"now"] ) {
        return;
    }
    [nav_button removeFromSuperview];
    [mViewWithMapViewController.view removeFromSuperview];
}

//地圖marker 被點擊的事件
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if ([[[view annotation] title] isEqualToString:@"now"] ) {
        return;
    }
    
    NSDictionary *dict=[mapDicts objectForKey:[[view annotation] title]];
    [mMapViewTools moveToPostionWithLat:[[dict objectForKey:@"lat"]doubleValue ] andLng:[[dict objectForKey:@"lng"]doubleValue]];
    
    NSLog(@"%@",dict);
    if(!mViewWithMapViewController){
        mViewWithMapViewController=[[ViewWithMapViewController alloc] initWithNibName:@"ViewWithMapViewController" bundle:nil];
    }
    [self.contentView addSubview:mViewWithMapViewController.view];
    [mViewWithMapViewController enableNavButton:(mCLLocation!=nil)];
    [mViewWithMapViewController setOrigin: mCLLocation.coordinate  andDestination:CLLocationCoordinate2DMake([[dict objectForKey:@"lat"]doubleValue], [[dict objectForKey:@"lng"]doubleValue])];
    
    [mViewWithMapViewController setDelegate:self];
    NSString *mString=[NSString stringWithFormat:@"%@\n(%@, %@)",[dict objectForKey:@"alladdress"],[dict objectForKey:@"lat"],[dict objectForKey:@"lng"]];
    [mViewWithMapViewController setTitleForParking:[dict objectForKey:@"name"] andAlladdress:mString andSummary:[dict objectForKey:@"summary"] andTel:[dict objectForKey:@"tel"] andIcon:@"icon_p.png"];
    
    mViewWithMapViewController.view.frame=CGRectMake(0, self.contentView.frame.size.height-mViewWithMapViewController.view.frame.size.height, mViewWithMapViewController.view.frame.size.width, mViewWithMapViewController.view.frame.size.height);
    //因為resize導致按鈕跑出來了，把他放到外面來動作
    
    nav_button=[mViewWithMapViewController getnavMapBtn];
    CGRect frame=nav_button.frame;
    [nav_button removeFromSuperview];
    [nav_button setFrame:CGRectMake(frame.origin.x, mViewWithMapViewController.view.frame.origin.y+frame.origin.y, frame.size.width, frame.size.height)];
    [self.contentView addSubview:nav_button];
}

//AnnotationView's UIControl 被點擊後的動作反應
-(void)mapView:(MKMapView *)mMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    //TEL
    NSDictionary *dict=[mapDicts objectForKey:[[view annotation] title]];
    NSString *telString=[NSString stringWithFormat:@"tel://%@",[dict objectForKey:@"tel"]];
    telString=[telString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    _tel=telString;
    [[[UIAlertView alloc] initWithTitle:nil message:@"確定撥出電話" delegate:self cancelButtonTitle:@"不要" otherButtonTitles:@"好", nil] show];
    dict=nil;
}

- (void)locMag:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    //偵測移動重新計算產生點
    if(mCLLocation){
        CLLocationDistance dis=[newLocation distanceFromLocation:mCLLocation];
        if(dis>1000){
            mCLLocation=[newLocation copy];
            [self loadDataWithMap];
        }
    }else{
        mCLLocation=[newLocation copy];
        [self loadDataWithMap];
    }
    [mMapViewTools putUserPoint:mCLLocation.coordinate andTitle:@"now" andSubTitle:@""];
    
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
}

#pragma mark - UIAlertViewDelegate 撥打電話
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_tel]];
        NSLog(@"tel: %@",_tel);
    }
}

#pragma mark - NavMapDelegate
-(void)navForOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest{
    MapForGoogleAPIViewController *vc=[[MapForGoogleAPIViewController alloc]initWithNibName:@"MapForGoogleAPIViewController" bundle:nil];
    [vc setType:0];
    [vc setOrigin:origin andDestination:dest];
    [self.navigationController pushViewController:vc animated:YES];
    vc=nil;
}

#pragma mark - ListResultDelegate
-(void)clickedItem:(CLLocationCoordinate2D)dest{
    CLLocationCoordinate2D mCLLocationCoordinate2D=[mMapViewTools getNowLocation2D];
    if(mCLLocationCoordinate2D.latitude==0.00){
        UIAlertView *alertView;
        alertView=[[UIAlertView alloc]initWithTitle:nil message:@"目前尚無位置資訊，請稍候再嘗試" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    MapForGoogleAPIViewController *vc=[[MapForGoogleAPIViewController alloc]initWithNibName:@"MapForGoogleAPIViewController" bundle:nil];
    [vc setType:0];
    [vc setOrigin:mCLLocationCoordinate2D andDestination:dest];
    [self.navigationController pushViewController:vc animated:YES];
    vc=nil;
}

@end
