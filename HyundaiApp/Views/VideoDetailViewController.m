//
//  VideoDetailViewController.m
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/29.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import "VideoDetailViewController.h"
#import "AsyncImageView.h"

#import "YoutubeViewController.h"

#import "AppDelegate+Facebook.h"
#import "LineKit.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "NewsWebViewController.h"

@interface VideoDetailViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet AsyncImageView *_img;
    IBOutlet UILabel *_title;
    IBOutlet UILabel *_date;
    IBOutlet UITextView *_content;
    IBOutlet UIWebView *_webContent;
    NSDictionary *_data;
    
    UIActionSheet *_sheet;
    NSString *_shareUrl;
}
@end

@implementation VideoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
    _titleView.text = @"影音專區";
    
//    _webContent.hidden = YES;
    _content.hidden = YES;
    
    [_webContent setBackgroundColor:[UIColor clearColor]];
    [_webContent setOpaque:NO];
    
    [self reloadData];
    
    
    UIBarButtonItem *navSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    navSpace.width = NAV_SPACING;
    
    NavButton *btn = [NavButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 44, 44)];
    [btn setImage:[UIImage imageNamed:@"icon_share.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickShare) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableArray *rightBtns = [[NSMutableArray alloc] initWithArray:[self.navigationItem.rightBarButtonItems copy]];
    self.navigationItem.rightBarButtonItems = @[[rightBtns objectAtIndex:0], [[UIBarButtonItem alloc] initWithCustomView:btn], [rightBtns objectAtIndex:1]];
    
    rightBtns = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Share
- (void)shareCopy
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _shareUrl;
    
    [Utilities showMessage:@"影片連結已複製" message:_shareUrl];
}

- (void)shareMail
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    if([MFMailComposeViewController canSendMail])
    {
        picker.mailComposeDelegate = self;
        picker.delegate = self;
        
        [picker setSubject:[_data objectForKey:@"title"]];
        [picker setMessageBody:[NSString stringWithFormat:@"%@<br/><br/>%@", [_data objectForKey:@"summary"], _shareUrl] isHTML:YES];
        
        [self presentViewController:picker animated:YES completion:^{
            
        }];
    }
    else
    {
        [Utilities showMessage:@"無法分享至 E-MAIL！" message:nil];
    }
}

- (void)shareFB
{
    NSDictionary *shareData = @{@"title":[_data objectForKey:@"title"],
                                @"description":[_data objectForKey:@"summary"],
                                @"photo":[_data objectForKey:@"thumb"],
                                @"link":_shareUrl};
    
    [currentApp doFacebookShare:shareData];
}

- (void)shareLine
{
//    if ([LineKit isUserInstallLine])
//    {
        [LineKit shareLineWithMessage:[NSString stringWithFormat:@"%@ | %@", [_data objectForKey:@"title"], _shareUrl]];
//    }
//    else
//    {
//        [Utilities showMessage:@"您尚未安裝 LINE 無法分享" message:nil];
//    }
}

- (void)clickShare
{
    if ([UIAlertController class])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"分享至..." message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        }];
        
        UIAlertAction *actCopy = [UIAlertAction actionWithTitle:@"複製連結" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self shareCopy];
        }];
        
        UIAlertAction *actEmail = [UIAlertAction actionWithTitle:@"E-MAIL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self shareMail];
        }];
        
        UIAlertAction *actFB = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self shareFB];
        }];
        
        UIAlertAction *actLine = [UIAlertAction actionWithTitle:@"LINE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self shareLine];
        }];
        
        [alert addAction:actLine];
        [alert addAction:actFB];
        [alert addAction:actEmail];
        [alert addAction:actCopy];
        
        [alert addAction:actCancel];
        alert.view.tintColor = [UIColor blackColor];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        _sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"LINE", @"Facebook", @"E-MAIL", @"複製連結", nil];
        
        _sheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        [_sheet showInView:self.view];
    }
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0)
    {
        [self shareLine];
    }
    else if (buttonIndex == 1)
    {
        [self shareFB];
    }
    else if (buttonIndex == 2)
    {
        [self shareMail];
    }
    else if (buttonIndex == 3)
    {
        [self shareCopy];
    }
}

#pragma mark - Mail & MessageCompose Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
}

#pragma mark - Public API
- (void)setNewsData:(NSDictionary *)data
{
    _data = data;
}

#pragma mark - Private API
- (void)reloadData
{
    _title.text = [_data objectForKey:@"title"];
    _date.text = [[[_data objectForKey:@"disp_date"] componentsSeparatedByString:@"T"] objectAtIndex:0];
//    _content.text = [[_data objectForKey:@"summary"] stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];

    NSString *content = [[_data objectForKey:@"summary"] stringByReplacingOccurrencesOfString:@"<br />" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"<p>" withString:@"<span>"];
    content = [content stringByReplacingOccurrencesOfString:@"<p " withString:@"<span "];
    content = [content stringByReplacingOccurrencesOfString:@"</p>" withString:@"</span><br />"];
    content = [content stringByReplacingOccurrencesOfString:@"<br /><br />" withString:@"<br />"];
    content = [content stringByReplacingOccurrencesOfString:@"<br /><br />" withString:@"<br />"];
    [_webContent loadHTMLString:content baseURL:nil];
    
    [_img loadImageFromURL:[NSURL URLWithString:[_data objectForKey:@"thumb"]] savePath:nil];
    
    _shareUrl = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@", [_data objectForKey:@"code"]];
}

- (IBAction)clickPlayer:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"觀賞提示" message:@"請將手機向左轉成水平瀏覽模式" delegate:nil cancelButtonTitle:@"好，我知道了！" otherButtonTitles:nil];
    [alert show];
    alert = nil;
    
    YoutubeViewController *vc = [[YoutubeViewController alloc] initWithNibName:@"YoutubeViewController" bundle:nil];
    [vc setVcode:[_data objectForKey:@"code"]];
    
    [self.navigationController pushViewController:vc animated:YES];
    vc = nil;
}


#pragma mark - UIWebView Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}

#pragma mark - 以下 Webber 新增 for QRCode Scan & AES Decryption

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"%@", request);
    
    if ([[[request URL] absoluteString] hasPrefix:@"http://"] || [[[request URL] absoluteString] hasPrefix:@"https://"])
    {
        NewsWebViewController *vc = [[NewsWebViewController alloc] initWithNibName:@"NewsWebViewController" bundle:nil];
        [vc setTitle:@"影音專區"];
        [vc setURL:[[request URL] absoluteString]];
        [self.navigationController pushViewController:vc animated:YES];
        vc = nil;
        
        return NO;
    }
    
    return YES;
}
@end
