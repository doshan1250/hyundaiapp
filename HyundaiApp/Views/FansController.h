//
//  FansController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FansController : HyundaiViewController
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andLink:(NSString *)link andtitle:(NSString *)viewTitle;
@end
