//
//  NavButton.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "NavButton.h"

@interface NavButton ()
{
    int imageHeight;
}

@end

@implementation NavButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIEdgeInsets)alignmentRectInsets
{
    UIEdgeInsets insets;
    
    if (IS_IOS7)
    {
        if ([self isLeftButton])
            insets = UIEdgeInsetsMake(0, 10.0f, 0, 0);
        else
            insets = UIEdgeInsetsMake(0, 0, 0, 10.0f);
    }else
        insets = UIEdgeInsetsZero;
    
    return insets;
}

- (BOOL)isLeftButton
{
    return self.frame.origin.x < (self.superview.frame.size.width / 2);
}

-(CGRect)backgroundRectForBounds:(CGRect)bounds
{
    CGRect bgRect = bounds;
    bgRect.origin.y = (bounds.size.height - imageHeight)/2.0f;
    bgRect.size.height = imageHeight;
    
    return bgRect;
}

@end
