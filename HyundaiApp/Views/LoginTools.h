//
//  LoginTools.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/18.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginTools : NSObject{
   
}
-(NSString *)getCarId;
-(NSString *)getUserId;
-(BOOL)putUserId:(NSString *)user_id andCarId:(NSString *)car_id andData:(NSDictionary *)data;
-(BOOL)updateUserData:(NSDictionary *)data;
-(NSDictionary *)getUserData;
-(BOOL)shouldLogin;

-(BOOL)logout;

@end
