//
//  NavMapInfoCell.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/20.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavMapInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lb_title;
@property (strong, nonatomic) IBOutlet UILabel *lb_number;


@end
