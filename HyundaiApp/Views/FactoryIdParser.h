//
//  FactoryIdParser.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FactoryIdParser : NSObject
-(NSArray *)getFacAreaNames;
-(NSArray *)getFacAreaIDs;
-(BOOL)putArrayValue:(NSArray *)object;
-(NSDictionary *)getFactoryDic;
@end
