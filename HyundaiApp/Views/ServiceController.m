//
//  ServiceController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ServiceController.h"
#import "ServiceDetailController.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>
#import "ListOptionViewController.h"
#import "ProgressViewController.h"

@interface ServiceController () <CLLocationManagerDelegate,LStableCellSelectDelegate,ProgressDelegate>
{
	CLLocationManager *_locMgr;
    CLLocation *_currLoc;
    
    IBOutlet UIButton *carBtn1;
    IBOutlet UIButton *carBtn2;
    
    IBOutlet UIButton *placeBtn1;
    IBOutlet UIButton *placeBtn2;
    
    IBOutlet UIButton *btn_area;
    
    IBOutlet UIButton *submit;
    
    NSInteger iCar;
    NSInteger iPlace;
    NSInteger iArea;
    ListOptionViewController *mListOptionViewController;
    ProgressViewController *mProgressViewController;
}
@property (strong, nonatomic) IBOutlet UIButton *bt_submit;

- (IBAction)selectCar:(id)sender;
- (IBAction)selectPlace:(id)sender;
- (IBAction)selectArea:(id)sender;
- (IBAction)submit:(id)sender;


@end

@implementation ServiceController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        iCar = 1;
        iPlace = 1;
        iArea = 1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _titleView.text = @"服務據點";
    [self.view setBackgroundColor:HYUNDAI_BG_GREY];
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady:"];
    _currLoc=[[CLLocation alloc]initWithLatitude:0.00 longitude:0.00];
	_locMgr = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [_locMgr requestWhenInUseAuthorization];
    
	[_locMgr setDelegate:self];
	[_locMgr setDesiredAccuracy:kCLLocationAccuracyBest];
	[_locMgr stopUpdatingLocation];
    
	[_locMgr startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
	[_locMgr stopUpdatingLocation];
    [_locMgr setDelegate:nil];
    _locMgr = nil;
    [currentApp.dataMgr delObserverData:self];
    
}

#pragma mark - Private API
- (IBAction)selectCar:(id)sender
{
    iCar = [sender tag];
    [carBtn1 setImage:[UIImage imageNamed:(iCar == 1) ? @"vehicle_focused.png" : @"vehicle_non_focused.png"] forState:UIControlStateNormal];
    [carBtn2 setImage:[UIImage imageNamed:(iCar == 2) ? @"commercial_vehicle_focused.png" : @"commercial_vehicle_non_focused.png"] forState:UIControlStateNormal];
}

- (IBAction)selectPlace:(id)sender
{
    iPlace = [sender tag];
    [placeBtn2 setImage:[UIImage imageNamed:(iPlace == 1) ? @"show_room_focused.png" : @"show_room_non_focused.png"] forState:UIControlStateNormal];
    [placeBtn1 setImage:[UIImage imageNamed:(iPlace == 2) ? @"service_focused.png" : @"service_non_focused.png"] forState:UIControlStateNormal];
}

- (IBAction)selectArea:(id)sender
{
    mListOptionViewController=[[ListOptionViewController alloc]initWithNibName:@"ListOptionViewController" bundle:nil];
    NSArray *areas=[[NSArray alloc]initWithObjects:@"台北市",@"新北市",@"桃竹苗",@"中彰投",@"雲嘉南",@"高屏",@"宜花東", nil];
    [mListOptionViewController setDatas:areas];
    [mListOptionViewController setDelegate:self];
    
    mListOptionViewController.view.frame=currentApp.self.window.frame;
    [currentApp.self.window addSubview:mListOptionViewController.view];
}
#pragma mark - LStableCellSelectDelegate
-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)dataName{
    iArea=[row intValue]+1;
    
    [btn_area setTitle:dataName forState:UIControlStateNormal];
    [btn_area setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [view removeFromSuperview];
    mListOptionViewController=nil;
    [self.bt_submit setEnabled:YES];
}

-(void)touchOutSide:(UIView *)view{
    [view removeFromSuperview];
    mListOptionViewController =nil;
}

- (IBAction)submit:(id)sender
{
    mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
    [mProgressViewController setDelegate:self];
    mProgressViewController.view.frame=currentApp.window.frame;
    [currentApp.window addSubview:mProgressViewController.view];
    //這裡修正參數
    NSMutableDictionary *post = [NSMutableDictionary dictionaryWithObjectsAndKeys:NB_I(iCar), @"CType",  NB_I(iPlace), @"RType", NB_I(iArea), @"ANum", nil];
    [currentApp.dataMgr fireRequest:Request_ServiceData postData:post];
}

#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_ServiceData)
    {
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
        NSDictionary *data = [notify objectForKey:API_NotifyData];
        NSArray *array= [data copy];
        if(array&&array.count>0){
            ServiceDetailController *vc = [[ServiceDetailController alloc] initWithNibName:@"ServiceDetailController" bundle:nil];
            vc.arrData =[array copy];
            array=nil;
            vc.curLocation = [_currLoc copy];
            vc.isFactory=(iPlace==1)?NO:YES;
            [self.navigationController pushViewController:vc animated:YES];
            vc = nil;
        }else{
            [[[UIAlertView alloc] initWithTitle:nil message:@"該服務據點尚無資料" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil] show];
        }
    }
}

#pragma mark - CLLocationManagerDelegate Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    _currLoc = [newLocation copy];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"尚未設定定位服務"
                                                        message:@"請確認您的定位設定是否正確。"
                                                       delegate:self
                                              cancelButtonTitle:@"好，我知道了"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

#pragma mark - ProgressDelegate
-(void)timeout{
    if([mProgressViewController.view superview]){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
    }
}

@end
