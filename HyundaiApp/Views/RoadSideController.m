//
//  RoadSideController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "RoadSideController.h"
#import "LoginTools.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


#define CALL_0800   @"您是否要聯絡拖吊服務"

@interface RoadSideController ()<MKMapViewDelegate,CLLocationManagerDelegate,UIAlertViewDelegate>{
    LoginTools *mLoginTools;
}

@property (strong, nonatomic) IBOutlet UIButton *bt_tel;
@property (strong, nonatomic) IBOutlet UILabel *loc_title;
@property (strong, nonatomic) IBOutlet MKMapView *map;

@end

@implementation RoadSideController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"聯絡拖吊場";
    mLoginTools=[LoginTools new];
    if([mLoginTools shouldLogin]){
        [self.bt_tel setEnabled:NO];
    }
    //顯示目前位置（藍色圓點）
    self.map.showsUserLocation = YES;
    
    //MapView的環境設置
    self.map.mapType = MKMapTypeStandard;
    self.map.scrollEnabled = YES;
    self.map.zoomEnabled = YES;
    self.map.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
}

#pragma mark - CLLocationManagerDelegate -
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    [self setPlaceName];
    [self moveToPostionWithLat:userLocation.coordinate.latitude andLng:userLocation.coordinate.longitude];
}

- (MKAnnotationView *)mapView:(MKMapView *)map
            viewForAnnotation:(id <MKAnnotation>)annotation{
    static NSString *AnnotationViewID = @"annotationViewID";
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if(!pinView){
            MKAnnotationView* customPinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:AnnotationViewID];
            [customPinView setImage:[UIImage imageNamed:@"icon_my_location.png"]];
            customPinView.canShowCallout = NO;
            return customPinView;
        }else{
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

#pragma mark - IBAction
- (IBAction)tel:(id)sender {
    [[[UIAlertView alloc] initWithTitle:nil message:CALL_0800 delegate:self cancelButtonTitle:@"不要" otherButtonTitles:@"好", nil] show];
}

//移動到指定位置
-(void)moveToPostionWithLat:(double)lat andLng:(double)lng{
    [self setMapRegionLongitudeWithAnimation:lng andLatitude:lat withLongitudeSpan:0.01 andLatitudeSpan:0.01];
}

//移動到目前位置
- (void)moveToNow {
    double X=0.00,Y=0.00;
    X=self.map.userLocation.coordinate.latitude;
    Y=self.map.userLocation.coordinate.longitude;
    NSLog(@"%g, %g",X,Y);
    if(X==0.00&&Y==0.00){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"尚未取得您的位置資訊"
                                                            message:@"請稍後，或確認您的定位設定是否正確。"
                                                           delegate:self
                                                  cancelButtonTitle:@"好，我知道了"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }else{
        //自行定義的設定地圖函式
        [self setMapRegionLongitudeWithAnimation:Y andLatitude:X withLongitudeSpan:0.01 andLatitudeSpan:0.01];
    }
}

- (void)setMapRegionLongitudeWithAnimation:(double)Y andLatitude:(double)X withLongitudeSpan:(double)SY andLatitudeSpan:(double)SX {
    //NSLog(@"動畫");
    [self go:[self getMKCoordinateRegion:Y andLatitude:X withLongitudeSpan:SY andLatitudeSpan:SX] andAnimation:YES];
}

-(MKCoordinateRegion)getMKCoordinateRegion:(double)Y andLatitude:(double)X withLongitudeSpan:(double)SY andLatitudeSpan:(double)SX{
    
    //設定經緯度
    CLLocationCoordinate2D mapCenter;
    mapCenter.latitude = X;
    mapCenter.longitude = Y;
    
    //Map Zoom設定
    MKCoordinateSpan mapSpan;
    mapSpan.latitudeDelta = SX;
    mapSpan.longitudeDelta = SY;
    
    //設定地圖顯示位置
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapCenter;
    mapRegion.span = mapSpan;
    return mapRegion;
}


-(void)go:(MKCoordinateRegion)mapRegion andAnimation:(BOOL)animation{
    //前往顯示位置
    [self.map setRegion:mapRegion animated:animation];
    [self.map regionThatFits:mapRegion];
}

// 错误信息
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"error");
}


-(void)setPlaceName{
    //------------------位置反编码---5.0之后使用-----------------
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:self.map.userLocation.location
                   completionHandler:^(NSArray *placemarks, NSError *error){
                       for (CLPlacemark *place in placemarks) {
                           NSString *subtitle=place.locality;
                           if((!subtitle)||[subtitle isEqualToString:@""]||[subtitle isEqualToString:place.subAdministrativeArea]){
                               subtitle=place.subLocality;
                               if((!subtitle)||[subtitle isEqualToString:@""]){
                                   subtitle=place.name;
                               }
                           }
                           
                           [self.loc_title setText:[NSString stringWithFormat:@"%@ %@",place.subAdministrativeArea,subtitle]];
                           //----說明------
                           //NSLog(@"name,%@",place.name);                       // 位置名
                           //NSLog(@"thoroughfare,%@",place.thoroughfare);       // 街道
                           //NSLog(@"subThoroughfare,%@",place.subThoroughfare); // 子街道
                           //NSLog(@"locality,%@",place.locality);               // 市
                           //NSLog(@"subLocality,%@",place.subLocality);         // 区
                           //NSLog(@"country,%@",place.country);                 // 国家
                       }
                       
                   }];
}

- (IBAction)moveToNowTouchDown:(UIButton *)sender {
    [self moveToNow];
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    {
        if([alertView.message isEqualToString:CALL_0800]){
            NSString *tel=[NSString stringWithFormat:@"%@%@",@"tel://",@"0800010010"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
            NSLog(@"%@",tel);
        }
    }
}

@end
