//
//  LocationPoint.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "LocationPoint.h"

@implementation LocationPoint

@synthesize lat;
@synthesize lng;

-(void)setLat:(NSString *)lat_string andLng:(NSString *)lng_string{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    /*ref: http://blog.teddylau.hk/nsnumberformatter/ */
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    self.lat= [f numberFromString:lat_string];
    self.lng=[f numberFromString:lng_string];
}

@end
