//
//  SettingController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginStatus.h"

@interface SettingController : HyundaiViewController{
    BOOL _singleMode;
}
-(void)setMode:(BOOL)singleMode;
@property(nonatomic,weak) id<LoginStatus> delegate;

@end
