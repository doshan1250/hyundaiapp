//
//  recordTools.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/22.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "RecordTools.h"


@interface RecordTools(){
    NSDate *date;
    NSString *FName;
    NSDateFormatter *dateFormatter;
}

@end

@implementation RecordTools

#pragma mark - 放置預約記錄的內容
-(void)setRecordOfReservation:(NSDictionary *)dic{
    if(!currentApp.fids||currentApp.fids.count==0){
        return;
    }
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSArray *keys=[dic allKeys];
    NSString *fid;
    NSDate *nowDate= [NSDate date];
    for(NSString *key in keys){
        NSArray *arr=[dic objectForKey:key];
        NSString *dateString=[arr objectAtIndex:0];
        dateString=[dateString stringByReplacingOccurrencesOfString:@":00.0" withString:@""];
        NSDate *date1 = [dateFormatter dateFromString:dateString];
        
        if((!date ||([date compare:date1]==NSOrderedDescending))&&[nowDate compare:date1]==NSOrderedAscending){
            date=date1;
            fid=[arr objectAtIndex:2];
        }//if
    }//for
    //獲得資料接著要取得服務場名稱
    keys=[currentApp.fids allKeys];
    BOOL contu=YES;
    for(NSString *key in keys){
        NSArray *arr = [currentApp.fids objectForKey:key];
        for( NSDictionary *dic in arr){
            if([[dic objectForKey:@"servicecode"] isEqualToString:fid]){
                FName=[dic objectForKey:@"name"];//GET!!
                contu=NO;
                break;
            }
        }//end for
        if(!contu){
            break;
        }
    }//end for
}

-(NSString *)getRecordOfReservation{
    if(!date||!FName){
        return nil;
    }
    else{
        NSString *dateString=[dateFormatter stringFromDate:date];
        return [NSString stringWithFormat:@"已預約%@\n於%@",dateString,FName];
    }
}

#pragma mark
@end
