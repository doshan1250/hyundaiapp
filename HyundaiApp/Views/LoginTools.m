//
//  LoginTools.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/18.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "LoginTools.h"
#import <EventKit/EventKit.h>

#define USER_ID          @"userid"
#define CAR_ID           @"carID"
#define USER_DATA            @"userData"
#define CALENDER_DATA            @"CalenderData"


@interface LoginTools (){
    
    NSUserDefaults *mNSUserDefaults;
    
}

@end

@implementation LoginTools

-(id)init{
    mNSUserDefaults=[NSUserDefaults standardUserDefaults];
    return [super init];
}

#pragma mark -帳號與引擎號碼的建立與獲取
-(BOOL)putUserId:(NSString *)user_id andCarId:(NSString *)car_id andData:(NSDictionary *)data{
    [mNSUserDefaults setValue:user_id forKey:USER_ID];
    [mNSUserDefaults setValue:car_id forKey:CAR_ID];
    [mNSUserDefaults setValue:data forKey:USER_DATA];
    if(user_id){
        [self setSc:data];
    }else{
        [self delLastCalenderData];
    }
    return [mNSUserDefaults synchronize];
}
#pragma mark - 建立一個行程
//see more - http://stackoverflow.com/questions/246249/programmatically-add-custom-event-in-the-iphone-calendar
-(void)setSc:(NSDictionary *)data{
    //[self delLastCalenderData];
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = @"現代好行App提醒：您有預約汽車保養";
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSDate *myDate = [df dateFromString: [data objectForKey:@"NextServiceDate"]];
        //設定開始時間與結束時間
        event.startDate = myDate;
        event.endDate = [event.startDate dateByAddingTimeInterval:24*60*60];

        NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init];
        EKAlarm *alarm1 = [EKAlarm alarmWithRelativeOffset:-1296000]; // 15 Day
//        NSTimeInterval secondsPerDay = 60;
//        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:secondsPerDay];
//        EKAlarm *alarm1 = [EKAlarm alarmWithAbsoluteDate:date];
        
        [myAlarmsArray addObject:alarm1];
        
        event.alarms = myAlarmsArray;
        
        [event setCalendar:[store defaultCalendarForNewEvents]];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        NSString *savedEventId = event.eventIdentifier;
        
        [mNSUserDefaults setValue:savedEventId forKey:CALENDER_DATA];
    }];
}

#pragma mark - 獲得行事曆上一筆事件資料 要刪除
-(void)delLastCalenderData{
    NSString* savedEventId=(NSString *)[mNSUserDefaults objectForKey:CALENDER_DATA];
    if(!savedEventId){
        NSLog(@"沒有行事曆資料");
        return;
    }
    NSLog(@"刪除行事曆資料");
    EKEventStore* store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent* eventToRemove = [store eventWithIdentifier:savedEventId];
        if (eventToRemove) {
            NSError* error = nil;
            [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
            [mNSUserDefaults setValue:nil forKey:CALENDER_DATA];
        }
    }];
}

#pragma mark - 更新資料(用於每次登入後做保修時間的差異判斷更新)
-(BOOL)updateUserData:(NSDictionary *)data{
    [mNSUserDefaults setValue:data forKey:USER_DATA];
    if(data){
        [self setSc:data];
    }
    return [mNSUserDefaults synchronize];
}

#pragma mark - 獲得用戶身分證字號
-(NSString *)getUserId{
    return (NSString *)[mNSUserDefaults objectForKey:USER_ID];
}

#pragma mark - 獲得車牌號碼
-(NSString *)getCarId{
    return (NSString *)[mNSUserDefaults objectForKey:CAR_ID];
}

#pragma mark - 獲得使用者資料
-(NSDictionary *)getUserData{
    return (NSDictionary *)[mNSUserDefaults objectForKey:USER_DATA];
}

#pragma mark - 返回true代表需要登入，反之則為已經登入的狀態
-(BOOL)shouldLogin{
    
    NSString *mNSString=[self getUserId];
    if(!mNSString||[mNSString isEqualToString:@""]){
        return YES;
    }
    return NO;
}

#pragma mark - 登出方法
-(BOOL)logout{
    return  [self putUserId:nil andCarId:nil andData:nil];
}


@end
