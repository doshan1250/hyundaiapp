//
//  NewsListCell.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *alreadyReadIcon;
@property (nonatomic, strong) IBOutlet UILabel *lbTitle;
@property (nonatomic, strong) IBOutlet UILabel *lbDate;
-(void)hideAlreadyReadIcon;
-(void)showAlreadyReadIcon;
@end


