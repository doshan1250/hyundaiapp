//
//  VideoViewController.h
//  hyundaiapp
//
//  Created by RHZ Webber on 2015/9/25.
//  Copyright © 2015年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HyundaiViewController.h"

@interface VideoViewController : HyundaiViewController
{
    IBOutlet UITableView *_videoTable;
    
    IBOutlet UIButton *_btnTaiwan;
    IBOutlet UIButton *_btnWorldwide;
}

@end
