//
//  AppDelegate+Facebook.m
//  HyundaiApp
//
//  Created by Webber Chuang on 2015/5/19.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "AppDelegate+Facebook.h"

@implementation AppDelegate (Facebook)

- (void)doFacebookLogin
{
    // 已登入
    if ([FBSDKAccessToken currentAccessToken])
    {
        [self getFacebookUserInfo];
    }
    // 未登入
    else
    {
        [self.fbLoginMgr logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error)
            {
                // Process error
            }
            else if (result.isCancelled)
            {
                // Handle cancellations
            }
            else
            {
                if ([result.grantedPermissions containsObject:@"email"]) {
                    [self getFacebookUserInfo];
                }
            }
        }];
    }
}

- (void)doFacebookLogout
{
    [self.fbLoginMgr logOut];
}

- (void)doFacebookShare:(NSDictionary *)shareData
{
    self.fbShareData = shareData;
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"])
    {
        [self facebookShareAPI];
    }
    else
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error)
            {
                if ([result.grantedPermissions containsObject:@"publish_actions"]) {
                    [self facebookShareAPI];
                } else {
                    [self facebookShareDialog];
                }
            }
        }];
    }
}

#pragma mark - Private
- (void)getFacebookUserInfo
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
     }];
}

- (FBSDKShareLinkContent *)buildShareContent
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle = [self.fbShareData objectForKey:@"title"];
    content.contentDescription = [self.fbShareData objectForKey:@"description"];
    content.imageURL = [NSURL URLWithString:[self.fbShareData objectForKey:@"photo"]];
    content.contentURL = [NSURL URLWithString:[self.fbShareData objectForKey:@"link"]];
    
    return content;
}

- (void)facebookShareDialog
{
    [FBSDKShareDialog showFromViewController:self.window.rootViewController withContent:[self buildShareContent] delegate:nil];
}

- (void)facebookShareAPI
{
    [FBSDKShareAPI shareWithContent:[self buildShareContent] delegate:self];
}

#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"Done");
    [Utilities showMessage:@"分享成功" message:nil];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"Error");
    [Utilities showMessage:@"分享失敗" message:nil];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"Cancel");
    [Utilities showMessage:@"取消分享" message:nil];
}

@end
