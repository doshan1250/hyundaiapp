//
//  main.m
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/28.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
