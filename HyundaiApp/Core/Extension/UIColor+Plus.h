//
//  UIColor+Plus.h
//  Wyeth
//
//  Created by Wei-Po Chuang on 12/4/22.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Plus)

+ (UIColor *)colorHex:(NSString *)hexStr;

@end
