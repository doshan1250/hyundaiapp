//
//  VideoViewCell2.m
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/28.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import "VideoViewCell2.h"

@implementation VideoViewCell2

- (IBAction)clickItem1:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickVideo:)])
        [self.delegate clickVideo:self.tag1];
}

@end
