//
//  DataEngine.h
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 Corma. All rights reserved.
//

#import "MKNetworkEngine.h"

typedef void (^DataResponseBlock)(NSString *response);
typedef void (^ImageResponseBlock)(UIImage *response);

@interface DataEngine : MKNetworkEngine

- (MKNetworkOperation *)getDataFromServer:(NSMutableDictionary *)params path:(NSString *)path isPost:(BOOL)isPost onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock )errorBlock;

- (MKNetworkOperation *)sendImageToServer:(NSMutableDictionary *)params path:(NSString *)path images:(NSArray *)imageArray onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getDataFromUrl:(NSString *)url params:(NSMutableDictionary *)params isPost:(BOOL)isPost onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getImageFromUrl:(NSString *)url params:(NSMutableDictionary *)params isPost:(BOOL)isPost onCompletion:(ImageResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock;

@end

