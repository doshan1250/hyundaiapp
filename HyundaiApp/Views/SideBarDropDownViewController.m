//
//  SideBarDropDownViewController
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/18.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "SideBarDropDownViewController.h"

@interface SideBarDropDownViewController (){
    id<SideBarDelegate> _delegate;
}
@property (strong, nonatomic) IBOutlet UIView *animation;

@end

@implementation SideBarDropDownViewController
@synthesize animation=_animation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.view.frame=CGRectMake(0, 63, 320, 523);
    [self viewSet];
}

-(void)viewSet{
//    self.animation.frame=CGRectMake(0, -247, 0, 0);
    [self.animation setAlpha:0.0];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
   
}

-(void)animationWithView{
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
//                         self.animation.frame=CGRectMake(0, 0, 320, 247);
                            [self.animation setAlpha:1.0];
                     } completion:^(BOOL finish){
                         
                         //NSLog(@"動畫結束");
                         
                     }];
   
    [UIView commitAnimations];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self viewSet];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)exitWithAnimation:(UIButton *)sender
{
    [_delegate exitWithAnimation:[sender tag]];
}

- (IBAction)dismiss:(UIButton *)sender {
    [_delegate dismiss];
}

-(void)setDelegate:(id<SideBarDelegate>) delegate{
    _delegate=delegate;
}


@end
