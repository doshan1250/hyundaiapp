//
//  LoginStatus.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/19.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginStatus <NSObject>
#pragma 回傳true代表繼續動作，false阻斷之後SettingController之後的行為（例如跳Alert等等..
-(BOOL)success;
-(BOOL)fail;

@end
