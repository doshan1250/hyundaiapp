//
//  MapForGoogleAPIViewController.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnctionTool.h"
#import "LocationPoint.h"
#import <MapKit/MapKit.h>
#import "PlacePin.h"
#import <CoreLocation/CoreLocation.h>

@interface MapForGoogleAPIViewController : HyundaiViewController


-(void)setOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest;

//以後如果萬一要導航可以用到
//@property(strong,nonatomic)NSMutableArray *lines;
@property(strong,nonatomic)MKPolylineView *routeLineView;

-(void)setType:(NSInteger)type;
@end
