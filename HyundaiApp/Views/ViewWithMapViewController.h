//
//  ViewWithMapViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/17.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol NavMapDelegate <NSObject>

-(void)navForOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest;


@end

@interface ViewWithMapViewController : UIViewController{
    NSString *_tel;
}
@property (strong,nonatomic) id<NavMapDelegate> delegate;
@property (strong, nonatomic) NSString  *tel;

-(void)enableNavButton:(BOOL)enable;
-(void)setOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest;

-(void)setTitleForParking:(NSString *)title andAlladdress:(NSString *)alladdress andSummary:(NSString *)summary andTel:(NSString *)tel andIcon:(NSString *)imageNamed;
-(void)setTitleForGas:(NSString *)title andAlladdress:(NSString *)alladdress andIcon:(NSString *)imageNamed andDis:(NSString *)dis;

-(UIButton *)getnavMapBtn;
@end
