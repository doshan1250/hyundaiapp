//
//  DataEngine.m
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 Corma. All rights reserved.
//

#import "DataEngine.h"

@implementation DataEngine

- (id)initWithHostName:(NSString *)hostName
{
    self = [super initWithHostName:hostName];
    
    if (self)
    {
        //[self useCache];
    }
    
    return self;
}

- (MKNetworkOperation *)getDataFromServer:(NSMutableDictionary *)params path:(NSString *)path isPost:(BOOL)isPost onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock
{
    NSString *_method = isPost ? @"POST" : @"GET";
    
    NSLog(@"URL:<%@> %@, %@", _method, path, params);
    
    MKNetworkOperation *_op = [self operationWithPath:path params:params httpMethod:_method];
    __block BOOL _isCached = NO;
    [_op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        if ([completedOperation isCachedResponse])
        {
            completionBlock([completedOperation responseString]);
            _isCached = YES;
        }
        else
        {
            if (!_isCached)
                completionBlock([completedOperation responseString]);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        errorBlock(error);
    }]; 
    
    [self enqueueOperation:_op];
    return _op;
}

- (MKNetworkOperation *)sendImageToServer:(NSMutableDictionary *)params path:(NSString *)path images:(NSArray *)imageArray onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *_op = [self operationWithPath:path params:params httpMethod:@"POST"];
    
    for (int i=0; i<[imageArray count]; i++)
    {
        NSData *_imgData = UIImagePNGRepresentation([imageArray objectAtIndex:i]);
        NSString *_filename = [NSString stringWithFormat:@"image%d.png", i+1];
        [_op addData:_imgData forKey:@"pic" mimeType:@"image/png" fileName:_filename];
    }
    
    __block BOOL _isCached = NO;
    [_op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        if ([completedOperation isCachedResponse])
        {
            completionBlock([completedOperation responseString]);
            _isCached = YES;
        }
        else
        {
            if (!_isCached)
                completionBlock([completedOperation responseString]);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:_op];
    return _op;

}

- (MKNetworkOperation *)getDataFromUrl:(NSString *)url params:(NSMutableDictionary *)params isPost:(BOOL)isPost onCompletion:(DataResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock
{
    NSString *_method = isPost ? @"POST" : @"GET";
    
    NSLog(@"URL:<%@> %@, %@", _method, url, params);
    
    MKNetworkOperation *_op = [self operationWithURLString:url params:params httpMethod:_method];
    __block BOOL _isCached = NO;
    [_op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        if ([completedOperation isCachedResponse])
        {
            completionBlock([completedOperation responseString]);
            _isCached = YES;
        }
        else
        {
            if (!_isCached)
                completionBlock([completedOperation responseString]);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:_op];
    return _op;
}

- (MKNetworkOperation *)getImageFromUrl:(NSString *)url params:(NSMutableDictionary *)params isPost:(BOOL)isPost onCompletion:(ImageResponseBlock)completionBlock onError:(MKNKErrorBlock)errorBlock;
{
    NSString *_method = isPost ? @"POST" : @"GET";
    
    NSLog(@"URL:<%@> %@, %@", _method, url, params);
    
    MKNetworkOperation *_op = [self operationWithURLString:url params:params httpMethod:_method];
    __block BOOL _isCached = NO;
    [_op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        if ([completedOperation isCachedResponse])
        {
            completionBlock([completedOperation responseImage]);
            _isCached = YES;
        }
        else
        {
            if (!_isCached)
                completionBlock([completedOperation responseImage]);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:_op];
    return _op;
}


- (int)cacheMemoryCost
{
    return 10;
}

- (NSString *)cacheDirectoryName
{
    NSArray *_paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *_documentsDirectory = [_paths objectAtIndex:0];
    NSString *_cacheDirectory = [_documentsDirectory stringByAppendingString:@"/Images"];
    return _cacheDirectory;
}

@end
