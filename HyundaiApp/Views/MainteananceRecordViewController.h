//
//  mainteananceRecordViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/22.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainteananceRecordDelegate.h"

@interface MainteananceRecordViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *tv_content;
@property (strong,nonatomic) id<MainteananceRecordDelegate> delegate;

@end
