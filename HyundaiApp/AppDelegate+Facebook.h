//
//  AppDelegate+Facebook.h
//  HyundaiApp
//
//  Created by Webber Chuang on 2015/5/19.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Facebook) <FBSDKSharingDelegate>

- (void)doFacebookLogin;
- (void)doFacebookLogout;
- (void)doFacebookShare:(NSDictionary *)shareData;

@end
