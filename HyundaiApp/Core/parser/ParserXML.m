//
//  ParserXML.m
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "ParserXML.h"

#import "XMLReader.h"

#import "NSString+Plus.h"

@implementation ParserXML

+ (id)nodeValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
    
	id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen - 1; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return [tmp objectForKey:[arPath objectAtIndex:(nLen -1)]];
}

+ (NSMutableArray *) arrValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [ParserXML nodeValue:node ofPath:path];
}

+ (NSDictionary *) dictValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
    
    id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return tmp;
}

+ (NSString *) stringValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [ParserXML nodeValue:node ofPath:path];
}

+ (int) intValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [[ParserXML nodeValue:node ofPath:path] intValue];
}

- (id)initWithData:(NSData *)data
{
	self = [super init];
	if (self)
    {
        NSError *parseError = nil;
        dictRes = [XMLReader dictionaryForXMLData:data error:&parseError];
		[self checkError:parseError];
	}
	return self;
}

- (id)initWithString:(NSString *)data
{
	self = [super init];
	if (self)
    {
        NSError *parseError = nil;
        dictRes = [XMLReader dictionaryForXMLString:data error:&parseError];
		[self checkError:parseError];
	}
	return self;
}

- (void)dealloc 
{
	if (dictRes) {		
		dictRes = nil;
	}
}

- (void)checkError:(NSError *)parseError
{
	errParser = (parseError != nil);
    
	if (errParser)
    {
		NSLog(@"ParserXML: checkErrorParser: 資料 Parser 錯誤");
    }
    else
    {
        if ([dictRes objectForKey:@"Configuration"] != nil)
        {
            
        }
        else if ([dictRes objectForKey:@"Result"] != nil)
        {
            if ([[dictRes objectForKey:@"Result"] isKindOfClass:[NSString class]])
                return;
            
            if ([[[dictRes objectForKey:@"Result"] allKeys] containsObject:@"Err"])
            {
                errCode = [self stringOfPath:@"Result/Err/Code"];
                errMsg = [self stringOfPath:@"Result/Err/Msg"];
            }
            else if ([[[dictRes objectForKey:@"Result"] allKeys] containsObject:@"Code"])
            {
                errCode = [self stringOfPath:@"Result/Code"];
                errMsg = [self stringOfPath:@"Result/Message"];
            }
            else
            {
                errCode = @"0";
                errMsg = @"";
            }
        }
        
        if ([errCode intValue] != 0)
            NSLog(@"ParserXML: checkErrorParser: 資料 Parser 正確, Server Error Code: %@", errCode); 
    }
}

- (void)checkErrorElement:(NSDictionary *)root
{
}

- (id)nodeOfPath:(NSString *)path
{
	return [ParserXML nodeValue:dictRes ofPath:path];
}

- (NSMutableArray *)arrOfPath:(NSString *)path
{
    return [ParserXML arrValue:dictRes ofPath:path];
}

- (NSDictionary *)dictOfPath:(NSString *)path
{
    return [ParserXML dictValue:dictRes ofPath:path];
}

- (NSString *)stringOfPath:(NSString *)path
{
    return [ParserXML stringValue:dictRes ofPath:path];
}

- (int)intOfPath:(NSString *)path
{
    return [ParserXML intValue:dictRes ofPath:path];
}

@end
