//
//  ConnctionTool.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ConnctionTool.h"

@interface ConnctionTool(){
    id<ConnctionDelegate> _delegate;
}

@end

@implementation ConnctionTool

-(void)beginDownload:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (connection) {
        self.data = [NSMutableData data];
    }
    else {
        if(_delegate){
            [_delegate getDataFail:Error_NoConnction];
        }
    }
}

#pragma mark - NSURLConnectionDataDelegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog( @"接收到回應");
    [self.data setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //NSLog( @"下載中");
    [self.data appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog( @"下載完成");
    if(_delegate){
        [_delegate getDataSuccess:[self.data copy]];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
      NSLog( @"下載失敗");
    if(_delegate){
        [_delegate getDataFail:Error_Fail];
        [_delegate getErrorMsg:error];
    }
    
}

-(void)setDelegate:(id<ConnctionDelegate>)delegate{
    _delegate=delegate;
}

@end
