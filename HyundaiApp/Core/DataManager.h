//
//  DataManager.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

#define API_NotifyName      @"HyundaiDataNotify"

//#define API_HOST            @"http://projects.linkinedge.com/hyundaiapp/api/%@"
#define API_HOST            @"http://app.hyundai-motor.tw/api/%@"
#define API_News            @"GetNews.ashx"
#define API_NewsDetail      @"GetNews.ashx"

#define API_GasStation      @"Getgas.ashx"
#define API_GasPrice        @"getprice.ashx"

#define API_ParkingArea     @"GetAddress.ashx"
#define API_ParkingData     @"GetParkService.ashx"
#define API_CarInfo         @"GetCarType.ashx"
//
#define API_GasData         @"getgas.ashx"
#define API_UserLogin       @"CarUserLogin.ashx"
#define API_CarFix          @"GetCarFix.ashx"
#define API_Reservation     @"BookingFix.ashx"
#define API_GetFixPlace     @"GetFixPlace.ashx"
#define API_GetFans         @"getfansfeed.ashx"
#define API_BookingInfo     @"getBookingInfo.ashx"
#define API_RegToken        @"SetDeviceToken.ashx"
#define API_GetNowBookingCarInfo        @"http://waynetest.no-ip.org/newhyundaiapp/api/getNowBookingCarInfo.ashx"
/*
 失敗：{"ERROR":"查無預約資料!!"}
 成功：{"dBookingDate":"2016-04-30 09:00","cSfIdn":"CG100","cConfirmStatus":"01"}

 */
#define API_GetBookingBalanceInfo        @"http://waynetest.no-ip.org/newhyundaiapp/api/getBookingBalanceInfo.ashx"
/*
 {"RESULT11":["2016-04-30 19:00","19",""],"RESULT10":["2016-04-30 18:00","18",""],"RESULT12":["2016-04-30 20:00","20",""],"RESULT7":["2016-04-30 15:00","15",""],"RESULT8":["2016-04-30 16:00","16",""],"OK":"資料讀取完成","RESULT5":["2016-04-30 13:00","13",""],"RESULT6":["2016-04-30 14:00","14",""],"RESULT9":["2016-04-30 17:00","17",""],"RESULT0":["2016-04-30 08:30","8",""],"RESULT3":["2016-04-30 11:00","11",""],"RESULT4":["2016-04-30 12:00","12",""],"RESULT1":["2016-04-30 09:00","9",""],"RESULT2":["2016-04-30 10:00","10",""]}

 */

#define API_BookingCarService  @"http://waynetest.no-ip.org/newhyundaiapp/api/BookingFix.ashx"


// GetBranches.ashx
// RType: 展示中心:1, 服務廠:2
// CType: 乘用車:1, 商用車:2
// ANum: 台北:1, 新北:2, 桃竹:3, 中彰:4, 嘉南:5, 高屏:6, 宜花東:7
#define API_ServiceData     @"GetBranches.ashx"

typedef enum
{
    Request_News                = 0,
    Request_GasStation          = 1,
    Request_GasPrice            = 2,
    Request_ParkingArea         = 3,
    Request_ParkingData         = 4,
    Request_CarInfo             = 5,
    Request_ServiceData         = 6,
    Request_NewsDetail          = 7,
    Request_GasData             = 8,
    Request_UserLogin           =9,
    Request_CarFix              =10,
    Request_Reservation         =11,
    Request_GetFixPlace         =12,
    Request_ParkingDataMap      = 13,
    Request_getfans             =14,
    Request_directions          =15,
    Request_bookingInfo         =16,
    Reuqest_RegToken            =17,
    
    
    // 4/23
    Reuqest_GetNowBookingCarInfo            =18,
    Request_GetBookingBalanceInfo = 19,
    Request_BookingCarService = 20,
    
    
} EnumRequestAPI;

@interface DataManager : NSObject
{
    
}

- (void)addObserverData:(NSObject *)obj callback:(NSString *)func;
- (void)delObserverData:(NSObject *)obj;

- (void)fireRequest:(EnumRequestAPI)action postData:(NSMutableDictionary *)data;

@end
