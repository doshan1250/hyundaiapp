//
//  Utilities.m
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 POSINCO. All rights reserved.
//

#import "Utilities.h"

#import "Base64.h"
#import "MD5.h"
#include <sys/utsname.h>

@implementation Utilities

#pragma mark - Methods for Device Info
+ (BOOL)deviceIsIOS6
{
    CGFloat _version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (_version >= 6.0 && _version < 7.0)
        return YES;
    return NO;
}

+ (BOOL)deviceIsIOS7
{
    CGFloat _version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (_version >= 7.0 && _version < 8.0)
        return YES;
    return NO;
}

+ (BOOL)deviceIsIOS8
{
    CGFloat _version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (_version >= 8.0)
        return YES;
    return NO;
}

+ (BOOL)deviceIs4Inch
{
    CGRect _rect = [UIScreen mainScreen].bounds;
    if (_rect.size.width == 568 || _rect.size.height == 568)
        return YES;
    return NO;
}

+ (BOOL)deviceIsIphone6Plus
{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1136);
}

+ (BOOL)deviceIsIphone
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        return YES;
    return NO;
}

+ (BOOL)deviceIsIpod {
    NSString *_ipod = @"iPod";
    NSString *_deviceModel = [UIDevice currentDevice].model;
    if ([_deviceModel rangeOfString:_ipod].location == NSNotFound)
        return NO;
    
    return YES;
}

+ (BOOL)deviceIsIpad
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return YES;
    
    return NO;
}

+ (NSString *)deviceVendorID
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+ (UIView *)getKeyWindow
{
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    if (mainWindow == nil)
    {
        id<UIApplicationDelegate>delegate = [UIApplication sharedApplication].delegate;
        mainWindow = [delegate window];
    }
    
    UIView* keyWindow = nil;
    if (mainWindow != nil && mainWindow.rootViewController != nil && mainWindow.rootViewController.view != nil)
        keyWindow = mainWindow.rootViewController.view;
    
    return keyWindow;
}

+ (NSString *)getAppVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString *)getAppBuild
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

#pragma mark - Check
+ (BOOL)isStringNumeric:(NSString *)text
{
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:text];
    return [alphaNums isSupersetOfSet:inStringSet];
}

+ (BOOL)isValidEmail:(NSString *)email
{
    BOOL stricterFilter = YES;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Date
+ (NSDate *)currentDate:(NSInteger)timeDiff
{
    NSDate *gmtDate = [NSDate date];
    NSTimeInterval timeZoneOffset = [[NSTimeZone timeZoneWithName:@"H"] secondsFromGMTForDate:gmtDate];
    NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneOffset-timeDiff];
    
    return localDate;
}

+ (NSString *)covertDateTimeToString:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"H"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *dateInput = [dateFormatter dateFromString:date];
    int dateSec = abs((int)[dateInput timeIntervalSinceDate:[Utilities currentDate:0]]);
    
    NSString *dateOutput;
    
    if (dateSec < 60)
    {
        dateOutput = @"幾秒鐘前";
    }
    else if (dateSec >= 60 && dateSec < 3600)
    {
        int min = round(dateSec/60);
        dateOutput = [NSString stringWithFormat:(min > 1) ? @"%d分鐘前" : @"%d分鐘前", min];
    }
    else if (dateSec >= 3600 && dateSec < (3600*24))
    {
        int hour = round(dateSec/3600);
        dateOutput = [NSString stringWithFormat:(hour > 1) ? @"%d小時前" : @"%d小時前", hour];
    }
    else if (dateSec >= (3600*24) && dateSec < (3600*24*7))
    {
        int day = round(dateSec/(3600*24));
        dateOutput = [NSString stringWithFormat:(day > 1) ? @"%d天前" : @"%d天前", day];
    }
    else
    {
        [dateFormatter setDateFormat:@"yyyy"];
        int year = [[dateFormatter stringFromDate:dateInput] intValue];
        
        [dateFormatter setDateFormat:@"MM"];
        int month = [[dateFormatter stringFromDate:dateInput] intValue];
        
        [dateFormatter setDateFormat:@"dd"];
        int day = [[dateFormatter stringFromDate:dateInput] intValue];
        
        dateOutput = [NSString stringWithFormat:@"%d年%d月%d日", year, month, day];
    }
    
    dateFormatter = nil;
    
    return dateOutput;
}

+ (NSTimeInterval)time
{
    return [[Utilities currentDate:0] timeIntervalSince1970];
}

#pragma mark - Math
+ (NSString *)formatNumericWithComma:(int)volume
{
    if (volume == 0)
    {
        return @"0";
    }
    else
    {
        NSMutableString *volumeStr = [NSMutableString string];
        
        int common = volume;
        int tail = volume % 1000;
        
        while (common > 0)
        {
            if(tail == common)
            {
                [volumeStr insertString:[NSString stringWithFormat:@"%d", tail] atIndex:0];
                break;
            }
            
            if (tail == 0)
            {
                [volumeStr insertString:@"000" atIndex:0];
            }
            else if(tail < 10)
            {
                [volumeStr insertString:[NSString stringWithFormat:@"00%d", tail] atIndex:0];
            }
            else if(tail < 100)
            {
                [volumeStr insertString:[NSString stringWithFormat:@"0%d", tail] atIndex:0];
            }
            else
            {
                [volumeStr insertString:[NSString stringWithFormat:@"%d", tail] atIndex:0];
            }
            
            common = common / 1000;
            tail = common % 1000;
            
            if (common > 0)
            {
                [volumeStr insertString:@"," atIndex:0];
            }
        }
        return volumeStr;
    }
}

#pragma mark - Cal Distance
+ (NSString *)getDistance:(double)lat1 mylng1:(double)lng1 mylat2:(double)lat2 mylng2:(double)lng2
{
    float ran_lat1 = lat1 * 3.1415926 / 180;
    float ran_lat2 = lat2 * 3.1415926 / 180;
    float ran_lng1 = lng1 * 3.1415926 / 180;
    float ran_lng2 = lng2 * 3.1415926 / 180;
    
    float a = ran_lat1 - ran_lat2;
    float b = ran_lng1 - ran_lng2;
    
    float s = 2 * asin(sqrt(pow(sin(a/2), 2) + cos(ran_lat1) * cos(ran_lat2) * pow(sin(b/2), 2)));
    s = s * 6378.137;
    s = round(s * 10000) / 10;
    
    // Meter to Yard
    //	s = s * 1.0936113;
    
    NSString *retStr = (s > 999) ? [NSString stringWithFormat:@"%.2f公里", (s/1000)] : [NSString stringWithFormat:@"%d公尺", (int)s];
    
    return retStr;
}

+ (float)getDistanceNonFormat:(double)lat1 mylng1:(double)lng1 mylat2:(double)lat2 mylng2:(double)lng2
{
    float ran_lat1 = lat1 * 3.1415926 / 180;
    float ran_lat2 = lat2 * 3.1415926 / 180;
    float ran_lng1 = lng1 * 3.1415926 / 180;
    float ran_lng2 = lng2 * 3.1415926 / 180;
    
    float a = ran_lat1 - ran_lat2;
    float b = ran_lng1 - ran_lng2;
    
    float s = 2 * asin(sqrt(pow(sin(a/2), 2) + cos(ran_lat1) * cos(ran_lat2) * pow(sin(b/2), 2)));
    s = s * 6378.137;
    s = round(s * 10000) / 10;
    
    // Meter to Yard
    //	s = s * 1.0936113;
    
    return s;
}

#pragma mark - AlertView
+ (void)showMessage:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
    [alert show];
    alert = nil;
}

#pragma mark - Methods for NSUserDefault
+ (void)userDefaultSaveData:(id) data forKey:(NSString *)key
{
    NSUserDefaults *_userDefault = [NSUserDefaults standardUserDefaults];
    [_userDefault setObject:data forKey:key];
    [_userDefault synchronize];
}

+ (id)userDefaultGetDataByKey:(NSString *)key
{
    NSUserDefaults *_userDefault = [NSUserDefaults standardUserDefaults];
    return [_userDefault objectForKey:key];
}

+ (void)userDefaultClearKey:(NSString *)key
{
    NSUserDefaults *_userDefault = [NSUserDefaults standardUserDefaults];
    [_userDefault removeObjectForKey:key];
    [_userDefault synchronize];
}

#pragma mark - Methods for JSON
+ (id) convertJSONStringToArray:(NSString *)string
{
    NSError *error;
    
    NSData *_data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *_temp = [NSJSONSerialization JSONObjectWithData:_data options:0 error:&error];
    
    if (error)
        NSLog(@"JSON Error: %@", [error description]);
    
    return error ? error : _temp;
}

+ (id) convertJSONStringToDictionary:(NSString *)string
{
    NSError *error;
    
    NSData *_data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *_temp = [NSJSONSerialization JSONObjectWithData:_data options:0 error:&error];
    
    if (error)
        NSLog(@"JSON Error: %@", [error description]);
    
    return error ? error : _temp;
}

#pragma mark - Directory
+ (NSString *)documentDirectory
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)cacheDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)documentDirectoryByName:(NSString *)dirName
{
	BOOL isDirectory;
    NSError *error = nil;
	NSString *directory = [[Utilities documentDirectory] stringByAppendingPathComponent:dirName];
	
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory isDirectory:&isDirectory])
    {
		[[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:NO attributes:nil error:&error];
	}
	
    return directory;
}

+ (NSString *)cacheDirectoryByName:(NSString *)dirName
{
	BOOL isDirectory;
    NSError *error = nil;
	NSString *directory = [[Utilities cacheDirectory] stringByAppendingPathComponent:dirName];
	
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory isDirectory:&isDirectory])
    {
		[[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:NO attributes:nil error:&error];
	}
	
    return directory;
}

+(NSString *)bundlePath:(NSString *)fileName {
    return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:fileName];
}

+(NSString *)documentsPath:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}


#pragma mark - Image
+ (void)makeCircleMask:(UIView *)view
{
    CAShapeLayer *aCircle=[CAShapeLayer layer];
    aCircle.path=[UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:view.frame.size.height/2].CGPath;
    
    aCircle.fillColor = [UIColor blackColor].CGColor;
    view.layer.mask = aCircle;
}

@end
