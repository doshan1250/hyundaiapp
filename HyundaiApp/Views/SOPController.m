//
//  SOPController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "SOPController.h"
#import "SOPViewCell.h"

#import "ContactController.h"
#import "RoadSideController.h"

#define CALL_119    @"您是否要撥打 119"
#define CALL_110    @"您是否要撥打 110"


@interface SOPController () <UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView *_sopTable;
    NSArray *_arrItem;
}

@end

@implementation SOPController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrItem = @[@"車後放置三角錐", @"撥打110報案", @"現場拍照/攝影", @"向現場員警備案，留下相關人員資料", @"現場有人受傷，打119", @"聯絡維修/營業員", @"聯絡拖吊服務", @"查詢保卡，聯絡保險公司"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"道路救援";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
}


#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SOPViewCell";
    SOPViewCell *cell = (SOPViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SOPViewCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (SOPViewCell *) currentObject;
                break;
            }
        }
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgView];
    }
    [cell.imgIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"sop_step%d_icon.png", indexPath.row+1]]];
    [cell.lbTitle setText:[_arrItem objectAtIndex:indexPath.row]];
    [cell.label_step setText:[NSString stringWithFormat:@"%d",(indexPath.row+1)]];
    
    if(indexPath.row==7){
        [cell.img_down setHidden:YES];
    }else{
        [cell.img_down setHidden:NO];
    }
    
    if (indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 7){
        [cell.imgNext setHidden:YES];
    }else {
        [cell.imgNext setHidden:NO];
    }
    
    if(indexPath.row == 1 ||indexPath.row == 4){
        [cell.img_call setHidden:NO];
    }else{
        [cell.img_call setHidden:YES];
    }
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row)
    {
        case 1:
            [[[UIAlertView alloc] initWithTitle:nil message:CALL_110 delegate:self cancelButtonTitle:@"不要" otherButtonTitles:@"好", nil] show];
            break;
            
        case 2:
        {
            [self openCamera];
            break;
        }
        case 4:
            [[[UIAlertView alloc] initWithTitle:nil message:CALL_119 delegate:self cancelButtonTitle:@"不要" otherButtonTitles:@"好", nil] show];
            break;
        case 5:
        {
            ContactController *vc = [[ContactController alloc] initWithNibName:@"ContactController" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
            vc = nil;
            break;
        }
        case 6:
        {
            RoadSideController *vc = [[RoadSideController alloc] initWithNibName:@"RoadSideController" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
            vc = nil;
            break;
        }
        case 0:
        case 3:
        case 7:
        default:
            break;
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    {
        if([alertView.message isEqualToString:CALL_110]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://110"]];
        }else if([alertView.message isEqualToString:CALL_119]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://119"]];
        }
    }
}

#pragma mark - Camera
- (void)openCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:),nil);
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
