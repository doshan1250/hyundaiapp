//
//  XMLReader.m
//
//  Created by Troy on 9/18/10.
//  Copyright 2010 Troy Brant. All rights reserved.
//

#import "XMLReader.h"
#import "NSString+Plus.h"

NSString *const kXMLReaderTextNodeKey = @"text";

@interface XMLReader (Internal)

- (id)initWithError:(NSError **)error;
- (NSDictionary *)objectWithData:(NSData *)data;

@end


@implementation XMLReader

#pragma mark -
#pragma mark Public methods

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)error
{
    XMLReader *reader = [[XMLReader alloc] initWithError:error];
    NSDictionary *rootDictionary = [reader objectWithData:data];
    [reader release];
    return rootDictionary;
}

+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)error
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [XMLReader dictionaryForXMLData:data error:error];
}

#pragma mark -
#pragma mark Parsing

- (id)initWithError:(NSError **)error
{
    if (self = [super init])
    {
        errorPointer = error;
    }
    return self;
}

- (void)dealloc
{
    [dictionaryStack release];
    [textInProgress release];
    [super dealloc];
}

- (NSDictionary *)objectWithData:(NSData *)data
{
    // Clear out any old data
    [dictionaryStack release];
    [textInProgress release];
    
    dictionaryStack = [[NSMutableArray alloc] init];
    textInProgress = [[NSMutableString alloc] init];
    
    // Initialize the stack with a fresh dictionary
    [dictionaryStack addObject:[NSMutableDictionary dictionary]];
    
    // Parse the XML
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:YES];
    
    BOOL success = [parser parse];
    
    [parser release];
    
    // Return the stack's root dictionary on success
    if (success)
    {
        NSDictionary *resultDict = [dictionaryStack objectAtIndex:0];
        return resultDict;
    }
    
    return nil;
}

#pragma mark -
#pragma mark NSXMLParserDelegate methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    // Get the dictionary for the current level in the stack
    NSMutableDictionary *parentDict = [dictionaryStack lastObject];
    
    // Create the child dictionary for the new element, and initilaize it with the attributes
    NSMutableDictionary *childDict = [NSMutableDictionary dictionary];
    [childDict addEntriesFromDictionary:attributeDict];
    
    hasNodeAttribute = ([attributeDict count] > 0);
    
    // If there's already an item for this key, it means we need to create an array
    id existingValue = [parentDict objectForKey:elementName];
    
    if (existingValue)
    {
        NSMutableArray *array = nil;
        if ([existingValue isKindOfClass:[NSMutableArray class]])
        {
            // The array exists, so use it
            array = (NSMutableArray *) existingValue;
            
            nodeParentDict = parentDict;
            txtParentDict = nil;
            txtParentElement = elementName;
            
            // Add the new child dictionary to the array
            [array addObject:childDict];
        }
        else
        {
            // Create an array if it doesn't exist
            array = [NSMutableArray array];
            [array addObject:existingValue];
            
            // Replace the child dictionary with an array of children dictionaries
            [parentDict setObject:array forKey:elementName];
            
            nodeParentDict = parentDict;
            txtParentDict = parentDict;
            txtParentElement = elementName;
            
            // Add the new child dictionary to the array
            [array addObject:childDict];
        }
    }
    else
    {
        // No existing value, so update the dictionary
        [parentDict setObject:childDict forKey:elementName];
        
        nodeParentDict = parentDict;
        txtParentDict = parentDict;
        txtParentElement = elementName;
    }
    
    // Update the stack
    [dictionaryStack addObject:childDict];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    [textInProgress replaceOccurrencesOfString:@"\t" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [textInProgress length])];
    [textInProgress replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [textInProgress length])];
    
    // Set the text property
    if (txtParentElement)
    {
        if ([textInProgress length] > 0)
        {
            id existsNode = [nodeParentDict objectForKey:txtParentElement];
            
            if ([existsNode isKindOfClass:[NSMutableArray class]])
            {
                NSMutableArray *arr = (NSMutableArray *)existsNode;
                NSMutableArray *newArr = [NSMutableArray array];
                
                for (int i=0; i<[arr count]; ++i)
                {
                    if ([[arr objectAtIndex:i] isKindOfClass:[NSMutableString class]])
                        [newArr addObject:[arr objectAtIndex:i]];
                }
                
                [newArr addObject:textInProgress];
                
                [nodeParentDict setObject:newArr forKey:txtParentElement];
            }
            else if ([existsNode isKindOfClass:[NSMutableString class]])
            {
                NSMutableArray *arr = [NSMutableArray array];
                
                [arr addObject:existsNode];
                [arr addObject:textInProgress];
                
                [nodeParentDict setObject:arr forKey:txtParentElement];
            }
            else
            {
                [nodeParentDict setObject:textInProgress forKey:txtParentElement];
            }
            
            // Reset the text
            [textInProgress release];
            textInProgress = nil;
            
            textInProgress = [[NSMutableString alloc] init];
            
            nodeParentDict = nil;
            txtParentElement = nil;
        }
        else
        {
            if (!hasNodeAttribute)
                [txtParentDict setObject:@"" forKey:txtParentElement];
        }
    }
    
    // Pop the current dict
    [dictionaryStack removeLastObject];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    // Build the text value
    [textInProgress appendString:[string stringByTrimmingWhitespace]];
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    *errorPointer = validationError;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    // Set the error pointer to the parser's error object
    *errorPointer = parseError;
}

@end
