//
//  UIColor+Plus.m
//  Wyeth
//
//  Created by Wei-Po Chuang on 12/4/22.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "UIColor+Plus.h"

@implementation UIColor (Plus)

+ (UIColor *)colorHex:(NSString *)hexStr
{
    NSString *cleanString = [hexStr stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if([cleanString length] == 3)
    {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@", 
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    
    if([cleanString length] == 6)
    {
        cleanString = [NSString stringWithFormat:@"FF%@", cleanString];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float alpha = ((baseValue >> 24) & 0xFF) / 255.0f;
    float red = ((baseValue >> 16) & 0xFF) / 255.0f;
    float green = ((baseValue >> 8) & 0xFF) / 255.0f;
    float blue = ((baseValue >> 0) & 0xFF) / 255.0f;
    
    return [[UIColor alloc] initWithRed:red green:green blue:blue alpha:alpha];
}

@end
