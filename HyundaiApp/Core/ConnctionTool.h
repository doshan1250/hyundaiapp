//
//  ConnctionTool.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    Error_NoConnction=1,
    Error_Fail,
    
} EnumErrorMsg;

@protocol ConnctionDelegate <NSObject>
@required
-(void)getDataSuccess:(NSMutableData *)data;
-(void)getDataFail:(NSInteger )errorMsg;
@optional
-(void)getErrorMsg:(NSError *)error;
@end

@interface ConnctionTool : NSObject

@property (nonatomic, retain) NSMutableData *data;
-(void)beginDownload:(NSString *)urlString;
-(void)setDelegate:(id<ConnctionDelegate>)delegate;
@end
