//
//  ProgressViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/26.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ProgressViewController.h"

@interface ProgressViewController (){
    NSTimer *timer;
}

@end

@implementation ProgressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    timer = [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [timer invalidate];
    timer=nil;
}

-(void)timerFired:(NSTimer *)t{
    [timer invalidate];
    timer=nil;
    if(_delegate){
        [_delegate timeout];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDelegate:(id<ProgressDelegate>)delegate{
    _delegate=delegate;
}

@end
