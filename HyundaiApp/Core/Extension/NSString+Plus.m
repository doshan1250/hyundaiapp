//
//  NSString+Plus.m
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "NSString+Plus.h"
#import "NSData+Plus.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Plus)

#pragma mark - Check

+ (BOOL)isNullorEmpty:(NSString *)str
{
    if (str == nil)
        return YES;
    return [str isEmpty];
}

- (BOOL)isEmpty
{
	return [self isEmptyIgnoringWhitespace:YES];
}

- (BOOL)isEmptyIgnoringWhitespace:(BOOL)ignoreWhitespace
{
	NSString *toCheck = (ignoreWhitespace) ? [self stringByTrimmingWhitespace] : self;
	return [toCheck isEqualToString:@""];
}

- (NSString *)stringByTrimmingWhitespace
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+ (BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Search

- (int)indexOf:(NSString *)str
{
	NSRange range = [self rangeOfString:str];
	if (range.length > 0) {
		return range.location;
	} else {
		return -1;
	}
}

- (BOOL)containsString:(NSString *)str
{    
    NSRange range = [self rangeOfString:str];
    BOOL found = (range.location != NSNotFound);
    
    return found;
}

#pragma mark - Read/Write to File

+ (NSMutableString *)fileGetContens:(NSString *)filePath
{
    // 取得目前 app bundle 的路徑
    NSString *path;    
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    
    path = [resourcePath stringByAppendingPathComponent:filePath];
    
    // 回傳樣版
    return [[NSMutableString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

#pragma mark - AES128

- (NSString *)AES128EncryptWithKey:(NSString *)key
{
	NSData *plainData = [self dataUsingEncoding:NSUTF8StringEncoding];
	NSData *encryptedData = [plainData AES128EncryptWithKey:key];
	
	NSString *encryptedString = [encryptedData base64Encoding];
	
	return encryptedString;
}

- (NSString *)AES128DecryptWithKey:(NSString *)key
{
	NSData *encryptedData = [NSData dataWithBase64EncodedString:self];
	NSData *plainData = [encryptedData AES128DecryptWithKey:key];
	
	NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
	
	return plainString;
}

#pragma mark - MD5

- (NSString *)md5
{
    NSData *stringBytes = [self dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    if (CC_MD5([stringBytes bytes], [stringBytes length], result)) {
        NSMutableString *returnString = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH];
        for (int i=0; i<CC_MD5_DIGEST_LENGTH; i++) {
            [returnString appendFormat:@"%02x", result[i]];
        }
        return [NSString stringWithString:returnString];
    } else {
        return nil;
    }
}

- (NSString*)sha1
{
    NSData *stringBytes = [self dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    if (CC_SHA1([stringBytes bytes], [stringBytes length], result)) {
        NSMutableString *returnString = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH];
        for (int i=0; i<CC_SHA1_DIGEST_LENGTH; i++) {
            [returnString appendFormat:@"%02x", result[i]];
        }
        return [NSString stringWithString:returnString];
    } else {
        return nil;
    }
}

+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (unsigned int)strlen(cStr), result);
    
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", 
            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]           
            ];
}

@end

@implementation NSMutableString (Plus)

- (void)trimCharactersInSet:(NSCharacterSet *)aCharacterSet
{
	// trim front
	NSRange frontRange = NSMakeRange(0, 1);
	while ([aCharacterSet characterIsMember:[self characterAtIndex:0]])
		[self deleteCharactersInRange:frontRange];
	
	// trim back
	while ([aCharacterSet characterIsMember:[self characterAtIndex:([self length] - 1)]])
		[self deleteCharactersInRange:NSMakeRange(([self length] - 1), 1)];
}

- (void)trimWhitespace
{
	[self trimCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
