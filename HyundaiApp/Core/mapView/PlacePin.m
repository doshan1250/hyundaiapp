//
//  PlacePin.m
//  MapViewTest
//
//  Created by ShuHsien Lin on 2013/10/26.
//  Copyright (c) 2013年 Linkinedge. All rights reserved.
//
//- See more at: http://furnacedigital.blogspot.tw/2011/01/mapview-pin.html#sthash.1l1XFqen.dpuf

#import "PlacePin.h"

@implementation PlacePin
@synthesize coordinate, title, subtitle;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coords {
    self = [super init];
    if (self != nil) coordinate = coords;
    
    return self;
}

@end