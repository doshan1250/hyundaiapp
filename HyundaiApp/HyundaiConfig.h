//
//  HyundaiConfig.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "UIColor+Plus.h"

#define IS_IOS7             ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)

#define NAV_SPACING         IS_IOS7 ? -10 : 0

#define HYUNDAI_BG_COLOR    [UIColor colorHex:@"180153"]
#define HYUNDAI_BG_GREY     [UIColor colorHex:@"e6e6e6"]

#define API_NotifyKey       @"key"
#define API_NotifyData      @"data"
#define API_NotifyParams    @"params"

#define NB_I(v)             ([NSNumber numberWithInt:(int)v])