//
//  ListOptionTwoLinesCell.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/19.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListOptionTwoLinesCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *areaName;
@property (strong, nonatomic) IBOutlet UILabel *subInfo;
@end
