//
//  ServiceItem.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceItemDelegate <NSObject>

- (void)clickItemWithIndex:(int)index;

@end

@interface ServiceItem : UIView

- (void)setDelegate:(id<ServiceItemDelegate>)delegate;
- (void)setTitle:(NSString *)titleString andSubTitle:(NSString *)subTitleStirng andDis:(NSString *)disString tag:(int)tag;
-(void)setIconImage:(UIImage *)image;
@end
