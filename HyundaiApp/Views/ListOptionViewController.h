//
//  ListSearchAreaOptionViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/15.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LStableCellSelectDelegate <NSObject>
@required
-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)areaName;
-(void)touchOutSide:(UIView *)view;
@optional
-(void)getTagFirst:(NSInteger)tag;
-(void)backClick:(NSInteger)tag;
@end

@interface ListOptionViewController : UIViewController{

    id<LStableCellSelectDelegate> _delegate;
    
}
-(void)setDelegate:(id<LStableCellSelectDelegate>)delegate;
-(void)setDatas:(NSArray *)datas;
-(void)setExtraDatas:(NSArray *)extraDatas;
-(void)setExtraExtraDatas:(NSArray *)extraExtraDatas;
-(void)reloadData;
-(void)setlabel_title:(NSString *)title;

-(void)setTag:(NSInteger)tag;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
-(void)getSizeAfterTableViewShow;
@end
