//
//  CubeButton.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/9.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CubeButton;

@protocol CubeButtonDelegate <NSObject>

- (void)clickCubeButton:(CubeButton *)cubeBtn;

@end

@interface CubeButton : UIView

- (void)setDelegate:(id<CubeButtonDelegate>)delegate;
- (void)setImgIcon:(UIImage *)imgIcon imgText:(UIImage *)imgText;

@end
