//
//  PlacePin.h
//  MapViewTest
//
//  Created by ShuHsien Lin on 2013/10/26.
//  Copyright (c) 2013年 Linkinedge. All rights reserved.
//- See more at: http://furnacedigital.blogspot.tw/2011/01/mapview-pin.html#sthash.1l1XFqen.dpuf

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PlacePin : NSObject <MKAnnotation> {
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coords;

@end