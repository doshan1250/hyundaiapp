//
//  HyundaiViewController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideBarDropDownViewController.h"

@interface HyundaiViewController : UIViewController
{
    UILabel *_titleView;
}

-(BOOL)clickedHomeEvent;
-(UIView *)titleViewSet;
-(void)exitWithAnimation:(NSInteger)tag;
-(void)pageToPageAcross:(NSInteger)tag;
-(BOOL)loadWithRequest:(NSString *)target;

@end
