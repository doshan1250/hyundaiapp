//
//  NewsDetailController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailController : HyundaiViewController

- (void)setNewsData:(NSDictionary *)data atIndex:(NSInteger)index withList:(NSArray *)arrData;

@end
