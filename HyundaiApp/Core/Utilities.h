//
//  Utilities.h
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 POSINCO. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIColorFromRGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@interface Utilities : NSObject

// Methods for Device Info
+ (BOOL)deviceIsIOS6;
+ (BOOL)deviceIsIOS7;
+ (BOOL)deviceIsIOS8;
+ (BOOL)deviceIs4Inch;
+ (BOOL)deviceIsIphone6Plus;
+ (BOOL)deviceIsIphone;
+ (BOOL)deviceIsIpod;
+ (BOOL)deviceIsIpad;

+ (NSString *)deviceVendorID;
+ (UIView *)getKeyWindow;

+ (NSString *)getAppVersion;
+ (NSString *)getAppBuild;

// Methods for Check
+ (BOOL)isStringNumeric:(NSString *)text;
+ (BOOL)isValidEmail:(NSString *)email;

// Methods for Date
+ (NSDate *)currentDate:(NSInteger)timeDiff;
+ (NSString *)covertDateTimeToString:(NSString *)date;
+ (NSTimeInterval)time;

// Methods for Math
+ (NSString *)formatNumericWithComma:(int)volume;

// Methods for Cal Distance
+ (NSString *)getDistance:(double)lat1 mylng1:(double)lng1 mylat2:(double)lat2 mylng2:(double)lng2;
+ (float)getDistanceNonFormat:(double)lat1 mylng1:(double)lng1 mylat2:(double)lat2 mylng2:(double)lng2;

// Methods for AlertView
+ (void)showMessage:(NSString *)title message:(NSString *)message;

// Methods for NSUserDefault
+ (void)userDefaultSaveData:(id)data forKey:(NSString *)key;
+ (id)userDefaultGetDataByKey:(NSString *)key;
+ (void)userDefaultClearKey:(NSString *)key;

// Methods For JSON
+ (id)convertJSONStringToArray:(NSString *)string;
+ (id)convertJSONStringToDictionary:(NSString *)string;

// Methods For Directory
+ (NSString *)documentDirectory;
+ (NSString *)cacheDirectory;
+ (NSString *)documentDirectoryByName:(NSString *)dirName;
+ (NSString *)cacheDirectoryByName:(NSString *)dirName;

+ (NSString *)bundlePath:(NSString *)fileName;
+ (NSString *)documentsPath:(NSString *)fileName;

// Methods For Image
+ (void)makeCircleMask:(UIView *)view;

@end
