//
//  ListSearchAreaOptionViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/15.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ListOptionViewController.h"
#import "LSOptionCell.h"
#import "LSOptionCell2.h"
#import <QuartzCore/QuartzCore.h>
#import "ListOptionTwoLinesCell.h"

@interface ListOptionViewController ()<UITableViewDelegate, UITableViewDataSource,LStableCellSelectDelegate>{
    NSArray *_datas;
    NSArray *_extraDatas;
    NSArray *_extraExtraDatas;
    NSInteger _tag;
    
}
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UILabel *label_title;
@property (strong, nonatomic) IBOutlet UIView *fixSizeView;

@end

@implementation ListOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void)reloadData{
    [self.table reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.table setDelegate:self];
    [self.table setDataSource:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.table layoutIfNeeded];
    [self getSizeAfterTableViewShow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier ;
    UITableViewCell *cell;
    if (_extraExtraDatas) {
        cellIdentifier= @"LSOptionCell2";
        cell= (LSOptionCell2 *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    else if(_extraDatas){
        cellIdentifier= @"ListOptionTwoLinesCell";
      cell= (ListOptionTwoLinesCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }else{
        cellIdentifier= @"LSOptionCell";
      cell= (LSOptionCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                if (_extraExtraDatas) {
                    cell = (LSOptionCell2 *) currentObject;
                }
                else if(_extraDatas){
                    cell= (ListOptionTwoLinesCell *)currentObject;
                }else{
                    cell = (LSOptionCell *) currentObject;
                }
                break;
            }
        }//end for
    }//end if
    if(_extraDatas){
        ((ListOptionTwoLinesCell *)cell).areaName.text=[_datas objectAtIndex:indexPath.row];
        ((ListOptionTwoLinesCell *)cell).subInfo.text=[_extraDatas objectAtIndex:indexPath.row];
        cell.backgroundView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"area_option_unselect.png"]];
        cell.selectedBackgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"area_option_select.png"]];
    }else{
        ((LSOptionCell *)cell).areaName.text=[_datas objectAtIndex:indexPath.row];
        if (_tag == 2) {
            
            ((LSOptionCell2 *)cell).bookingAmountTitle.layer.cornerRadius = 4;
            
            ((LSOptionCell2 *)cell).bookingAmountTitle.hidden = false;
            ((LSOptionCell2 *)cell).bookingAmountValue.hidden = false;
            if (_extraExtraDatas.count > indexPath.row) {
                ((LSOptionCell2 *)cell).bookingAmountTitle.textColor = [UIColor whiteColor];
                ((LSOptionCell2 *)cell).bookingAmountTitle.adjustsFontSizeToFitWidth = true;
//                ((LSOptionCell2 *)cell).bookingAmountValue.adjustsFontSizeToFitWidth = true;
                
                [((LSOptionCell2 *)cell).rightButton setImage:[UIImage imageNamed:@"可預約件數按鈕2"] forState:UIControlStateSelected];
                
                [((LSOptionCell2 *)cell).rightButton bringSubviewToFront:cell];
                ((LSOptionCell2 *)cell).bookingAmountTitle.text = [NSString stringWithFormat:@" 可預約件數:%@",[_extraExtraDatas objectAtIndex:indexPath.row]];
                
                if ([[_extraExtraDatas objectAtIndex:indexPath.row] intValue] > 0) {
                    ((LSOptionCell2 *)cell).bookingAmountTitle.backgroundColor = UIColorFromRGB(48, 112, 175, 1);
                }
                else{
                ((LSOptionCell2 *)cell).bookingAmountTitle.backgroundColor = UIColorFromRGB(149, 149, 149, 1);
                }
                ((LSOptionCell2 *)cell).bookingAmountValue.text = [_extraExtraDatas objectAtIndex:indexPath.row];
                
            }
          
            
        }
        else{
            cell.backgroundView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"area_option_unselect.png"]];
            cell.selectedBackgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"area_option_select.png"]];
            ((LSOptionCell *)cell).bookingAmountTitle.hidden = true;
            ((LSOptionCell *)cell).bookingAmountValue.hidden = true;;
        }
        
    }
    
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_tag == 2 && [[_extraExtraDatas objectAtIndex:indexPath.row] intValue] == 0) {
        return;
    }
    if(_delegate){
        if(_tag){
            [_delegate getTagFirst:_tag];
        }
        //傳出去給外面用
        [_delegate dismiss:self.view andTableRowNum:[NSNumber numberWithInt:indexPath.row]andDataName: [_datas objectAtIndex:indexPath.row]];
    }else{
        [_delegate getTagFirst:-1];
        [self dismiss:self.view andTableRowNum:[NSNumber numberWithInt:indexPath.row]andDataName: [_datas objectAtIndex:indexPath.row]];
    }//end if else
}

-(void)getSizeAfterTableViewShow{
}
- (IBAction)clickListOutside:(UIButton *)sender {
    //按下在list外面的動作
    if(_delegate){
        [_delegate touchOutSide:self.view];
    }
}

-(void)getTagFirst:(NSInteger)tag{
}

-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)areaName{
}
-(void)touchOutSide:(UIView *)view{
}

-(void)setDatas:(NSArray *)datas{
    _datas=datas;
}

-(void)setExtraDatas:(NSArray *)extraDatas{
    _extraDatas=extraDatas;
}
-(void)setExtraExtraDatas:(NSArray *)extraExtraDatas{
    _extraExtraDatas = extraExtraDatas;
}

-(void)setDelegate:(id<LStableCellSelectDelegate>)delegate{
    _delegate=delegate;
}

-(void)setlabel_title:(NSString *)title{
    self.label_title.text=title;
}

-(void)setTag:(NSInteger)tag{
    _tag=tag;
}
- (IBAction)backClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(backClick:)]) {
        [_delegate backClick:_tag];
    }
}

@end
