//
//  ProgressViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/26.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProgressDelegate <NSObject>

-(void)timeout;

@end

@interface ProgressViewController : UIViewController{
    id<ProgressDelegate> _delegate;
}
-(void)setDelegate:(id<ProgressDelegate>)delegate;
@end
