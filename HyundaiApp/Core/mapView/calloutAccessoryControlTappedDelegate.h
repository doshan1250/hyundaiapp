//
//  calloutAccessoryControlTappedDelegate.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/14.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CalloutAccessoryControlTappedDelegate <NSObject>
-(void)delegateWithMapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
@end
