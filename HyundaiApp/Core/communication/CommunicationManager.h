//
//  CommunicationManager.h
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CommunicationContext.h"
#import "ASIHTTPRequest.h"


@interface CommunicationManager : NSObject <ASIHTTPRequestDelegate>
{
    NSMutableDictionary *pCtxQueue;
}

@property (nonatomic, strong) NSMutableDictionary *pCtxQueue;

- (ASIHTTPRequest *)doRequest:(CommunicationContext *)pCtx;
- (ASIHTTPRequest *)doASIHTTPRequest:(CommunicationContext *)pCtx;

@end

