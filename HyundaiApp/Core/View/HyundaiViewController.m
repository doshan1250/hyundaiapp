//
//  HyundaiViewController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "HyundaiViewController.h"

#import "ParkingPlaceController.h"
#import "GasStationController.h"
#import "CarInfoController.h"
#import "MaintenanceController.h"
#import "SOPController.h"
#import "ContactController.h"
#import "ServiceController.h"
#import "FansController.h"
#import "CardController.h"
#import "NewsListController.h"
#import "BuyCarController.h"
#import "QuarterlyViewController.h"
#import "VideoViewController.h"

@interface HyundaiViewController() <SideBarDelegate>
{
    SideBarDropDownViewController *mSideBarDropDownViewController;
}

@end

@implementation HyundaiViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        _titleView.font = [UIFont boldSystemFontOfSize:20.0f];
        _titleView.textColor = [UIColor whiteColor];
        _titleView.textAlignment = NSTextAlignmentCenter;
        _titleView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (BOOL)preferStatusBarHidden
{
    return YES;
}

-(UIView *)titleViewSet{
     return _titleView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.titleView =[self titleViewSet];
//    self.navigationItem.titleView = _titleView;
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
    
    UIBarButtonItem *navSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    navSpace.width = NAV_SPACING;
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setFrame:CGRectMake(0, 0, 44, 44)];
    [btn1 setImage:[UIImage imageNamed:@"nav_back2.png"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(clickNavHome:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = @[navSpace, [[UIBarButtonItem alloc] initWithCustomView:btn1]];
    
    NavButton *btn2 = [NavButton buttonWithType:UIButtonTypeCustom];
    [btn2 setFrame:CGRectMake(0, 0, 44, 44)];
    [btn2 setImage:[UIImage imageNamed:@"nav_list2.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(clickNavList:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:btn2], navSpace];
}

- (IBAction)clickNavHome:(id)sender
{
    [self clickedHomeEvent];
    [self.navigationController popViewControllerAnimated:YES];
    
  
}

//在一些按下home按鈕時需要release畫面的動作
-(BOOL)clickedHomeEvent{
    return NO;
}

- (IBAction)clickNavList:(id)sender{
    [self switchSideBar];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)switchSideBar{
   
    
    if(!mSideBarDropDownViewController){
        mSideBarDropDownViewController=[[SideBarDropDownViewController alloc]initWithNibName:@"SideBarDropDownViewController" bundle:nil];
        [mSideBarDropDownViewController setDelegate:self];
        UIView *s_view=mSideBarDropDownViewController.view;
//        view.tag=1;
        [currentApp.window addSubview: s_view];
        [mSideBarDropDownViewController animationWithView];
    }else{
        UIView *s_view=mSideBarDropDownViewController.view;
//        view.tag=0;
        [s_view removeFromSuperview];
        mSideBarDropDownViewController=nil;
    }
}

-(void)pageToPageAcross:(NSInteger)tag
{
    UIViewController *vc = nil;
    tag = tag + 1;
    
    switch (tag)
    {
        case 1: // 最新消息
        {
            if([self isMemberOfClass:[NewsListController class]])
                return;
            
            NewsListController *newList = [[NewsListController alloc] initWithNibName:@"NewsListController" bundle:nil];
            [newList setContent:currentApp.newsArray];
            [newList reloadData];
            
            vc = newList;
            
            newList = nil;
            break;
        }
            
        case 2: // 粉絲團 (New)
        {
            if([self isMemberOfClass:[FansController class]])
                return;
            
            vc = [[FansController alloc] initWithNibName:@"FansController" bundle:nil andLink:@"https://www.facebook.com/hyundai.tw" andtitle:@"粉絲團"];
            break;
        }
            
        case 3: // 影音專區 (New)
        {
            if([self isMemberOfClass:[VideoViewController class]])
                return;
            
            vc = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
            break;
        }
            
        case 4: // 車主季刊 (New)
        {
            if([self isMemberOfClass:[QuarterlyViewController class]])
                return;
            
            vc = [[QuarterlyViewController alloc] initWithNibName:@"QuarterlyViewController" bundle:nil];
            break;
        }
            
        case 5: // 聯絡營業員
        {
            if([self isMemberOfClass:[ContactController class]])
                return;
            
            vc = [[ContactController alloc] initWithNibName:@"ContactController" bundle:nil];
            break;
        }
            
        case 6: // 維修紀錄
        {
            if([self isMemberOfClass:[MaintenanceController class]])
                return;
            
            vc = [[MaintenanceController alloc] initWithNibName:@"MaintenanceController" bundle:nil];
            break;
        }
            
        case 7: // 服務據點
        {
            if([self isMemberOfClass:[ServiceController class]])
                return;
            
            vc = [[ServiceController alloc] initWithNibName:@"ServiceController" bundle:nil];
            break;
        }
            
        case 8: // 道路救援
        {
            if([self isMemberOfClass:[SOPController class]])
                return;
            
            vc = [[SOPController alloc] initWithNibName:@"SOPController" bundle:nil];
            break;
        }
            
        case 9: // 新車資訊
        {
            if([self isMemberOfClass:[CarInfoController class]])
                return;
            
            vc = [[CarInfoController alloc] initWithNibName:@"CarInfoController" bundle:nil];
            break;
        }
            
        case 10: // 購車優惠
        {
            if([self isMemberOfClass:[BuyCarController class]])
                return;
            
            vc = [[BuyCarController alloc] initWithNibName:@"BuyCarController" bundle:nil];
            break;
        }
            
        case 11: // 加油站
        {
            if([self isMemberOfClass:[GasStationController class]])
                return;
            
            vc = [[GasStationController alloc] initWithNibName:@"GasStationController" bundle:nil];
            break;
        }
            
        case 12: // 停車場
        {
            if([self isMemberOfClass:[ParkingPlaceController class]])
                return;
            
            vc = [[ParkingPlaceController alloc] initWithNibName:@"ParkingPlaceController" bundle:nil];
            break;
        }
            
        default:
            break;
    }
    
    [currentApp.nav popToRootViewControllerAnimated:NO];
    
    if (vc)
    {
        [currentApp.nav pushViewController:vc animated:NO];
        vc = nil;
    }

}//end pageToPageAcross

-(void)exitWithAnimation:(NSInteger)tag{
    
    [self switchSideBar];
    
    [self pageToPageAcross:tag];
}//end exitWithAnimation

-(void)dismiss{
    [self switchSideBar];
}

-(BOOL)loadWithRequest:(NSString *)target{
    NSRange check = [target rangeOfString:@"?"];
    
    if( check.location != NSNotFound )
    {//如果有抓到ＧＥＴ參數
        NSArray *urlStirngs=[target componentsSeparatedByString:@"?"];
        NSString *urlGetString=[urlStirngs objectAtIndex:1] ;
        urlGetString=[urlGetString stringByReplacingOccurrencesOfString:@"&" withString:@"="];
        NSArray *urlGetStrings = [urlGetString componentsSeparatedByString:@"="];
        //獲取event的參數
        bool breakValue=NO;
        int index=[urlGetStrings indexOfObject:@"event"];
        if(index !=NSNotFound&&!(index>=([urlGetStrings count]-1))){
            int eventID= [[urlGetStrings objectAtIndex:(index+1)] intValue];
            switch (eventID) {
                case 0:
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    break;
                case 1:
                    [self pageToPageAcross:1];
                    break;
                case 2:
                    [self pageToPageAcross:0];
                    break;
                case 3:
                    [self pageToPageAcross:2];
                    break;
                case 4:
                    [self pageToPageAcross:3];
                    break;
                case 5:
                    [self pageToPageAcross:4];
                    break;
                case 6:
                    [self pageToPageAcross:5];
                    break;
                case 7:
                    [self pageToPageAcross:6];
                    break;
                case 8:
                    [self pageToPageAcross:7];
                    break;
                case 9:
                    [self pageToPageAcross:8];
                    break;
                default:
                    breakValue=YES;
                    break;
            }//end switch
            
        }else{
            breakValue=YES;
        }//end if
        
        urlGetStrings=nil;
        urlStirngs=nil;
        return breakValue;
    }
    return YES;
}


@end
