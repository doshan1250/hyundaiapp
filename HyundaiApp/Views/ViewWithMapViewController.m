//
//  ViewWithMapViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/17.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ViewWithMapViewController.h"

@interface ViewWithMapViewController (){
    NSNumber  *_mode;
    CGRect _frame;
    CGRect _textFrame;
    CGRect _navButtonFrame;
    CLLocationCoordinate2D _origin;
    CLLocationCoordinate2D _dest;
    
}
@property (strong, nonatomic) IBOutlet UIButton *nav_map_btn;
@property (strong, nonatomic) IBOutlet UIImageView *icon_type;
@property (strong, nonatomic) IBOutlet UILabel *label_title;
@property (strong, nonatomic) IBOutlet UILabel *label_dec1;
@property (strong, nonatomic) IBOutlet UITextView *label_dec2;
@property (strong, nonatomic) IBOutlet UIView *view_parking;
@property (strong, nonatomic) IBOutlet UIView *view_gasAndParing;
@property (strong, nonatomic) IBOutlet UIView *view_fixSize;
@property (strong, nonatomic) IBOutlet UILabel *label_dis;
@property (strong, nonatomic) IBOutlet UILabel *label_km_text;
@end

@implementation ViewWithMapViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setMode:(int)mode{
    //設定是停車場還是加油站的layout
    if(!_mode){
        _mode=[[NSNumber alloc]initWithInt:mode];
        switch (mode) {
            case 0://park
                _textFrame=_label_dec1.frame;
                //如果是停車場，就把文字長度拉到最大
                [_label_dec1 setFrame:CGRectMake(_textFrame.origin.x, _textFrame.origin.y, _label_km_text.frame.origin.x+_label_km_text.frame.size.width-_textFrame.origin.x, _label_dec1.frame.size.height)];
                _label_km_text.hidden=YES;
                _label_dis.hidden=YES;
                break;
            case 1:
                _view_fixSize.frame=CGRectMake(_view_fixSize.frame.origin.x, _view_fixSize.frame.origin.y+_view_parking.frame.size.height, _view_fixSize.frame.size.width, _view_gasAndParing.frame.size.height);
                _view_parking.hidden=YES;
                self.nav_map_btn.frame=CGRectMake(self.nav_map_btn.frame.origin.x, self.nav_map_btn.frame.origin.y+_view_parking.frame.size.height, self.nav_map_btn.frame.size.width, self.nav_map_btn.frame.size.height) ;
                break;
            default:
                break;
        }
        _frame=self.view_gasAndParing.frame;//init
        _textFrame=_label_dec1.frame;//init
        _navButtonFrame=self.nav_map_btn.frame;//init
    }
    
}

-(void)setTitleForParking:(NSString *)title andAlladdress:(NSString *)alladdress andSummary:(NSString *)summary andTel:(NSString *)tel andIcon:(NSString *)imageNamed{
    
    [self setMode:0];
    
    [_icon_type setImage:[UIImage imageNamed:imageNamed]];
    _label_title.text=title;
     [self setAddress:[NSString stringWithFormat:@"%@\n%@", alladdress,summary]];
    if(!tel||[tel isEqualToString:@""]){
    }else{
        _tel=tel;
    }
}

-(void)setAddress:(NSString *)alladdress{
    self.view_gasAndParing.frame=_frame;//reset
    _label_dec1.frame =_textFrame;//reset
    self.nav_map_btn.frame=_navButtonFrame;//reset
    CGSize titleSize = [alladdress sizeWithFont:_label_dec1.font constrainedToSize:CGSizeMake(_label_dec1.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
       CGFloat dH=titleSize.height-_label_dec1.frame.size.height;
    if(dH>0){
        int lines=titleSize.height/_label_dec1.frame.size.height;
        [_label_dec1 setNumberOfLines:lines+4];
        if([_mode intValue]==0){//park
            CGRect frame=_label_dec1.frame;
            CGFloat dH2=dH+frame.size.height;
            [_label_dec1 setFrame:CGRectMake(frame.origin.x, frame.origin.y+5, frame.size.width, frame.size.height*2+dH)];
            CGRect frame2=self.view_gasAndParing.frame;
            [self.view_gasAndParing setFrame:CGRectMake(frame2.origin.x, frame2.origin.y-dH2, frame2.size.width, frame2.size.height+dH2)];
        }else{
            CGRect frame=_label_dec1.frame;
            [_label_dec1 setFrame:CGRectMake(frame.origin.x, frame.origin.y+5, frame.size.width, frame.size.height*2+dH)];
            CGRect frame2=self.view_gasAndParing.frame;
            [self.view_gasAndParing setFrame:CGRectMake(frame2.origin.x, frame2.origin.y-dH, frame2.size.width, frame2.size.height+dH)];
        }
    }
    
    [_label_dis setFrame:CGRectMake(_label_dis.frame.origin.x, self.view_gasAndParing.frame.size.height/2-(_label_dis.frame.size.height/2), _label_dis.frame.size.width,_label_dis.frame.size.height )];
    [_label_km_text setFrame:CGRectMake(_label_km_text.frame.origin.x, self.view_gasAndParing.frame.size.height/2-(_label_km_text.frame.size.height/2), _label_km_text.frame.size.width, _label_km_text.frame.size.height)];
    
    CGRect frame_nav_map=self.nav_map_btn.frame;
    //導航按鈕
    [self.nav_map_btn setFrame:CGRectMake(frame_nav_map.origin.x, frame_nav_map.origin.y+(self.view_gasAndParing.frame.origin.y-_frame.origin.y), frame_nav_map.size.width, frame_nav_map.size.height)];
    
    _label_dec1.text=alladdress;
}

-(void)setTitleForGas:(NSString *)title andAlladdress:(NSString *)alladdress andIcon:(NSString *)imageNamed andDis:(NSString *)dis{
    [self setMode:1];
    if(!dis||[dis isEqualToString:@""]){
        dis=@"0";
    }
    _label_dis.text=dis;
    _label_title.text=title;
    [self setAddress:alladdress];
    [_icon_type setImage:[UIImage imageNamed:imageNamed]];
}

- (IBAction)dismiss:(UIButton *)sender {
    NSLog(@"close");
    [self.view removeFromSuperview];
}


#pragma mark - 導航相關
-(void)enableNavButton:(BOOL)enable{
    [self.nav_map_btn setEnabled:enable];
}

-(void)setOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest{
    _origin=origin;
    _dest=dest;
}

- (IBAction)nav_clicked:(UIButton *)sender {
    if(self.delegate){
        [delegate navForOrigin:_origin andDestination:_dest];
    }
}

-(UIButton *)getnavMapBtn{
    return self.nav_map_btn;
}

@end
