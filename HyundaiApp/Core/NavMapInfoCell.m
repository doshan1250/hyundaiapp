//
//  NavMapInfoCell.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/20.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "NavMapInfoCell.h"

@implementation NavMapInfoCell
@synthesize lb_title;
@synthesize lb_number;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


@end
