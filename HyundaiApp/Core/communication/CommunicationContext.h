//
//  CommunicationContext.h
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "ICommunication.h"

@interface CommunicationContext : NSObject
{
    // Delegate
    __unsafe_unretained id<ICommunication> delegate;
    
    // Action Enum
    NSInteger action;
    
    // URL
    NSString *url;
    
    // HTTP Method
    NSString *method;
    
    // POST Data
    id postData;
    
    // Request Data
    NSData *retData;
    
    // User Object
    id userObj;
    
    // Sync, Async Method
    BOOL isSync;
    
    // Error Code
    NSInteger errorCode;
    
    // Error Message
    NSString *errorMessage;
}

@property (nonatomic, unsafe_unretained) id<ICommunication> delegate;
@property (nonatomic, unsafe_unretained) NSInteger action;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *method;
@property (nonatomic, strong) id postData;
@property (nonatomic, strong) NSData *retData;
@property (nonatomic, strong) id userObj;
@property (nonatomic, unsafe_unretained) BOOL isSync;
@property (nonatomic, unsafe_unretained) NSInteger errorCode;
@property (nonatomic, strong) NSString *errorMessage;

@end
