//
//  GasPriceViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/14.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "GasPriceViewController.h"

@interface GasPriceViewController ()
@property (strong, nonatomic) IBOutlet UILabel *gas92;
@property (strong, nonatomic) IBOutlet UILabel *gas95;
@property (strong, nonatomic) IBOutlet UILabel *gas98;
@property (strong, nonatomic) IBOutlet UILabel *gasSuper;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressView;

@end

@implementation GasPriceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.progressView && ![self.progressView superview]){
        [self.view addSubview:self.progressView];
    }
    [self.progressView startAnimating];
}

-(void)viewDidAppear:(BOOL)animated{
    //NSLog(@"viewDidAppear, _gas92.text: %@",_gas92.text);
   
       [super viewDidAppear:animated];
    
}

-(void)dealloc{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)stopProgress{
    if(self.progressView && [self.progressView superview]){
        [self.progressView stopAnimating];
        [self.progressView removeFromSuperview];
    }
}

-(void)setGAsPrice:(NSString *)gasPrice withIndex:(NSInteger)row{
    
    if(row>-1&&row<4){
        switch (row) {
            case 0:
                _gas92.text=gasPrice;
                break;
            case 1:
                _gas95.text=gasPrice;
                break;
            case 2:
                _gas98.text=gasPrice;
                break;
            case 3:
                _gasSuper.text=gasPrice;
                break;
            default:
                break;
        }
    }//endif
}

-(void)setGasPrice:(NSString *)gas92 andGas95:(NSString *)gas95 andGas98:(NSString *)gas98 andGasSuper:(NSString *)gasSuper{
    self.gas92.text=gas92;
    self.gas95.text=gas95;
    self.gas98.text=gas98;
    self.gasSuper.text=gasSuper;
}

@end
