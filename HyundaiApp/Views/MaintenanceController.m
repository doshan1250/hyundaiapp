//
//  MaintenanceController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "MaintenanceController.h"
#import "CollapseClick.h"
#import "LoginTools.h"
#import "SettingController.h"
#import "LoginStatus.h"
#import "ListOptionViewController.h"
#import "RecordTools.h"
#import "MainteananceRecordViewController.h"
#import "MainteananceRecordDelegate.h"

#import "ProgressViewController.h"
#import "FactoryIdParser.h"
#import "Reservation2View.h"
#import "Reservation1View.h"
#import "Reservation6View.h"
#define KEY_DATE    @"key_date"
#define KEY_F_NAME  @"key_f_name"

#define DDD 1   // loading indicator
#define DDD2  1  // time section 假資料
#define DDD3 0 //邊境測試資料

@interface MaintenanceController () <LStableCellSelectDelegate,CollapseClickDelegate,LoginStatus,UITextFieldDelegate,MainteananceRecordDelegate,ProgressDelegate>
{
    IBOutlet UIView *record;
    IBOutlet CollapseClick *myCollapseClick;
    IBOutlet UIScrollView *reservationView;
    LoginTools *mLoginTools;
    SettingController *mSettingController;
    //保養紀錄的內容
    NSArray * data;
    //確認預約的內容
    
    NSString *fidAreaIndex, *r_fid, *r_time, *r_year, *r_month, *r_day, *r_date;
    NSString *r_fidArea_name,*r_fName, *r_userName, *r_phone,*r_memo,*r_carId;
    NSString *r_time_record;
    ListOptionViewController *mListOptionViewController;
    
    //日期選擇相關
    NSInteger unitFlags;
    NSCalendar *calendar;
    NSDateComponents *comps ;
    NSDate *date;
    NSInteger select_tag;
    MainteananceRecordViewController *mMainteananceRecordViewController;
    
    ProgressViewController *mProgressViewController;
    FactoryIdParser *mFacParser;
    
    NSArray *fidArray;
    NSMutableArray *remainAmount;
    NSMutableArray *remainSection;
    
    
    BOOL bDisplayTimeSection;
    BOOL b1;
}

@property (strong, nonatomic) IBOutlet UIView *view_buttons;

@property (strong, nonatomic) IBOutlet UIButton *bt1;
@property (strong, nonatomic) IBOutlet UIButton *bt2;
@property (strong, nonatomic) IBOutlet UIView *contentVeiw;
@property (strong, nonatomic) IBOutlet UITextField *tf_carID;
@property (strong, nonatomic) IBOutlet UITextField *tf_name;
@property (strong, nonatomic) IBOutlet UITextField *tf_phone;
@property (strong, nonatomic) IBOutlet UITextField *tf_memo;
@property (strong, nonatomic) IBOutlet UIButton *bt_chooseFidArea;

@property (strong, nonatomic) IBOutlet UIButton *bt_chooseFid;
@property (strong, nonatomic) IBOutlet UIButton *bt_choose_time;
@property (strong, nonatomic) IBOutlet UIButton *bt_reservationRecord;
@property (strong, nonatomic) IBOutlet UIButton *bt_reservationSubmit;
@property (strong, nonatomic) IBOutlet UIButton *bt_chooseDate;
@property (strong, nonatomic) IBOutlet UIButton *bt_next_time;

@end

@implementation MaintenanceController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        b1 = false;
        bDisplayTimeSection = false;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"保養記錄";
    
    mLoginTools=[LoginTools new];
    
    unitFlags = NSYearCalendarUnit |
    NSMonthCalendarUnit |
    NSDayCalendarUnit |
    NSHourCalendarUnit |
    NSMinuteCalendarUnit |
    NSSecondCalendarUnit;
    
    calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    comps = [[NSDateComponents alloc] init] ;
    [self.tf_phone setDelegate:self];
    [self.tf_name setDelegate:self];
    
    [self.tf_memo setDelegate:self];
    // [self.tf_carID setDelegate:self];
    [self.contentVeiw setBackgroundColor:HYUNDAI_BG_COLOR];
    [record setBackgroundColor:HYUNDAI_BG_COLOR];
    [self setStatus:1];//0=線上預約, 1= 保養紀錄
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady:"];
    CGRect fullScreenBounds = [[UIScreen mainScreen] bounds];
    NSLog(@"%f",fullScreenBounds.size.height);
    if(fullScreenBounds.size.height<500){
        [reservationView setContentSize:CGSizeMake(self.view.frame.size.width, self.view_buttons.frame.size.height+self.view_buttons.frame.origin.y)];
    }
    mFacParser=[FactoryIdParser new];
    //●○
    
    [self.maintenanceButton setImage:[UIImage imageNamed:@"可預約件數按鈕2"] forState:UIControlStateSelected];
    
    [self.routineCheck setImage:[UIImage imageNamed:@"可預約件數按鈕2"] forState:UIControlStateSelected];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];

}
- (void) dismissKeyboard{
    [self.view endEditing:true];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([mLoginTools shouldLogin]){
        mSettingController=[[SettingController alloc] initWithNibName:@"SettingController" bundle:nil];
        mSettingController.delegate=self;
        [mSettingController setMode:NO];
        [self.view addSubview:mSettingController.view];
    }else{
        [self.tf_name setText: [mLoginTools.getUserData objectForKey:@"OwnerName"]];
        [self.tf_carID setText:[mLoginTools getCarId]];
        [self dataInit];
    }
}

- (void)dealloc
{
    if(mProgressViewController){
        if([mProgressViewController.view superview]){
            [mProgressViewController.view removeFromSuperview];
        }
        mProgressViewController=nil;
    }
}

-(BOOL)clickedHomeEvent{
    [currentApp.dataMgr delObserverData:self];
    return YES;
}


- (IBAction)fidClicked:(UIButton *)sender {
    [self pushOptions:fidAreaIndex];
}

- (IBAction)fidAreaClicked:(UIButton *)sender {
    [self pushOptions:nil];
}


//將fids送出
-(void)pushOptions:(NSString *)index{
    
    NSMutableArray *mNSMutableArray=[NSMutableArray new];
    
    if(!index){//獲取地區列表
        NSArray *arr=[mFacParser getFacAreaNames];
        [self options:6 orDatas:[[NSArray alloc]initWithArray: arr] orExtra:nil];
        return;
    }else{//獲取服務廠列表
        //獲取本機使用者的carType
        NSString *carType=([[[mLoginTools getUserData]objectForKey:@"ModelSaleMgtIdn"] isEqualToString:@"乘"])?@"1":@"2";
        NSDictionary *mNSDictionary;
        NSMutableArray *extraAddressArray=[NSMutableArray new];
        NSArray *oldFidArray= [currentApp.fids objectForKey:fidAreaIndex];
        NSMutableArray *newFidArray=[NSMutableArray new];
        if(oldFidArray){
            for(int n=0;n<oldFidArray.count;n++){
                mNSDictionary= [oldFidArray objectAtIndex:n];
                
                NSString *carTypeByCar=[NSString stringWithFormat:@"%@",[mNSDictionary objectForKey:@"cartype"]];
                NSLog(@"獲取服務場列表,%@, 自己的:%@" ,carTypeByCar,carType);
                
                if([carType isEqualToString: carTypeByCar]){
                    [mNSMutableArray addObject:[mNSDictionary objectForKey:@"name"] ];
                    [extraAddressArray addObject:[mNSDictionary objectForKey:@"address"] ];
                    [newFidArray addObject:[mNSDictionary copy]];
                }
            }
        }
        fidArray=newFidArray;
        
        [self options:1 orDatas:[[NSArray alloc]initWithArray: mNSMutableArray] orExtra:[[NSArray alloc]initWithArray: extraAddressArray]];
    }
}

- (IBAction)dateClicked:(UIButton *)sender {
    [self options:3 orDatas:nil  orExtra:nil];
}

- (IBAction)timeClicked:(UIButton *)sender {
    [self options:2 orDatas:nil  orExtra:nil];
}

- (IBAction)recordGet:(UIButton *)sender {
    
    mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
    [mProgressViewController setDelegate:self];
    mProgressViewController.view.frame=currentApp.window.frame;
    [currentApp.window addSubview:mProgressViewController.view];
    [currentApp.dataMgr fireRequest:Request_bookingInfo postData:[NSMutableDictionary dictionaryWithObjectsAndKeys:[[mLoginTools getUserData] objectForKey:@"cEngNo"],@"CengId", nil]];
}

#pragma mark - MainteananceRecordDelegate 預約記錄離開畫面
-(void)dismissView:(UIView *)view{
    [view removeFromSuperview];
}

- (IBAction)submit:(UIButton *)sender {
    
    if([self check]){
        //["Name"],["CengId"],["BDate"],["CNum"],["FCode"],["Tel"],["Notes”]
        //依照指定的格式送出日期，這裡是背景操作
        NSString *dateString=[NSString stringWithFormat:@"%@/%@/%@ %@",r_year,r_month,r_day,r_time];
        r_userName=_tf_name.text;
        r_phone=_tf_phone.text;
        r_memo=_tf_memo.text;
        NSString * cEngNo = [[mLoginTools getUserData] objectForKey:@"cEngNo"];
        r_carId=[mLoginTools getCarId];
        
        NSString *km = self.currentKM.text;
        bool b = self.maintenanceButton.selected;
        
#if DDD
        //debug
        mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
        [mProgressViewController setDelegate:self];
        mProgressViewController.view.frame=currentApp.window.frame;
        [currentApp.window addSubview:mProgressViewController.view];
#endif
        
#if DDD3
        [currentApp.dataMgr
         fireRequest:Request_BookingCarService
         postData:
         [NSMutableDictionary dictionaryWithObjectsAndKeys:
          r_userName,@"Name",
          cEngNo,@"CengId",
          dateString,@"BDate",
          r_carId,@"CNum",
          r_fid,@"FCode",
          r_phone,@"Tel",
          r_memo,@"Notes",
          km,@"mileage",
          (b == true ? @"F":@"M"),@"BookingType", nil]];
#else
        [currentApp.dataMgr
         fireRequest:Request_BookingCarService
         postData:
         [NSMutableDictionary dictionaryWithObjectsAndKeys:
          r_userName,@"Name",
          cEngNo,@"CengId",
          dateString,@"BDate",
          r_carId,@"CNum",
          @"CG100",@"FCode",
          r_phone,@"Tel",
          @"邊境測試",@"Notes",
          km,@"mileage",
          (b == true ? @"F":@"M"),@"BookingType", nil]];
#endif
        
//       [currentApp.dataMgr fireRequest:Request_Reservation postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: r_userName,@"Name",[[mLoginTools getUserData] objectForKey:@"cEngNo"],@"CengId",dateString,@"BDate",r_carId,@"CNum",r_fid,@"FCode",r_phone,@"Tel",r_memo,@"Notes", nil]];
    }//end if
}

-(BOOL)check{
    NSString *error;
    NSString *mString;
    mString=self.tf_carID.text;
    if((!mString)||[mString isEqualToString:@""]){
        error=@"請輸入車牌號碼";
    }else{
        mString=self.tf_name.text;
        if((!mString)||[mString isEqualToString:@""]){
            error=@"請輸入您的名字";
        }else{
            mString=self.tf_phone.text;
            if((!mString)||[mString isEqualToString:@""]){
                error=@"請輸入您的聯絡電話";
            }else{
                if((!r_day)||[r_day isEqualToString:@""]||[r_month isEqualToString:@""]||[r_year isEqualToString:@""]){
                    error=@"請選擇日期";
                }else{
                    if((!r_fid)||[r_fid isEqualToString:@""]){
                        error=@"請選擇服務廠";
                    }else{
                        if((!r_time)||[r_time isEqualToString:@""]){
                            error=@"請選擇時段";
                        }//endif
                    }//end if else
                }//end if else
            }//end if else
        }//end if else
    }//end if else
    if((error)&&![error isEqualToString:@""]){
        [[[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil] show];
        return false;
    }
    return true;
}





#pragma mark - 依照index產生列表選擇
-(void)options:(int)index orDatas:(NSArray *)optionDatas orExtra:(NSArray *)extraArray{
    [self.tf_carID resignFirstResponder];
    [self.tf_memo resignFirstResponder];
    [self.tf_name resignFirstResponder];
    [self.tf_phone resignFirstResponder];
    
    mListOptionViewController.backButton.hidden = true;
    NSArray *datas;
    mListOptionViewController=[[ListOptionViewController alloc]initWithNibName:@"ListOptionViewController" bundle:nil];
    NSString *title;
    if(index==6){
        datas=optionDatas;
        [mListOptionViewController setTag:6];
        title=self.bt_chooseFidArea.titleLabel.text;
    }
    else if(index==1){//服務廠選擇
        datas=optionDatas;
        [mListOptionViewController setTag:1];
        title=self.bt_chooseFid.titleLabel.text;
        [mListOptionViewController setExtraDatas:extraArray];
        
    }else if(index==3){//日期選擇 這裡先選擇年
        date= [NSDate dateWithTimeIntervalSinceNow: +(24 * 60 * 60)];
        NSDate *endDate = [date dateByAddingTimeInterval:24*60*60*90];
        
        comps = [calendar components:unitFlags fromDate:date];
        NSDateComponents *endComps = [calendar components:unitFlags fromDate:endDate];
        long year=[comps year];
        long endYear=[endComps year];
        
        NSString *year1, *year2;
        year1=[NSString stringWithFormat:@"%ld",year];
        year2 = [NSString stringWithFormat:@"%ld",endYear];
        if (year+1 == endYear) {
            datas=[[NSArray alloc]initWithObjects:year1,year2, nil];
            
        }else{
            datas=[[NSArray alloc]initWithObjects:year1, nil];
        }
        
        [mListOptionViewController setTag:3];
        title=self.bt_chooseDate.titleLabel.text;
        
    }else if(index==2){//時段選擇
        
#if DDD2
        if (r_day == nil || r_year == nil || r_month == nil) {
            return;
        }
#endif
        
#if DDD2
        if (remainAmount == nil) {
            
            if (r_fid && r_date) {
                
#if DDD
                //debug
                mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
                [mProgressViewController setDelegate:self];
                mProgressViewController.view.frame=currentApp.window.frame;
                [currentApp.window addSubview:mProgressViewController.view];
#endif
                [currentApp.dataMgr fireRequest:Request_GetBookingBalanceInfo postData:[NSMutableDictionary dictionaryWithDictionary:@{@"cSfIdn":r_fid,@"dBookingDate":r_date}]];
                bDisplayTimeSection = true;
            }
            
            return;
        }
#endif
        
        datas=[[NSArray alloc]initWithObjects:@"08:30",@"09:00",@"10:00",@"11:00",@"12:00",@"13:00",@"14:00",@"15:00",@"16:00",@"17:00", nil];
//        datas = remainSection;
        
#if DDD2
        NSMutableArray * arr = [NSMutableArray new];
        for (int i = 0 ; i < datas.count ; i ++) {
            if (([remainSection indexOfObject:datas[i]] != NSNotFound)) {
                [arr addObject:remainAmount[[remainSection indexOfObject:datas[i]]]];
            }
            else{
                [arr addObject:@"0"];
            }
        }
#else
        NSMutableArray * arr = [NSMutableArray new];
        for (int i = 0 ; i < datas.count ; i ++) {
            [arr addObject:[[NSDecimalNumber alloc] initWithInt:i]];
        }
        
#endif
        
        NSMutableArray * merged = [NSMutableArray new];
        for (int i = 0; i < datas.count; i++) {
            NSString *merge = @"";
            if (i == datas.count-1) {
                merge = [NSString stringWithFormat:@"%@-%@",datas[i],@"18:00"];
            }
            else{
                merge = [NSString stringWithFormat:@"%@-%@",datas[i],datas[i+1]];
            }
            [merged addObject:merge];
        }
        datas = merged;
        [mListOptionViewController setTag:2];
        title=self.bt_choose_time.titleLabel.text;
        [mListOptionViewController setExtraExtraDatas:arr];
    }
    
    [mListOptionViewController setDatas:datas];
    [mListOptionViewController setDelegate:self];
    mListOptionViewController.view.frame=currentApp.self.window.frame;
    [currentApp.self.window addSubview:mListOptionViewController.view];
    switch (index) {
        case 1:
            [mListOptionViewController setlabel_title:@"選擇服務廠"];
            break;
        case 2:
            [mListOptionViewController setlabel_title:@"選擇時段"];
            break;
        case 3:
            [mListOptionViewController setlabel_title:@"選擇年份"];
            break;
        default:
            break;
    }
}

#pragma mark - LStableCellSelectDelegate

-(void)getTagFirst:(NSInteger)tag{
    select_tag=tag;
//    [currentApp.dataMgr fireRequest:Request_GetBookingBalanceInfo postData:[NSMutableDictionary dictionaryWithDictionary:@{@"cSfIdn":r_fid,@"dBookingDate":r_date}]];
}

//選項結果
-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)dataName{
    mListOptionViewController.backButton.hidden = true;
    if(select_tag==6){//服務場地區
        [view removeFromSuperview];
        mListOptionViewController=nil;
        fidAreaIndex=dataName;
        r_fidArea_name=dataName;
        [self.bt_chooseFidArea setTitle:dataName forState:UIControlStateNormal];
        [self.bt_chooseFidArea setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        r_fid=nil;
        r_fName=nil;
        //這裡和UI重複了
        [self.bt_chooseFid setTitle:@"服務廠代碼" forState:UIControlStateNormal];
        [self.bt_chooseFid setTitleColor:[UIColor colorWithWhite:0.36 alpha:1] forState:UIControlStateNormal];
        
    }else if(select_tag==1){//服務廠
        [view removeFromSuperview];
        mListOptionViewController=nil;
        r_fid=[[fidArray objectAtIndex:[row intValue]] objectForKey:@"servicecode"];
        r_fName=dataName;
        [self.bt_chooseFid setTitle:dataName forState:UIControlStateNormal];
        [self.bt_chooseFid setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if (r_fid && r_date) {
            [currentApp.dataMgr fireRequest:Request_GetBookingBalanceInfo postData:[NSMutableDictionary dictionaryWithDictionary:@{@"cSfIdn":r_fid,@"dBookingDate":r_date}]];
        }
    }else if(select_tag==2){//時段
        [view removeFromSuperview];
        mListOptionViewController=nil;
        r_time=dataName;
        int time_check=[row intValue];
        if(time_check<7){
            r_time_record=@"上午時段";
        }else if(time_check<11){
            r_time_record=@"中午時段";
        }else{
            r_time_record=@"下午時段";
        }
        [self.bt_choose_time setTitle:dataName forState:UIControlStateNormal];
        [self.bt_choose_time setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }else if(select_tag==3){//年份
        mListOptionViewController.backButton.hidden = false;
        r_year=dataName;
        long month = [comps month];
        if([r_year intValue]!=[comps year]){
            month=1;
        }
        
        NSMutableArray *months=[NSMutableArray new];
        short endMonth = [comps month] + 3;
        while(month<endMonth+1){//從該月到12月
            [months addObject:[NSString stringWithFormat:@"%ld",month]];
            month=month+1;
        }
        [mListOptionViewController setTag:4];
        [mListOptionViewController setDatas:[[NSArray alloc]initWithArray:months]];
        [mListOptionViewController reloadData];
        [mListOptionViewController setlabel_title:@"選擇月份"];
    }else if(select_tag==4){//顯示day
        mListOptionViewController.backButton.hidden = false;
        r_month=dataName;
        short endMonth = [comps month] + 3;

        NSMutableArray *days=[NSMutableArray new];
        if([r_year intValue]==[comps year]&&[r_month intValue]==[comps month]){
            //如果選擇當年當月
            NSCalendar *calendarmonth = [NSCalendar currentCalendar];
            NSRange range = [calendarmonth rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
            NSUInteger numberOfDaysInMonth = range.length;//得到当前月的天数
            for(int n=[comps day]+1;n<=numberOfDaysInMonth;n++){
                [days addObject:[NSString stringWithFormat:@"%d",n]];
            }
        }else if ([r_year intValue]==[comps year]&&[r_month intValue]==endMonth){
            // 第三個月
            for(int n=1;n<=[comps day];n++){
                [days addObject:[NSString stringWithFormat:@"%d",n]];
            }
        }
        else{
            //如果是未來
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            [dateComponents setYear:[r_year intValue]-[comps year]];
            [dateComponents setMonth:[r_month intValue]-[comps month]];
            NSDate *newDate=[gregorian dateByAddingComponents:dateComponents toDate:date  options:0];
            NSRange range = [gregorian rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:newDate];
            NSUInteger numberOfDaysInMonth = range.length;//得到当前月的天数
            for(int n=1;n<=numberOfDaysInMonth;n++){
                [days addObject:[NSString stringWithFormat:@"%d",n]];
            }
        }
        
        [mListOptionViewController setTag:5];
        
        [mListOptionViewController setDatas:[[NSArray alloc]initWithArray:days]];
        [mListOptionViewController reloadData];
        [mListOptionViewController setlabel_title:@"選擇日期"];
        
    }else if(select_tag==5){
        r_day=dataName;
        NSString *mString=[NSString stringWithFormat:@"%@-%@-%@",r_year,r_month,r_day];
        NSLog(@"day: %@",r_day);
        r_date = mString;
        [self.bt_chooseDate setTitle:mString forState:UIControlStateNormal];
        [self.bt_chooseDate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [view removeFromSuperview];
        mListOptionViewController=nil;
        
        if (r_fid && r_date) {
            [currentApp.dataMgr fireRequest:Request_GetBookingBalanceInfo postData:[NSMutableDictionary dictionaryWithDictionary:@{@"cSfIdn":r_fid,@"dBookingDate":r_date}]];
        }
        
    }//end a long ifelse
}

-(void)touchOutSide:(UIView *)view{
    [view removeFromSuperview];
    mListOptionViewController =nil;
}

#pragma mark - close keybroad
- (IBAction)closeKeyBroad:(id)sender {
    [self.tf_phone resignFirstResponder];
    [self.tf_name resignFirstResponder];
    [self.tf_memo resignFirstResponder];
}

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range  replacementString:(NSString *)string{
    
    int MAXLENGTH=0;
    if(textField==self.tf_phone){
        MAXLENGTH=10;
    }else if(textField==self.tf_name){
        MAXLENGTH=5;
    }else if(textField==self.tf_memo){
        MAXLENGTH=50;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > MAXLENGTH) ? NO : YES;
}



#pragma mark - LoginStatus
-(BOOL)success{
    [self.tf_name setText: [mLoginTools.getUserData objectForKey:@"OwnerName"]];
    [self.tf_carID setText:[mLoginTools getCarId]];
    [self dataInit];
    return NO;
}

-(BOOL)fail{
    //目前沒有動作
    return YES;
}

-(void)dataInit{
    if(![mLoginTools shouldLogin]){
#if DDD
        //debug
        mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
        [mProgressViewController setDelegate:self];
        mProgressViewController.view.frame=currentApp.window.frame;
        [currentApp.window addSubview:mProgressViewController.view];
#endif
        
        if(!currentApp.fids){
            //如果沒有值 讀取工廠
            [currentApp.dataMgr fireRequest:Request_GetFixPlace postData:nil];
        }
        //讀取保養紀錄
        [self.bt_next_time setTitle:[[mLoginTools getUserData] objectForKey:@"NextServiceDate"] forState:UIControlStateNormal];
        
        [currentApp.dataMgr fireRequest:Request_CarFix postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: [[mLoginTools getUserData] objectForKey:@"cEngNo"],@"CengId", nil]];
    }
}

#pragma mark - ProgressDelegate
-(void)timeout{
}

#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_CarFix){
        data = [notify objectForKey:API_NotifyData];
        
        NSLog(@"Request_CarFix Done: %@",data);
        //獲取到內容之後 在CollapseClickDelegate 依據data設定
        [myCollapseClick setBackgroundColor:HYUNDAI_BG_COLOR];
        myCollapseClick.CollapseClickDelegate = self;
        [myCollapseClick reloadCollapseClick];
        
        [self checkPorgressLoad];
    }else if([[notify objectForKey:API_NotifyKey] intValue] ==Request_bookingInfo){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController =nil;
        NSDictionary *datas = [notify objectForKey:API_NotifyData];
        RecordTools *mRecordTools=[RecordTools new];
        [mRecordTools setRecordOfReservation:datas];
        NSString *mString=[mRecordTools getRecordOfReservation];
        mMainteananceRecordViewController=[[MainteananceRecordViewController alloc]initWithNibName:@"MainteananceRecordViewController" bundle:nil];
        mMainteananceRecordViewController.delegate=self;
        mMainteananceRecordViewController.view.frame=currentApp.self.window.frame;
        [currentApp.self.window addSubview:mMainteananceRecordViewController.view];
        
        if(!mString||[mString isEqualToString:@""]){
            [mMainteananceRecordViewController.tv_content setText:@"沒有資料"];
            return;
        }else{
            [mMainteananceRecordViewController.tv_content setText:mString];
        }
    }
    else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_Reservation){
        NSDictionary *datas = [notify objectForKey:API_NotifyData];
        
        if([[datas objectForKey:@"status"] isEqualToString:@"OK"]){//成功的情況下
            [[[UIAlertView alloc] initWithTitle:nil message:@"預約完成!\n我們將盡快與您聯絡。" delegate:nil cancelButtonTitle:@"ＯＫ" otherButtonTitles:nil] show];
        }else{
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"預約失敗。\n%@",[datas objectForKey:@"msg"]] delegate:nil cancelButtonTitle:@"ＯＫ" otherButtonTitles:nil] show];
        }
        [self.bt_reservationSubmit setEnabled:YES];
    }else  if([[notify objectForKey:API_NotifyKey] intValue] == Request_GetFixPlace){
        
        if([mFacParser putArrayValue:[notify objectForKey:API_NotifyData]])
            currentApp.fids = [mFacParser getFactoryDic];
        [self checkPorgressLoad];
    }
    else  if([[notify objectForKey:API_NotifyKey] intValue] == Reuqest_GetNowBookingCarInfo){
        
        [mProgressViewController.view removeFromSuperview];

        NSDictionary *datas = [notify objectForKey:API_NotifyData];
        NSLog(@"%@",datas);
        
        /*
         {
         cConfirmStatus = 01;
         cSfIdn = CG100;
         dBookingDate = "2016-04-30 10:00";
         }
         */
        
//        [self handleGetNowBookingCarInfoResponse:@{@"cConfirmStatus":@"02",
//                                                   @"cSfIdn":@"CG100",
//                                                   @"dBookingDate":@"2016-04-30 10:00",}];
//
        [self handleGetNowBookingCarInfoResponse:datas];
        b1 = true;
        
    }
    else  if([[notify objectForKey:API_NotifyKey] intValue] == Request_GetBookingBalanceInfo){
        
        NSDictionary *datas = [notify objectForKey:API_NotifyData];
        NSLog(@"%@",datas);
        [mProgressViewController.view removeFromSuperview];
        
        
        if (datas) {
            [self handleGetBookingBalanceInfo:datas];
        }
        else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"查無資料" message:@"請再試一次" delegate:nil cancelButtonTitle:@"確認" otherButtonTitles:nil, nil];
//            [alert show];
        }
    }
    //
    else  if([[notify objectForKey:API_NotifyKey] intValue] == Request_BookingCarService){
        
        NSDictionary *datas = [notify objectForKey:API_NotifyData];
        NSLog(@"%@",datas);
        [mProgressViewController.view removeFromSuperview];
        
        //debug
        [self handleGetBookingCarService:datas];
//        [self handleGetBookingCarService:nil];
    }
}

-(void)checkPorgressLoad{
    if(currentApp.fids && data){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




#pragma mark - CollapseList
- (int)numberOfCellsForCollapseClick
{
    return [data count];
}

- (NSString *)titleForCollapseClickAtIndex:(int)index
{
    return @"";
}

- (UIView *)viewForCollapseClickContentViewAtIndex:(int)index
{
    UITextView *textView=[[UITextView alloc]initWithFrame:CGRectMake(0, 0, 320, 220)];
    UIView *view=[UIView new];
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView setTextColor:[UIColor whiteColor]];
    [textView setFont:[UIFont boldSystemFontOfSize:18.0]];
    [textView setEditable:NO];
    NSDictionary *dict=[data objectAtIndex:index];
    NSArray *dict_temp=[dict objectForKey:@"item"];
    
    BOOL ERROR=NO;
    if(!dict_temp){
        ERROR=YES;
    }
    NSString *mString=@"";
    if(dict_temp.count <= 0){
        ERROR=YES;
        [view setFrame: CGRectMake(0, 0, 320, 50)];
        [textView setText:@"無內容記錄"];//填入資料
    }else{
        
        NSArray *zeroItem = nil;
        NSDictionary *item = nil;
        NSDictionary *oldTitle = nil;
        NSDictionary *newTitle = nil;
        mString=[NSString stringWithFormat:@"維修廠：%@\n\n維修里程數：%@ 公里", [dict objectForKey:@"fname"], [dict objectForKey:@"mileage"]];
        
        for(int n=0;n<dict_temp.count;n++){
            
            newTitle = [[dict_temp objectAtIndex:n] objectForKey:@"labor"];
            zeroItem = [[dict_temp objectAtIndex:n] objectForKey:@"part"];
            for(int i = 0;i < zeroItem.count;i++){
                item = [zeroItem objectAtIndex:i];
                if(oldTitle == newTitle){
                    mString = [NSString stringWithFormat:@"%@\n\n  %@ x %@",mString,[item objectForKey:@"Parts"],[item objectForKey:@"Qty"]];
                }else{
                    mString = [NSString stringWithFormat:@"%@\n\n%@\n\n  %@ x %@",mString,newTitle,[item objectForKey:@"Parts"],[item objectForKey:@"Qty"]];
                    oldTitle = newTitle;
                }
            }
        }
        
        //這裡設定ＵＩ
        [textView setText:mString];//填入資料
        [view setFrame: CGRectMake(0, 0, 320, 220)];
    }
    //用UIView包起來 準備傳值進去
    [view setBackgroundColor:[UIColor blackColor]];
    [view addSubview:textView];
    return view;
}


- (void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open {
}

-(NSDictionary *)viewForCollapseClickTitleViewAtIndex:(int)index{
    NSDictionary *dict=[data objectAtIndex:index];
    return dict;
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index{
    return [UIColor whiteColor];
}


#pragma mark - 預約保養與保養紀錄tab
- (IBAction)bt1Clicked:(UIButton *)sender {
    //預約保養
    if(![mLoginTools shouldLogin]){
        
        if (b1 == false) {
#if DDD
            mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
            [mProgressViewController setDelegate:self];
            mProgressViewController.view.frame=currentApp.window.frame;
            [currentApp.window addSubview:mProgressViewController.view];
#endif
            [currentApp.dataMgr fireRequest:Reuqest_GetNowBookingCarInfo postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: [[mLoginTools getUserData] objectForKey:@"cEngNo"],@"cEngNo", nil]];
        }
        
        
        [self setStatus:0];
    }
}
- (IBAction)bt2Clicked:(UIButton *)sender {
    //保養紀錄
    if(![mLoginTools shouldLogin]){
        [self setStatus:1];
    }
}


-(void)setStatus:(int)mode{
    switch (mode) {
        case 0:
            [self.bt1 setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
            [self.bt2 setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
            [self.contentVeiw bringSubviewToFront:reservationView];
            break;
        case 1:
            [self.bt1 setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
            [self.bt2 setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
            [self.contentVeiw bringSubviewToFront:record];
            break;
        default:
            break;
    }
    
}


-(NSString *) getFieldNameWithId:(NSString *)fieldId{
    
    for (NSArray *arr in currentApp.fids.allValues) {
        for (NSDictionary *dic in arr) {
            if ([[dic objectForKey:@"servicecode"] isEqualToString:fieldId]) {
                return [dic objectForKey:@"name"];
            }
        }
    }
    
    return @"";
}

-(void) handleGetNowBookingCarInfoResponse:(NSDictionary *)dic
{
    if (dic) {
        
        //cConfirmStatus 確認狀態(01-未確認,02-已確認)
        if ([[dic objectForKey:@"cConfirmStatus"] isEqualToString:@"01"]) {
            Reservation1View *view11 = [[[NSBundle mainBundle] loadNibNamed:@"Reservation1View" owner:nil options:nil] objectAtIndex:0];
            [view11 setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.bt1.frame.origin.y)];
            [self.view addSubview:view11];
        }
        else if ([[dic objectForKey:@"cConfirmStatus"] isEqualToString:@"02"]){
            
            NSString *str = [dic objectForKey:@"dBookingDate"];
            NSArray *arr = [str componentsSeparatedByString:@" "];
            NSString *str1 = [dic objectForKey:@"cSfIdn"];
            
            Reservation2View *view11 = [[[NSBundle mainBundle] loadNibNamed:@"Reservation2View" owner:nil options:nil] objectAtIndex:0];
            view11.reservationDateLabel.text = [NSString stringWithFormat:@"%@ %@",view11.reservationDateLabel.text,arr[0]];
            NSString *name = [self getFieldNameWithId:str1];
            view11.reverationPlaceLabel.text = [NSString stringWithFormat:@"%@ %@",view11.reverationPlaceLabel.text,name];
            view11.reservationTimeLabel.text = [NSString stringWithFormat:@"%@ %@",view11.reservationTimeLabel.text,arr[1]];
            [view11 setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.bt1.frame.origin.y)];
            [self.view addSubview:view11];
        }
    }
    else{
        // 沒有預約
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *f = [NSDateFormatter new];
        [f setDateFormat:@"HH"];
        NSString *hh = [f stringFromDate:currentDate];
        if (hh.intValue >= 17) {
            UIView *view11 = [[[NSBundle mainBundle] loadNibNamed:@"reservation3View" owner:nil options:nil] objectAtIndex:0];
            [view11 setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.bt1.frame.origin.y)];
            [self.view addSubview:view11];
        }
    }
}

-(void) handleGetBookingBalanceInfo:(NSDictionary *)dic
{
    NSArray *RESULT0 = [dic objectForKey:@"RESULT0"];
    NSArray *RESULT1 = [dic objectForKey:@"RESULT1"];
    NSArray *RESULT2 = [dic objectForKey:@"RESULT2"];
    NSArray *RESULT3 = [dic objectForKey:@"RESULT3"];
    NSArray *RESULT4 = [dic objectForKey:@"RESULT4"];
    NSArray *RESULT5 = [dic objectForKey:@"RESULT5"];
    NSArray *RESULT6 = [dic objectForKey:@"RESULT6"];
    NSArray *RESULT7 = [dic objectForKey:@"RESULT7"];
    NSArray *RESULT8 = [dic objectForKey:@"RESULT8"];
    NSArray *RESULT9 = [dic objectForKey:@"RESULT9"];
    NSArray *RESULT10 = [dic objectForKey:@"RESULT10"];
    NSArray *RESULT11 = [dic objectForKey:@"RESULT11"];
    NSArray *RESULT12 = [dic objectForKey:@"RESULT12"];
    remainAmount = [NSMutableArray new];
    [remainAmount addObject:RESULT0[1]];
    [remainAmount addObject:RESULT1[1]];
    [remainAmount addObject:RESULT2[1]];
    [remainAmount addObject:RESULT3[1]];
    [remainAmount addObject:RESULT4[1]];
    [remainAmount addObject:RESULT5[1]];
    [remainAmount addObject:RESULT6[1]];
    [remainAmount addObject:RESULT7[1]];
    [remainAmount addObject:RESULT8[1]];
    [remainAmount addObject:RESULT9[1]];
    [remainAmount addObject:RESULT10[1]];
    [remainAmount addObject:RESULT11[1]];
    [remainAmount addObject:RESULT12[1]];
    
    remainSection = [NSMutableArray new];
    
    NSArray *arr0 =[RESULT0[0] componentsSeparatedByString:@" "];
    NSArray *arr1 =[RESULT1[0] componentsSeparatedByString:@" "];
    NSArray *arr2 =[RESULT2[0] componentsSeparatedByString:@" "];
    NSArray *arr3 =[RESULT3[0] componentsSeparatedByString:@" "];
    NSArray *arr4 =[RESULT4[0] componentsSeparatedByString:@" "];
    NSArray *arr5 =[RESULT5[0] componentsSeparatedByString:@" "];
    NSArray *arr6 =[RESULT6[0] componentsSeparatedByString:@" "];
    NSArray *arr7 =[RESULT7[0] componentsSeparatedByString:@" "];
    NSArray *arr8 =[RESULT8[0] componentsSeparatedByString:@" "];
    NSArray *arr9 =[RESULT9[0] componentsSeparatedByString:@" "];
    NSArray *arr10 =[RESULT10[0] componentsSeparatedByString:@" "];
    NSArray *arr11 =[RESULT11[0] componentsSeparatedByString:@" "];
    NSArray *arr12 =[RESULT12[0] componentsSeparatedByString:@" "];
    
    
    [remainSection addObject:arr0[1]];
    [remainSection addObject:arr1[1]];
    [remainSection addObject:arr2[1]];
    [remainSection addObject:arr3[1]];
    [remainSection addObject:arr4[1]];
    [remainSection addObject:arr5[1]];
    [remainSection addObject:arr6[1]];
    [remainSection addObject:arr7[1]];
    [remainSection addObject:arr8[1]];
    [remainSection addObject:arr9[1]];
    [remainSection addObject:arr10[1]];
    [remainSection addObject:arr11[1]];
    [remainSection addObject:arr12[1]];
    
    if (bDisplayTimeSection) {
        bDisplayTimeSection = false;
        // -(void)options:(int)index orDatas:(NSArray *)optionDatas orExtra:(NSArray *)extraArray{

        [self options:2 orDatas:nil orExtra:nil];
    }
    
    /*
     {
     OK = "\U8cc7\U6599\U8b80\U53d6\U5b8c\U6210";
     RESULT0 =     (
     "2017-01-01 08:30",
     8,
     0
     );
     RESULT1 =     (
     "2017-01-01 09:00",
     9,
     0
     );
     RESULT10 =     (
     "2017-01-01 18:00",
     18,
     0
     );
     RESULT11 =     (
     "2017-01-01 19:00",
     19,
     0
     );
     RESULT12 =     (
     "2017-01-01 20:00",
     20,
     0
     );
     RESULT2 =     (
     "2017-01-01 10:00",
     10,
     0
     );
     RESULT3 =     (
     "2017-01-01 11:00",
     11,
     0
     );
     RESULT4 =     (
     "2017-01-01 12:00",
     12,
     0
     );
     RESULT5 =     (
     "2017-01-01 13:00",
     13,
     0
     );
     RESULT6 =     (
     "2017-01-01 14:00",
     14,
     0
     );
     RESULT7 =     (
     "2017-01-01 15:00",
     15,
     0
     );
     RESULT8 =     (
     "2017-01-01 16:00",
     16,
     0
     );
     RESULT9 =     (
     "2017-01-01 17:00",
     17,
     0
     );
     }
     */
}

-(void) handleGetBookingCarService:(NSDictionary *)dic{
    if ([[dic objectForKey:@"status"] isEqualToString:@"ERROR"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dic objectForKey:@"msg"] message:nil delegate:nil cancelButtonTitle:@"確認" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        Reservation6View *view11 = [[[NSBundle mainBundle] loadNibNamed:@"reservation6View" owner:nil options:nil] objectAtIndex:0];
        [view11 setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.bt1.frame.origin.y)];
        [self.view addSubview:view11];
    }
}
- (IBAction)clicRoutineCheck:(id)sender {
    self.maintenanceButton.selected = false;
    self.routineCheck.selected = true;
    self.currentKM.hidden = false;
}

- (IBAction)clickMaintenance:(id)sender {
    self.maintenanceButton.selected = true;
    self.routineCheck.selected = false;
    self.currentKM.hidden = true;
}


-(void)backClick:(NSInteger)tag{
    
    if (tag == 4) {
        [mListOptionViewController.view removeFromSuperview];
        mListOptionViewController = nil;
        [self dateClicked:nil];
    }
    if (tag == 5) {
        mListOptionViewController.backButton.hidden = false;
//        r_year=dataName;
        long month = [comps month];
        if([r_year intValue]!=[comps year]){
            month=1;
        }
        
        NSMutableArray *months=[NSMutableArray new];
        short endMonth = [comps month] + 3;
        while(month<endMonth+1){//從該月到12月
            [months addObject:[NSString stringWithFormat:@"%ld",month]];
            month=month+1;
        }
        [mListOptionViewController setTag:4];
        [mListOptionViewController setDatas:[[NSArray alloc]initWithArray:months]];
        [mListOptionViewController reloadData];
        [mListOptionViewController setlabel_title:@"選擇月份"];
    }
}
@end
