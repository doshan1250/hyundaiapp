//
//  QuarterlyViewController.m
//  hyundaiapp
//
//  Created by RHZ Webber on 2015/9/25.
//  Copyright © 2015年 LinkinEDGE. All rights reserved.
//

#import "QuarterlyViewController.h"
#import "LoginTools.h"
#import "LoginStatus.h"
#import "SettingController.h"

#import "ProgressViewController.h"
#import "AsyncImageView.h"

@interface QuarterlyViewController() <LoginStatus, ProgressDelegate, UIScrollViewDelegate>
{
    LoginTools *mLoginTools;
    SettingController *mSettingController;
    
    ProgressViewController *_mProgressViewController;
    
    NSMutableArray *_data;
    
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIPageControl *_pageCtrl;
    IBOutlet AsyncImageView *_cover;
    IBOutlet AsyncImageView *_pic1;
    IBOutlet AsyncImageView *_pic2;
    IBOutlet AsyncImageView *_pic3;
    IBOutlet AsyncImageView *_pic4;
    IBOutlet UILabel *_title;
    IBOutlet UILabel *_date;
}

@end

@implementation QuarterlyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
    _titleView.text = @"車主季刊";
    
    mLoginTools = [LoginTools new];
    
//    if([mLoginTools shouldLogin])
//    {
//        mSettingController = [[SettingController alloc] initWithNibName:@"SettingController" bundle:nil];
//        mSettingController.delegate=self;
//        [mSettingController setMode:NO];
//        [self.view addSubview:mSettingController.view];
//    }
//    else
//    {
        [self initData];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LoginStatus
- (BOOL)success
{
    [self initData];
    return NO;
}

- (BOOL)fail
{
    return YES;
}


#pragma mark - Loading
- (void)showLoading
{
    _mProgressViewController = [[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
    [_mProgressViewController setDelegate:self];
    _mProgressViewController.view.frame = currentApp.window.frame;
    [currentApp.window addSubview:_mProgressViewController.view];
}

- (void)hideLoading
{
    if ([_mProgressViewController.view superview])
    {
        [_mProgressViewController.view removeFromSuperview];
        [_mProgressViewController setDelegate:nil];
        _mProgressViewController = nil;
    }
}

- (void)timeout
{
    [self hideLoading];
}

#pragma mark -
- (void)initData
{
    [self showLoading];
    
    [_scrollView setDelegate:self];
    [_scrollView setBackgroundColor:[UIColor colorHex:@"EDEDED"]];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(1520, _scrollView.frame.size.height-106)];
    
    NSString *url = [NSString stringWithFormat:API_HOST, @"getCarQuarterly.ashx?cmd=get_list&limit=1"];
    
    [currentApp.dataEngine getDataFromUrl:url params:nil isPost:NO onCompletion:^(NSString *response) {
        
        [self hideLoading];
        _data = [Utilities convertJSONStringToArray:response];
        if([_data count] > 0){
        NSDictionary *item = [_data objectAtIndex:0];
        
        [_cover loadImageFromURL:[NSURL URLWithString:[item objectForKey:@"cover"]] savePath:nil];
        
        [_title setText:[item objectForKey:@"title"]];
        
        NSArray *arrDate = [[[[item objectForKey:@"disp_date"] componentsSeparatedByString:@"T"] objectAtIndex:0] componentsSeparatedByString:@"-"];
        [_date setText:[NSString stringWithFormat:@"%@年%@月%@日", [arrDate objectAtIndex:0], [arrDate objectAtIndex:1], [arrDate objectAtIndex:2]]];
        
        [_pic1 loadImageFromURL:[NSURL URLWithString:[item objectForKey:@"image1"]] savePath:nil];
        [_pic2 loadImageFromURL:[NSURL URLWithString:[item objectForKey:@"image2"]] savePath:nil];
        [_pic3 loadImageFromURL:[NSURL URLWithString:[item objectForKey:@"image3"]] savePath:nil];
        [_pic4 loadImageFromURL:[NSURL URLWithString:[item objectForKey:@"image4"]] savePath:nil];
            
//           _cover.frame = CGRectMake(0, 0, _cover.frame.size.width, _cover.frame.size.height);
//            _cover.center = self.view.center;
//            _cover.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                          UIViewAutoresizingFlexibleLeftMargin |
//                                          UIViewAutoresizingFlexibleRightMargin);
//            _pic1.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleLeftMargin |
//                                       UIViewAutoresizingFlexibleRightMargin);
//
//            _pic2.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleLeftMargin |
//                                       UIViewAutoresizingFlexibleRightMargin);
//
//            _pic3.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleLeftMargin |
//                                       UIViewAutoresizingFlexibleRightMargin);
//            _pic4.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
//                                       UIViewAutoresizingFlexibleLeftMargin |
//                                       UIViewAutoresizingFlexibleRightMargin);

        }else{
            [Utilities showMessage:@"系統訊息" message:@"目前並無資料，請稍候再試一次！"];
        }
    } onError:^(NSError *error) {
        
        NSLog(@"ERROR: GetQuarterly");
        [self hideLoading];
        
        [Utilities showMessage:@"系統訊息" message:@"資料無法下載，請稍候再試一次！"];
        
    }];
}

- (IBAction)getQuarterly:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入收件人電子郵件信箱：" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"送出", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [alert show];
    alert = nil;
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UITextField *tf = [alertView textFieldAtIndex:0];
        
        if (![Utilities isValidEmail:tf.text])
        {
            [Utilities showMessage:@"系統提示" message:@"電子郵件信箱格式錯誤！"];
            return;
        }
        
        [self showLoading];
        
        NSDictionary *item = [_data objectAtIndex:0];
        
        NSString *srcUrl = [NSString stringWithFormat:API_HOST, @"getCarQuarterly.ashx?cmd=send_quarterly&qno=%@&email=%@&plate=%@&name=%@"];
//        NSString *url = [NSString stringWithFormat:srcUrl, [item objectForKey:@"Id"], [tf.text mk_urlEncodedString], [[mLoginTools getCarId] mk_urlEncodedString], [[[mLoginTools getUserData] objectForKey:@"OwnerName"] mk_urlEncodedString]];
//
        NSString *url = [NSString stringWithFormat:srcUrl, [item objectForKey:@"Id"], [tf.text mk_urlEncodedString], [@"XX-1234" mk_urlEncodedString], [@"未登入" mk_urlEncodedString]];
        
        [currentApp.dataEngine getDataFromUrl:url params:nil isPost:NO onCompletion:^(NSString *response) {
            
            [self hideLoading];
            
            NSArray *resArr = [Utilities convertJSONStringToArray:response];
            NSDictionary *res = [resArr objectAtIndex:0];
            [Utilities showMessage:[res objectForKey:@"msg"] message:nil];
            
        } onError:^(NSError *error) {
            
            NSLog(@"ERROR: GetQuarterly");
            [self hideLoading];
            
            [Utilities showMessage:@"系統訊息" message:@"無法索取季刊，請稍候再試一次！"];
            
        }];
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [_pageCtrl setCurrentPage:(scrollView.contentOffset.x / 304)];
}
@end
