//
//  YoutubeViewController.m
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/29.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import "YoutubeViewController.h"
#import "YTPlayerView.h"

@interface YoutubeViewController () <YTPlayerViewDelegate>
{
    IBOutlet YTPlayerView *_player;
    IBOutlet UIActivityIndicatorView *_loading;
}
@end

@implementation YoutubeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_player setDelegate:self];
    [_player loadWithVideoId:self.vcode playerVars:@{@"controls" : @1,
                                                     @"playsinline" : @1,
                                                     @"autohide" : @1,
                                                     @"showinfo" : @0,
                                                     @"modestbranding" : @1,
                                                     @"autoplay" : @1
                                                     }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedPlaybackStartedNotification:) name:@"Playback started" object:nil];
    
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
        self.view.transform = CGAffineTransformIdentity;
        self.view.transform = CGAffineTransformMakeRotation(M_PI/2);
        self.view.bounds = CGRectMake(0.0, 0.0, currentApp.window.frame.size.height, currentApp.window.frame.size.width);
    }
    
    [UIView commitAnimations];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
        self.view.transform = CGAffineTransformIdentity;
        self.view.transform = CGAffineTransformMakeRotation(-(M_PI/2));
        self.view.bounds = CGRectMake(0.0, 0.0, currentApp.window.frame.size.width, currentApp.window.frame.size.height);
    }
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
- (IBAction)closePlayer:(id)sender
{
    [_player stopVideo];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView
{
    [_loading stopAnimating];
    [_loading setHidden:YES];
}

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification
{
    if([notification.name isEqual:@"Playback started"] && notification.object != self)
    {
        [_player pauseVideo];
    }
}

@end
