//
//  BuyCarController.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2014/6/3.
//  Copyright (c) 2014年 LinkinEDGE. All rights reserved.
//

#import "BuyCarController.h"

@interface BuyCarController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *_ciWeb;
}


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progress;

@end


@implementation BuyCarController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"本月購車優惠";
    [self.progress startAnimating];
     [_ciWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.hyundai-motor.com.tw/hyundai/Shopping/special-offer"]]];
    _ciWeb.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - webviewdeleatge
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Disable 長按(右鍵)功能, WebView Default 會顯示 copy/paste 等預設功能
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    
    //source: http://blog.techno-barje.fr/post/2010/10/04/UIWebView-secrets-part1-memory-leaks-on-xmlhttprequest/
	[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [self.progress stopAnimating];
    [self.progress removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

@end
