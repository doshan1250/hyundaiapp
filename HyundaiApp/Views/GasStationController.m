//
//  GasStationController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "GasStationController.h"
#import "MapViewTools.h"
#import "GasPriceViewController.h"
#import "ListOptionViewController.h"
#import <MapKit/MapKit.h>
#import "PlacePin.h"
#import "ListResultViewController.h"
#import "ViewWithMapViewController.h"
#import "MapForGoogleAPIViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface GasStationController ()<LStableCellSelectDelegate,MKMapViewDelegate,NavMapDelegate,ListResultDelegate,mapToolDelegate,UIAlertViewDelegate>{
    MapViewTools *mMapViewTools;
    GasPriceViewController *mGasPriceViewController;
    ListOptionViewController *mListSearchAreaOptionViewController;
    //地圖的data從網路上抓下來以後就先暫時存著，下次開啟頁面再重讀
    NSDictionary *stationData;
    NSString *_areaName;
    NSArray *_areaIDArray;
    NSArray *areaDataArray;
    //作為列表查詢用的id
    NSString *_areaId;
    ListResultViewController *mListResultViewController;
    //地圖點擊後產生的資料，在資料和點之間建立一個連結的索引表
    NSMutableDictionary *mapDicts;
    NSArray *stationDataArray;
    ViewWithMapViewController *mViewWithMapViewController;
    
    CLLocation *mCLLocation;
    UIButton *nav_button;
    BOOL isAlertOpen;
    
}

@property (strong, nonatomic) IBOutlet UIView *listPage;
@property (strong, nonatomic) IBOutlet UIButton *bt_area_listPage;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *bt_map;
@property (strong, nonatomic) IBOutlet UIButton *bt_list;
@property (strong, nonatomic) IBOutlet UIButton *bt_price;
@property (strong, nonatomic) IBOutlet UIButton *bt_submit;

@end

@implementation GasStationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //先隱藏起來，因為第一個畫面應該是地圖
    _listPage.hidden=YES;
    isAlertOpen=NO;
    
    _titleView.text = @"加油站";
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady1:"];

    areaDataArray=[currentApp.areaDataArray copy];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!mMapViewTools){
        mMapViewTools=[[MapViewTools alloc]init];
        [mMapViewTools setDelegate:self];
    }
    [self addMapView];
    
    [self.bt_list setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_price setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    [currentApp.dataMgr delObserverData:self];
    [mViewWithMapViewController setDelegate:nil];
    mViewWithMapViewController=nil;
    mListSearchAreaOptionViewController =nil;
    mGasPriceViewController=nil;
}

-(BOOL)clickedHomeEvent{
    if(mListResultViewController &&[mListResultViewController.view superview]){
        [mListResultViewController.view removeFromSuperview];
    }
    [mListResultViewController setDelegate:nil];
    mListResultViewController=nil;
    if(mViewWithMapViewController){
        if([mViewWithMapViewController.view superview]){
            [mViewWithMapViewController.view removeFromSuperview];
        }
        [mViewWithMapViewController setDelegate:nil];
        mViewWithMapViewController=nil;
    }
    mMapViewTools=nil;
    [mListSearchAreaOptionViewController setDelegate:nil];
    return YES;
}

- (void)onDataReady1:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_GasPrice){
        NSDictionary * data = [notify objectForKey:API_NotifyData];
        if(data){
            //NSLog(@"Request_GasPrice, data: %@",data);
            NSArray *dataArray=[data objectForKey:@"data"];
            for(int n=0;n<[dataArray count];n++){
                [mGasPriceViewController setGAsPrice:[((NSDictionary *)[dataArray objectAtIndex:n]) objectForKey:@"price"] withIndex:n];
                
            }//end for
            
        }else{
            if(!isAlertOpen){
            UIAlertView * alert =
            [[UIAlertView alloc]
             initWithTitle:@"獲取油價資訊錯誤"
             message: @"請離開並重新嘗試。"
             delegate:self
             cancelButtonTitle:nil
             otherButtonTitles:@"OK", nil];
            [alert show];
                isAlertOpen=YES;
            }
        }
        [mGasPriceViewController stopProgress];
    }
    else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_GasStation){
        stationDataArray = [notify objectForKey:API_NotifyData];
        MKMapView *map=[mMapViewTools getMapView];
        [map removeAnnotations:map.annotations];
        mapDicts=[[NSMutableDictionary alloc]init];
        for(int n=0;n<stationDataArray.count;n++){
            NSDictionary *data=[stationDataArray objectAtIndex:n];
            NSString *siteName= [data objectForKey:@"siteName"];
            /**
             為了讀取重複資料的錯誤避免 這裡把資料做一份hash map
             用標點的title作為key 點擊button的時候依據key取得hash map的內容
             */
            [mapDicts setObject:[data copy] forKey:siteName];
            [mMapViewTools putPoint:siteName andSubTitle:@"" andLat:[[data objectForKey:@"lat"] doubleValue] andLng:[[data objectForKey:@"lng"] doubleValue]];
        }
    }
    else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_GasData){
        NSArray *array = [[notify objectForKey:API_NotifyData]copy];
        if(array.count>0){
            BOOL superView=YES;
            if(!mListResultViewController){
                mListResultViewController=[[ListResultViewController alloc]initWithNibName:@"ListResultViewController" bundle:nil];
                mListResultViewController.view.frame=self.contentView.frame;
                superView=NO;
                
            }
            [mListResultViewController setDelegate:self];
            [mListResultViewController setAreaDatas:array andImage:[UIImage imageNamed:@"icon_g.png"]];
            [mListResultViewController setLocation:[mMapViewTools getNowLocation]];
            if(superView){
                [self.contentView bringSubviewToFront:mListResultViewController.view];
                [mListResultViewController reloadData];
            }else{
                [self.contentView addSubview:mListResultViewController.view];
            }
        }else{
            UIAlertView *alertView;
            alertView=[[UIAlertView alloc]initWithTitle:nil message:@"查無該地區加油站資料！" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else if ([[notify objectForKey:API_NotifyKey] intValue] == Request_ParkingArea){
        areaDataArray = [notify objectForKey:API_NotifyData];
        [self showData];
    }//end if else
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
            [self.navigationController popViewControllerAnimated:YES];
			break;
		default:
			break;
	}
}

#pragma mark - 地圖相關呼叫方法
-(void)addMapView{
    _listPage.hidden=YES;
    MKMapView *view=[mMapViewTools getMapView];
    if(![view superview]){
        view.frame=self.contentView.frame;
        [self.contentView addSubview:view];
    }else{
        [self.contentView bringSubviewToFront:view];
    }
    view.delegate=self;
}

-(void)loadDataWithMap{
    [mMapViewTools moveToNow];
    CLLocationCoordinate2D mCLLocationCoordinate2D=[mMapViewTools getNowLocation2D];
    [currentApp.dataMgr fireRequest:Request_GasStation postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%g",mCLLocationCoordinate2D.latitude],@"lat",[NSString stringWithFormat:@"%g",mCLLocationCoordinate2D.longitude],@"lng",@"5",@"ran", nil]];
}

//地圖頁面選擇
- (IBAction)select_map:(UIButton *)sender {
    [self.bt_list setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_price setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [self addMapView];
}
//列表頁面選擇
- (IBAction)select_list:(UIButton *)sender {
    _listPage.hidden=NO;
    [self.bt_list setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [self.bt_price setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.contentView bringSubviewToFront:_listPage];
}

- (IBAction)select_price:(UIButton *)sender {
    _listPage.hidden=YES;
    [self.bt_list setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    [self.bt_price setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [self.bt_map setBackgroundImage:[UIImage imageNamed: @"btn_unSelect.png"] forState:UIControlStateNormal];
    if(!mGasPriceViewController){
        mGasPriceViewController=[[GasPriceViewController alloc]initWithNibName:@"GasPriceViewController" bundle:nil];
    }
    UIView *view=mGasPriceViewController.view;
    if(view)
    {
        if(![view superview]){
            view.frame=self.contentView.frame;
            [self.contentView addSubview:view];
            [currentApp.dataMgr fireRequest:Request_GasPrice postData:nil];

           
        }else{
//            [self.contentView bringSubviewToFront:view];
            [view removeFromSuperview];
            [self.contentView addSubview:view];
            [mGasPriceViewController stopProgress];
        }
        
    }else {
        UIAlertView * alert =
        [[UIAlertView alloc]
         initWithTitle:@"錯誤訊息"
         message: @"獲取油價資料錯誤，請離開後重新嘗試"
         delegate:nil
         cancelButtonTitle:nil
         otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }

}

- (IBAction)listAreaChoose:(UIButton *)sender {
    [self.bt_area_listPage setEnabled:NO];
    //列表的頁面中，點選地區選擇
    [self showData];
}

-(void) showData{
    _areaName=@"";
    NSMutableArray *mNSMutableArray = [[NSMutableArray alloc]init];
    for(int n=0;n<[areaDataArray count];n++){
        NSDictionary * mNSDictionary=[areaDataArray objectAtIndex:n];
        [mNSMutableArray addObject:[mNSDictionary objectForKey:@"city"]];
    }
    NSArray *areaNames=[NSArray arrayWithArray:mNSMutableArray];
    
    mListSearchAreaOptionViewController=[[ListOptionViewController alloc]initWithNibName:@"ListOptionViewController" bundle:nil];
    
    [mListSearchAreaOptionViewController setDatas:areaNames];
    [mListSearchAreaOptionViewController setDelegate:self];
    mListSearchAreaOptionViewController.view.frame=currentApp.self.window.frame;
    [currentApp.self.window addSubview:mListSearchAreaOptionViewController.view];
    [self.bt_area_listPage setEnabled:YES];
}

-(void)dismiss:(UIView *)view andTableRowNum:(NSNumber *)row andDataName:(NSString *)areaName{
    //點選了地區之後回傳地區參數
    if(_areaIDArray){
        //第二層獲得
        [view removeFromSuperview];
        [self.bt_area_listPage setTitle:[NSString stringWithFormat:@"%@-%@",_areaName,areaName] forState:UIControlStateNormal];
        _areaId=[[_areaIDArray objectAtIndex:[row integerValue]] objectForKey:@"areaid"];
        //最後消除這個內容
        _areaIDArray=nil;
        [self.bt_submit setEnabled:YES];
        mListSearchAreaOptionViewController =nil;
    }else{
        [self.bt_area_listPage setTitle:areaName forState:UIControlStateNormal];
        [self.bt_area_listPage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _areaName=areaName;
        _areaIDArray =[[[areaDataArray objectAtIndex:[row integerValue]] objectForKey:@"area"] copy];
        //跳第二層選擇細節
        
        NSMutableArray *mNSMutableArray = [[NSMutableArray alloc]init];
        for(int n=0;n<[_areaIDArray count];n++){
            NSDictionary * mNSDictionary=[_areaIDArray objectAtIndex:n];
            [mNSMutableArray addObject:[mNSDictionary objectForKey:@"name"]];
        }
        NSArray *areaNames=[NSArray arrayWithArray:mNSMutableArray];
        [mListSearchAreaOptionViewController setDatas:areaNames];
        [mListSearchAreaOptionViewController setDelegate:self];
        mListSearchAreaOptionViewController.view.frame=currentApp.self.window.frame;
        [mListSearchAreaOptionViewController reloadData];
    }
}

-(void)touchOutSide:(UIView *)view{
    [view removeFromSuperview];
    mListSearchAreaOptionViewController =nil;
    [self.bt_submit setEnabled:YES];
}

- (IBAction)listSubmit:(UIButton *)sender {
    //列表的頁面中，點選送出跳轉頁面
    if(_areaName &&![_areaName isEqualToString:@""]){
        //跳轉頁面
        [currentApp.dataMgr fireRequest:Request_GasData postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: _areaId,@"areaid", nil]];
    }
}

#pragma mark methods for MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)map
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    //ref:https://developer.apple.com/library/ios/DOCUMENTATION/UserExperience/Conceptual/LocationAwarenessPG/AnnotatingMaps/AnnotatingMaps.html#//apple_ref/doc/uid/TP40009497-CH6-SW6
    static NSString *AnnotationViewID = @"annotationViewID";
    static NSString *currentloc=@"currentloc";
    if ([[annotation title] isEqualToString:@"now"]) {
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:currentloc];
        if(!pinView){
            pinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:currentloc];
            [pinView setImage:[UIImage imageNamed:@"icon_my_location.png"]];
            pinView.canShowCallout = NO;
        }
        else{
            pinView.annotation = annotation;
        }
        return pinView;
    }else{
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if (!pinView)
        {
            //這裡針對大頭針做各種設定
            MKAnnotationView* customPinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:AnnotationViewID];
            //設置圖釘圖案
            customPinView.image=[UIImage imageNamed:@"icon_map_g.png"];
            //顯示視窗
            customPinView.canShowCallout = YES;
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    if ([[[view annotation] title] isEqualToString:@"now"] ) {
        return;
    }
    [nav_button removeFromSuperview];
    [mViewWithMapViewController.view removeFromSuperview];
}

//地圖marker 被點擊的事件
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    if ([[[view annotation] title] isEqualToString:@"now"] ) {
        return;
    }
    // 要popup底下一個畫面上來
    NSDictionary *dict=[mapDicts objectForKey:[[view annotation] title]];
    
    [mMapViewTools moveToPostionWithLat:[[dict objectForKey:@"lat"]doubleValue ] andLng:[[dict objectForKey:@"lng"]doubleValue]];
    if(!mViewWithMapViewController){
        mViewWithMapViewController=[[ViewWithMapViewController alloc] initWithNibName:@"ViewWithMapViewController" bundle:nil];
    }
    if(![mViewWithMapViewController.view superview])
        [self.contentView addSubview:mViewWithMapViewController.view];
    else
        [self.contentView bringSubviewToFront:mViewWithMapViewController.view];
    
    [mViewWithMapViewController enableNavButton:(mCLLocation!=nil)];
    [mViewWithMapViewController setOrigin: mCLLocation.coordinate  andDestination:CLLocationCoordinate2DMake([[dict objectForKey:@"lat"]doubleValue], [[dict objectForKey:@"lng"]doubleValue])];
    [mViewWithMapViewController setDelegate:self];
    
    CLLocation *loc=[[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"lat"]doubleValue ]  longitude:[[dict objectForKey:@"lng"] doubleValue]];
    CLLocationDistance dis=[[mMapViewTools getNowLocation] distanceFromLocation:loc];
    NSString *mString=[NSString stringWithFormat:@"%@\n(%@, %@)",[dict objectForKey:@"alladdress"],[dict objectForKey:@"lat"],[dict objectForKey:@"lng"]];
    [mViewWithMapViewController setTitleForGas:[dict objectForKey:@"siteName"] andAlladdress:mString andIcon:@"icon_g.png" andDis:[NSString stringWithFormat:@"%.2f", dis/1000.00]];
    
    mViewWithMapViewController.view.frame=CGRectMake(0, self.contentView.frame.size.height-mViewWithMapViewController.view.frame.size.height, mViewWithMapViewController.view.frame.size.width, mViewWithMapViewController.view.frame.size.height);
    
    nav_button=[mViewWithMapViewController getnavMapBtn];
    CGRect frame=nav_button.frame;
    [nav_button removeFromSuperview];
    [nav_button setFrame:CGRectMake(frame.origin.x, mViewWithMapViewController.view.frame.origin.y+frame.origin.y, frame.size.width, frame.size.height)];
    [self.contentView addSubview:nav_button];
}

- (void)locMag:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    //偵測移動重新計算產生點
    if(mCLLocation){
        CLLocationDistance dis=[newLocation distanceFromLocation:mCLLocation];
        if(dis>1000){
            mCLLocation=[newLocation copy];
            [self loadDataWithMap];
        }
    }else{
        mCLLocation=[newLocation copy];
        [self loadDataWithMap];
    }
    [mMapViewTools putUserPoint:mCLLocation.coordinate andTitle:@"now" andSubTitle:@""];
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
}

#pragma mark - NavMapDelegate
-(void)navForOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest{
    MapForGoogleAPIViewController *vc=[[MapForGoogleAPIViewController alloc]initWithNibName:@"MapForGoogleAPIViewController" bundle:nil];
    [vc setType:1];
    [vc setOrigin:origin andDestination:dest];
    [self.navigationController pushViewController:vc animated:YES];
    vc=nil;
}

#pragma mark - ListResultDelegate
-(void)clickedItem:(CLLocationCoordinate2D)dest{
    CLLocationCoordinate2D mCLLocationCoordinate2D=[mMapViewTools getNowLocation2D];
    if(mCLLocationCoordinate2D.latitude==0.00){
        UIAlertView *alertView;
        alertView=[[UIAlertView alloc]initWithTitle:nil message:@"目前尚無位置資訊，請稍候再嘗試" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    MapForGoogleAPIViewController *vc=[[MapForGoogleAPIViewController alloc]initWithNibName:@"MapForGoogleAPIViewController" bundle:nil];
    [vc setType:1];
    [vc setOrigin:mCLLocationCoordinate2D andDestination:dest];
    [self.navigationController pushViewController:vc animated:YES];
    vc=nil;
}

@end
