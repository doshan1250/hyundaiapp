//
//  MapViewViewController.h
//  MapViewTest
//
//  Created by ShuHsien Lin on 2013/11/10.
//  Copyright (c) 2013年 Linkinedge. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PlacePin.h"

//#import "calloutAccessoryControlTappedDelegate.h"

@protocol mapToolDelegate


- (void)locMag:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

@end

@interface MapViewTools : NSObject{
    MKMapView *mapView;
}

@property(strong,nonatomic)id<mapToolDelegate> delegate;

-(PlacePin *)putPoint:(NSString *)title andSubTitle:(NSString *)subTitle andLat:(CLLocationDegrees )latitude andLng:(CLLocationDegrees)longitude;
-(PlacePin *)putPoint:(CLLocationCoordinate2D )coordinate andTitle:(NSString *)title andSubTitle:(NSString *)subTitle;

-(void)moveToPostionWithLat:(double)lat andLng:(double)lng;
//移動地圖到目前位置
- (void)moveToNow;
//獲得目前位置
-(CLLocationCoordinate2D )getNowLocation2D;
-(CLLocation *)getNowLocation;
-(MKMapView *)getMapView;
-(PlacePin *)putUserPoint:(NSString *)title andSubTitle:(NSString *)subTitle andLat:(CLLocationDegrees )latitude andLng:(CLLocationDegrees)longitude;
-(PlacePin *)putUserPoint:(CLLocationCoordinate2D )coordinate andTitle:(NSString *)title andSubTitle:(NSString *)subTitle;

@end