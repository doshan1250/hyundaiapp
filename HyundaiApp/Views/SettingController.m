//
//  SettingController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/10.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "SettingController.h"
#import "LoginTools.h"
#import "ProgressViewController.h"


@interface SettingController ()<UIAlertViewDelegate,UITextFieldDelegate,ProgressDelegate>{
    LoginTools *mLoginTools;
    ProgressViewController *mProgressViewController;
}

@property (strong, nonatomic) IBOutlet UITextField *textField_carId;
@property (strong, nonatomic) IBOutlet UITextField *textField_userId;
@property (strong, nonatomic) IBOutlet UIButton *btn_submit;

@property (strong, nonatomic) IBOutlet UISwitch *sw_Policy;

@end

@implementation SettingController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    _titleView.text = @"車主資訊";
    
    [self.view setBackgroundColor:HYUNDAI_BG_GREY];
    mLoginTools=[LoginTools new];
    self.textField_carId.delegate=self;
    self.textField_userId.delegate=self;
    
    [currentApp.dataMgr addObserverData:self callback:@"callback:"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submit:(UIButton *)sender {
    [self.btn_submit setEnabled:NO];
    [self.textField_carId resignFirstResponder];
    [ self.textField_userId resignFirstResponder];
    //送出資料
    NSString *carId , *userId;
    //全部轉大寫
    carId=[self.textField_carId.text uppercaseString];
    userId=[self.textField_userId.text uppercaseString];
    
    BOOL error=false;
    NSString *strMsg = @"";
    if(!carId||[carId isEqualToString:@""]){
        error=true;
        strMsg = @"身分証字號不得為空！";
    }else if(!userId ||[userId isEqualToString:@""]){
        error=true;
        strMsg = @"車牌號碼不得為空！";
    }else if (!self.sw_Policy.on) {
        error=true;
        strMsg = @"您必須同意個人資料提供HYUNDAI好行APP使用";
    }
    
    if(error){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:strMsg
                                                       delegate:nil
                                              cancelButtonTitle:@"確認"
                                              otherButtonTitles:nil];
        //        - See more at: http://furnacedigital.blogspot.tw/2010/12/alerts.html#sthash.KOOv1rvq.dpuf
        [alert show];
        [self.btn_submit setEnabled:YES];
    }else{
        mProgressViewController=[[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
        [mProgressViewController setDelegate:self];
        mProgressViewController.view.frame=currentApp.window.frame;
        [currentApp.window addSubview:mProgressViewController.view];
        
        [currentApp.dataMgr fireRequest:Request_UserLogin postData:[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",carId],@"cid",userId,@"pid", nil]];
    }
}//end function




#pragma mark - Data Notify
- (void)callback:(NSNotification *)pNotify{
    NSDictionary *notify = [pNotify userInfo];
    
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_UserLogin)
    {
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
        
        NSDictionary *dic = [notify objectForKey:API_NotifyData];
        
        NSString *mString=[dic objectForKey:@"ERROR"];
        [self.btn_submit setEnabled:YES];
        if(!mString||[mString isEqualToString:@""]){
            //如果不是錯誤則進行儲存動作
            [mLoginTools putUserId:self.textField_userId.text andCarId:self.textField_carId.text andData:[dic copy]];
            
            if(!delegate ||[delegate success]){
                [self.navigationController popViewControllerAnimated:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"登入成功！"
                                                               delegate:nil
                                                      cancelButtonTitle:@"確認"
                                                      otherButtonTitles:nil];
                [alert show];
                
                // 存 Device Token
                if ([currentApp getDeviceToken])
                {
                    NSLog(@"更新 DeviceToken");
                    
                    NSString *srcUrl = [NSString stringWithFormat:API_HOST, @"SetDeviceToken2.ashx?t=%@&d=iOS&plate=%@&name=%@"];
                    NSString *url = [NSString stringWithFormat:srcUrl, [currentApp getDeviceToken], [self.textField_carId.text mk_urlEncodedString], [[dic objectForKey:@"OwnerName"] mk_urlEncodedString] ];
                    [currentApp.dataEngine getDataFromUrl:url params:nil isPost:NO onCompletion:^(NSString *response) {
                        
                    } onError:^(NSError *error) {
                        
                    }];
                }
                
            }
            if(!_singleMode){
                //如果存在不是自己的ViewController開啟的話
                [self.view removeFromSuperview];
            }
        }else{
            if(!delegate || [delegate fail]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"登入失敗，請確認輸入是否正確？"
                                                               delegate:nil
                                                      cancelButtonTitle:@"確認"
                                                      otherButtonTitles:nil];
                [alert show];
            }//end if
        }//end if else
    }//end if notify
}


-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
}

- (void)dealloc
{
    [currentApp.dataMgr delObserverData:self];
}

-(BOOL)clickedHomeEvent{
        return YES;
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(self.textField_carId.text.length>0){
        self.textField_carId.text=[self.textField_carId.text uppercaseString];
    }
    
    if(self.textField_userId.text.length>0){
        self.textField_userId.text=[self.textField_userId.text uppercaseString];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    int MAXLENGTH=0;
    if(textField==self.textField_carId){
        MAXLENGTH=8;
    }else if(textField==self.textField_userId){
        MAXLENGTH=10;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > MAXLENGTH) ? NO : YES;
    
}

#pragma mark - 設定如果true 則該為單ㄧ頁面，反之為跳轉的頁面
-(void)setMode:(BOOL)singleMode{
    _singleMode=singleMode;
}

#pragma mark - ProgressDelegate
-(void)timeout{
    if([mProgressViewController.view superview]){
        [mProgressViewController.view removeFromSuperview];
        mProgressViewController=nil;
    }
    [self.btn_submit setEnabled:YES];
}


@end
