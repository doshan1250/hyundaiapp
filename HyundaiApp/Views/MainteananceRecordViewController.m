//
//  mainteananceRecordViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/22.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "MainteananceRecordViewController.h"

@interface MainteananceRecordViewController ()

@end

@implementation MainteananceRecordViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)exit:(UIButton *)sender {
    if(delegate){
        [delegate dismissView:self.view];
    }
}

@end
