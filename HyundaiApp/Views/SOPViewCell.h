//
//  SOPViewCell.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/5.
//  Copyright (c) 2013年 Corma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOPViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_call;
@property (strong, nonatomic) IBOutlet UIImageView *img_down;
@property (strong, nonatomic) IBOutlet UILabel *label_step;

@property (nonatomic, strong) IBOutlet UIImageView *imgIcon;
@property (nonatomic, strong) IBOutlet UIImageView *imgNext;
@property (nonatomic, strong) IBOutlet UILabel *lbTitle;
@end
