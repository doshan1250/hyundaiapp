//
//  CommunicationManager.m
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "CommunicationManager.h"

// Class & Manager
#import "CommunicationContext.h"
#import "Reachability.h"

// ASIHTTPRequest
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@implementation CommunicationManager

@synthesize pCtxQueue;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        pCtxQueue = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    if (pCtxQueue)
        pCtxQueue = nil;
}

#pragma mark -

- (ASIHTTPRequest *)doRequest:(CommunicationContext *)pCtx
{   
    ASIHTTPRequest *request = nil;
    
    if ([pCtx.url isEqualToString:@""] || pCtx.url == nil)
    {
        NSLog(@"DJCM: pCtx URL 為空值, 取消 Request");
    }
    
    // process: pCtx Queue
    else if ([pCtxQueue objectForKey:pCtx.url] == nil)
    {
        //Log(DJLogTypeDebug, @"DJCM: pCtxQueue: %@", pCtx.url);
        [pCtxQueue setValue:pCtx forKey:pCtx.url];
        
        request = [self doASIHTTPRequest:pCtx];
    }
    else
    {
        NSLog(@"DJCM: 重覆 pCtx, 目前已有相同 API 正在執行: %@", pCtx.url);
    }
    
    return request;
}

- (ASIHTTPRequest *)doASIHTTPRequest:(CommunicationContext *)pCtx
{   
    ASIHTTPRequest *request = nil;
    
    if ([pCtx.method isEqualToString:@"GET"])
    {   
        //NSLog(@"DJCM: <GET> : %@, URL: %@", (pCtx.isSync ? @"Sync" : @"Async"), pCtx.url);
        
        request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:pCtx.url]];
        [request setDelegate:self];
        [request setUserInfo:[NSDictionary dictionaryWithObject:pCtx forKey:@"pCtx"]];
        [request setTimeOutSeconds:(![pCtx.errorMessage isEqualToString:@""] ? 1 : 30)];
        [request setNumberOfTimesToRetryOnTimeout:3];
        
        if (pCtx.isSync)
        {
            [request startSynchronous];
        }
        else
        {
            [request startAsynchronous];
        }
    }
    else
    {
        //NSLog(@"DJCM: <POST> : %@, URL: %@", (pCtx.isSync ? @"Sync" : @"Async"), pCtx.url);
        
        // 如果 postData 是 NSString 就直接傳出去
        if ([pCtx.postData isKindOfClass:[NSString class]])
        {   
           // NSLog(@"DJCM: <POST> : postData");
            
            request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:pCtx.url]];
            [request appendPostData:[(NSString *)pCtx.postData dataUsingEncoding:NSUTF8StringEncoding]];
            [request setRequestMethod:@"POST"];
        }
        
        // 如果 postData 是 NSDictionary 就必須檢查是否有 File 要上傳
        else
        {
            //NSLog(@"DJCM: <POST> : postDict");
            
            request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:pCtx.url]];
            
            for (NSString *key in pCtx.postData)
            {
                // NSDictionary for Upload Files
                if ([[pCtx.postData objectForKey:key] isKindOfClass:[NSMutableDictionary class]])
                {
                    NSDictionary *dictFile = [pCtx.postData objectForKey:key];
                    
                    for (NSString *fileKey in dictFile)
                    {
                        [(ASIFormDataRequest *)request setFile:[dictFile objectForKey:fileKey] forKey:fileKey];
                        //NSLog(@"DJCM: postFile: %@ => %@", fileKey, [dictFile objectForKey:fileKey]);
                    }
                }
                
                // NSString Key/Value
                else
                {
                    [(ASIFormDataRequest *)request setPostValue:[pCtx.postData objectForKey:key] forKey:key];
                    //NSLog(@"DJCM: postData: %@ => %@", key, [pCtx.postData objectForKey:key]);
                }
                
                // Other POST data
                //[request setFile:@"/Users/ben/Desktop/ben.jpg" forKey:@"photo"];
                //[request setData:imageData withFileName:@"myphoto.jpg" andContentType:@"image/jpeg" forKey:@"photo"];
            }
            [request setRequestMethod:@"POST"];
        }
        
        [request setDelegate:self];
        [request setUserInfo:[NSDictionary dictionaryWithObject:pCtx forKey:@"pCtx"]];
        [request setTimeOutSeconds:(![pCtx.errorMessage isEqualToString:@""] ? 1 : 30)];
        [request setNumberOfTimesToRetryOnTimeout:3];
        
        if (pCtx.isSync)
        {
            [request startSynchronous];
        }
        else
        {
            [request startAsynchronous];
        }
    }
    
    // wait response, GOTO: ASIHTTPReqeust Delegate
    return request;
}

#pragma mark -
#pragma mark ASIHTTPReqeust Delegate

- (void)requestStarted:(ASIHTTPRequest *)request
{
}

- (void)requestFinished:(ASIHTTPRequest *)request
{   
    //NSLog(@"DJCM: HTTP 連線結束, 開始檢查 Status Code ...");
    
    CommunicationContext *pCtx = [[request userInfo] objectForKey:@"pCtx"];
    
    // remove from Queue
    [pCtxQueue removeObjectForKey:pCtx.url];
    
    if([request responseStatusCode] != 200)
    {
       // NSLog(@"DJCM: HTTP StatusCode != 200, 發生錯誤: %d", [request responseStatusCode]);
        
        pCtx.errorCode = [request responseStatusCode];
        pCtx.errorMessage = [request responseStatusMessage];
    }
    else
    {
        //NSLog(@"DJCM: HTTP StatusCode = 200, 收到正確資料");
        
        // 將收到的 NSData 重新 alloc memory 進去避免 Parser 發生錯誤
        pCtx.retData = [[NSData alloc] initWithData:[request responseData]];
    }
    
    [pCtx.delegate handleCommunicationResult:pCtx];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    CommunicationContext *pCtx = [[request userInfo] objectForKey:@"pCtx"];
    
   // NSLog(@"DJCM: HTTP 連線失敗: %d, 設定 pCtx.errorCode = -1", [request responseStatusCode]);

    // remove from Queue
    [pCtxQueue removeObjectForKey:pCtx.url];
    
    pCtx.errorCode = -1;
    pCtx.errorMessage = @"Connection Failed";
    
    [pCtx.delegate handleCommunicationResult:pCtx];
}

@end
