//
//  CubeButton.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/9.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "CubeButton.h"

@interface CubeButton ()
{
    id<CubeButtonDelegate> _delegate;
    
    UIImageView *_imgIcon;
    UIImageView *_imgText;
    
    UITapGestureRecognizer *_tap;
    UISwipeGestureRecognizer *_swipeLeft;
    UISwipeGestureRecognizer *_swipeRight;
}

@end

@implementation CubeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createUI];
    }
    return self;
}

- (void)dealloc
{
    [self destroyUI];
}

#pragma mark - Private API
- (void)createUI
{
    _imgText = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:_imgText];
    
    _imgIcon = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:_imgIcon];
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapButton)];
    [self addGestureRecognizer:_tap];
    
    _swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    [_swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:_swipeLeft];
    
    _swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    [_swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:_swipeRight];
    
    //susan edit for bg color
//    [self setBackgroundColor:[[UIColor alloc] initWithRed:38.0/255.0 green:136.0/255.0 blue:233.0/255.0 alpha:1]];
}

- (void)destroyUI
{
    if (_tap)
    {
        [_tap removeTarget:self action:@selector(tapButton)];
        [self removeGestureRecognizer:_tap];
    }
    
    if (_swipeLeft)
    {
        [_swipeLeft removeTarget:self action:@selector(swipeLeft)];
        [self removeGestureRecognizer:_swipeLeft];
    }
    
    if (_swipeRight)
    {
        [_swipeRight removeTarget:self action:@selector(swipeRight)];
        [self removeGestureRecognizer:_swipeRight];
    }
    
    if (_imgIcon)
    {
        if ([_imgIcon superview])
            [_imgIcon removeFromSuperview];
        _imgIcon = nil;
    }
    
    if (_imgText)
    {
        if ([_imgText superview])
            [_imgText removeFromSuperview];
        _imgText = nil;
    }
}

- (void)tapButton
{
    if (_delegate)
    {
        if ([_delegate respondsToSelector:@selector(clickCubeButton:)])
            [_delegate clickCubeButton:self];
    }
    else
    {
        //NSLog(@"id<CubeButtonDelegate> NOT implemented");
    }
}

- (void)swipeLeft
{
    //TODO 先拿掉，如果有需要在放回來
    //NSLog(@"swipeLeft");
//    [UIView transitionFromView:_imgIcon
//                        toView:_imgText
//                      duration:1
//                       options:UIViewAnimationOptionTransitionFlipFromRight
//                    completion:nil];
}

- (void)swipeRight
{
    //TODO 先拿掉，如果有需要在放回來
    //NSLog(@"swipeRight");
//    [UIView transitionFromView:_imgText
//                        toView:_imgIcon
//                      duration:1
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    completion:nil];
}

#pragma mark - Public API
- (void)setDelegate:(id<CubeButtonDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setImgIcon:(UIImage *)imgIcon imgText:(UIImage *)imgText
{
    [_imgIcon setImage:imgIcon];
    [_imgText setImage:imgText];
}

@end
