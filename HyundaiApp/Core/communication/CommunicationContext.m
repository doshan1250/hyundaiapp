//
//  CommunicationContext.m
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "CommunicationContext.h"

@implementation CommunicationContext

@synthesize delegate;
@synthesize action, url, method, postData, retData, userObj;
@synthesize errorCode, errorMessage, isSync;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        action = 0;
        
        url = nil;
        method = @"POST";
        postData = nil;
        
        retData = nil;
        userObj = nil;
        
        errorCode = 0;
        errorMessage = @"";
        
        isSync = NO;
    }
    
    return self;
}

- (void)dealloc
{    
    if (postData)
        postData = nil;
    
    if (retData)
        retData = nil;
    
    if (userObj)
        userObj = nil;
}

@end