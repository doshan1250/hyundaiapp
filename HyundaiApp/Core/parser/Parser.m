//
//  Parser.m
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "Parser.h"

#import "NSString+Plus.h"

@implementation Parser

@synthesize errCode, errMsg, dictRes, errParser;

+ (id)nodeValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
	id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen - 1; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return [tmp objectForKey:[arPath objectAtIndex:(nLen -1)]];
}

+ (NSMutableArray *) arrValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [Parser nodeValue:node ofPath:path];
}

+ (NSDictionary *) dictValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
    id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return tmp;
}

+ (NSString *) stringValue:(NSDictionary *)node ofPath:(NSString *)path
{
    return nil;
}

+ (int) intValue:(NSDictionary *)node ofPath:(NSString *)path
{
    return 0;
}

- (id)initWithData:(NSData *)data
{
	self = [super init];
	if (self) {
	}
	return self;
}

- (id)initWithString:(NSString *)data
{
	self = [super init];
	if (self) {
	}
	return self;
}

- (void)dealloc
{
	if (dictRes) {
		dictRes = nil;
	}
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        errCode = [decoder decodeObjectForKey:@"errCode"];
        errMsg = [decoder decodeObjectForKey:@"errMsg"];
        dictRes = [decoder decodeObjectForKey:@"dictRes"];
        errParser = [decoder decodeBoolForKey:@"errParser"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:errCode forKey:@"errCode"];
    [encoder encodeObject:errMsg forKey:@"errMsg"];
    [encoder encodeObject:dictRes forKey:@"dictRes"];
    [encoder encodeBool:errParser forKey:@"errParser"];
}

- (void)checkError:(NSError *)parseError
{
}

- (id)nodeOfPath:(NSString *)path
{
    return nil;
}

- (NSMutableArray *)arrOfPath:(NSString *)path
{
    return nil;
}

- (NSDictionary *)dictOfPath:(NSString *)path
{
    return nil;
}

- (NSString *)stringOfPath:(NSString *)path
{
    return nil;
}

- (int)intOfPath:(NSString *)path
{
    return 0;
}

#pragma mark -

+ (NSDictionary *)detectDictionaryData:(id)data dataType:(Class)aClass
{
    NSDictionary *retData = nil;
    // 判斷解析的格式是否是 NSDictionary
    if ([data isKindOfClass:aClass])
    {
        retData = nil;
    }
    else
    {
        retData = (NSDictionary *)data;
    }
    
    return retData;
}

+ (NSArray *)detectArrayData:(id)data dataType:(Class)aClass
{
    NSArray *retData = nil;
    // 判斷解析的格式是否是 NSArray
    if ([data isKindOfClass:aClass])
    {
        retData = [NSArray arrayWithObject:data];
    }
    else
    {
        retData = (NSArray *)data;
    }
    
    return retData;
}


@end
