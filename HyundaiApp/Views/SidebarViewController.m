//
//  SidebarViewController.m
//  Ma2Club
//
//  Created by Webber Chuang on 13/11/7.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//這裡目前暫時沒有用途，之後如果要做sidebar必須在appdelegate實做

#import "SidebarViewController.h"

@interface SidebarViewController ()

@end

@implementation SidebarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
}





@end
