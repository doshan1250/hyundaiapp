//
//  NSString+Plus.h
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Plus)

// String
+ (BOOL)isNullorEmpty:(NSString *)str;
- (BOOL)isEmpty;
- (BOOL)isEmptyIgnoringWhitespace:(BOOL)ignoreWhitespace;
- (NSString *)stringByTrimmingWhitespace;
+ (BOOL)isValidEmail:(NSString *)checkString;

- (int)indexOf:(NSString *)str;
- (BOOL)containsString:(NSString*)str;

+ (NSMutableString *)fileGetContens:(NSString *)filePath;

// AES
- (NSString *)AES128EncryptWithKey:(NSString *)key;
- (NSString *)AES128DecryptWithKey:(NSString *)key;

// MD5, SHA1
- (NSString*)md5;
- (NSString*)sha1;

+ (NSString *)md5:(NSString *)str;

@end

@interface NSMutableString (Plus)

- (void)trimCharactersInSet:(NSCharacterSet *)aCharacterSet;
- (void)trimWhitespace;

@end
