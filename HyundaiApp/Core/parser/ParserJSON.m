//
//  ParserJSON
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import "ParserJSON.h"

#import "NSString+Plus.h"

@implementation ParserJSON

+ (id)nodeValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
    
	id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen - 1; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return [tmp objectForKey:[arPath objectAtIndex:(nLen -1)]];
}

+ (NSMutableArray *) arrValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [ParserJSON nodeValue:node ofPath:path];
}

+ (NSDictionary *) dictValue:(NSDictionary *)node ofPath:(NSString *)path
{
    if (node == nil || [NSString isNullorEmpty:path])
        return nil;
    
    id tmp = node;
    NSArray* arPath = [path componentsSeparatedByString: @"/"];
    int nLen = [arPath count];
    for (int i = 0; i < nLen; i++) {
        tmp = [tmp objectForKey:[arPath objectAtIndex:i]];
    }
    return tmp;
}

+ (NSString *) stringValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [ParserJSON nodeValue:node ofPath:path];
}

+ (int) intValue:(NSDictionary *)node ofPath:(NSString *)path
{
	return [[ParserJSON nodeValue:node ofPath:path] intValue];
}

- (id)initWithData:(NSData *)data
{
	self = [super init];
	if (self)
    {
        NSError *parseError = nil;
		dictRes = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&parseError];
	}
	return self;
}

- (id)initWithString:(NSString *)data
{
	self = [super init];
	if (self)
    {
        NSError *parseError = nil;
        NSData *dataConvert = [data dataUsingEncoding:NSUTF8StringEncoding];
		dictRes = [NSJSONSerialization JSONObjectWithData:dataConvert options:NSJSONReadingAllowFragments error:&parseError];
		[self checkError:parseError];
	}
	return self;
}

- (void)dealloc
{
	if (dictRes) {
		dictRes = nil;
	}
}

- (void)checkError:(NSError *)parseError
{
	errParser = (parseError != nil);
    
	if (errParser)
    {
		NSLog(@"ParserJSON: checkErrorParser: 資料 Parser 錯誤");
    }
    else
    {
        errCode = @"0";
        errMsg = @"";
    }
}

- (id)nodeOfPath:(NSString *)path
{
	return [ParserJSON nodeValue:dictRes ofPath:path];
}

- (NSMutableArray *)arrOfPath:(NSString *)path
{
    return [ParserJSON arrValue:dictRes ofPath:path];
}

- (NSDictionary *)dictOfPath:(NSString *)path
{
    return [ParserJSON dictValue:dictRes ofPath:path];
}

- (NSString *)stringOfPath:(NSString *)path
{
    return [ParserJSON stringValue:dictRes ofPath:path];
}

- (int)intOfPath:(NSString *)path
{
    return [ParserJSON intValue:dictRes ofPath:path];
}
@end
