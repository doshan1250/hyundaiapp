//
//  ContactController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ContactController.h"
#import "LoginTools.h"
#import "SettingController.h"

@interface ContactController ()<LoginStatus>{
    LoginTools *mLoginTools;
    SettingController *mSettingController;
    NSString *FF,*SS;
    NSString *telF,*telS;
}
@property (strong, nonatomic) IBOutlet UILabel *label_fixTel;
@property (strong, nonatomic) IBOutlet UILabel *label_salesTel;
@property (strong, nonatomic) IBOutlet UIButton *btn_f;
@property (strong, nonatomic) IBOutlet UIButton *btn_s;

@end

@implementation ContactController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mLoginTools=[LoginTools new];
    
    _titleView.text = @"聯絡維修╱營業員";
    
    [self.view setBackgroundColor:HYUNDAI_BG_GREY];
    [self.btn_f setEnabled:NO];
    [self.btn_s setEnabled:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([mLoginTools shouldLogin]){
        mSettingController=[[SettingController alloc] initWithNibName:@"SettingController" bundle:nil];
        mSettingController.delegate=self;
        [mSettingController setMode:NO];
        [self.view addSubview:mSettingController.view];
    }else{
        [self dataInit];
    }
}

-(BOOL)success{
    //填入正確的聯絡員資料
    [self dataInit];
    
      return NO;
}

-(BOOL)fail{
    return YES;
}

-(void)dataInit{
    NSDictionary *data=[mLoginTools getUserData];
    telF=[NSString stringWithFormat:@"%@",[data objectForKey:@"SfPhone"]];
    FF=[NSString stringWithFormat:@"%@\n%@",[data objectForKey:@"SfName"],[data objectForKey:@"SfPhone"]];
    
    if(FF&&![[data objectForKey:@"SfPhone"] isEqualToString:@""]){
        [self.label_fixTel setText:FF];
    }else{
        telF=self.label_fixTel.text;
    }
    telS=[NSString stringWithFormat:@"%@",[data objectForKey:@"SaleEmpPhone"]];
    SS=[NSString stringWithFormat:@"%@\n%@",[data objectForKey:@"SaleEmpName"],[data objectForKey:@"SaleEmpPhone"]];
    if(SS&&![[data objectForKey:@"SaleEmpPhone"] isEqualToString:@""]){
        [self.label_salesTel setText:SS];
    }else{
        telS=self.label_salesTel.text;
    }
    
    telF=[telF stringByReplacingOccurrencesOfString:@"-" withString:@""];
    telS=[telS stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [self.btn_f setEnabled:YES];
    [self.btn_s setEnabled:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
}

#pragma mark - 點擊事件

- (IBAction)telF:(UIButton *)sender {//維修廠
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",telF]]];
    //NSLog(@"%@",telF);
}
- (IBAction)telS:(UIButton *)sender {//業務
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",telS]]];
    //NSLog(@"%@",telS);
}


@end
