//
//  VideoViewCell.h
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/28.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@protocol VideoViewCellDelegate <NSObject>

- (void)clickVideo:(NSInteger)index;

@end

@interface VideoViewCell : UITableViewCell

@property (nonatomic, unsafe_unretained) id<VideoViewCellDelegate> delegate;

@property (nonatomic, assign) NSInteger tag1;
@property (nonatomic, strong) IBOutlet AsyncImageView *pic1;
@property (nonatomic, strong) IBOutlet UILabel *title1;
@property (nonatomic, strong) IBOutlet UILabel *date1;

@property (nonatomic, assign) NSInteger tag2;
@property (nonatomic, strong) IBOutlet AsyncImageView *pic2;
@property (nonatomic, strong) IBOutlet UILabel *title2;
@property (nonatomic, strong) IBOutlet UILabel *date2;
@property (nonatomic, strong) IBOutlet UIImageView *bg2;
@property (nonatomic, strong) IBOutlet UIImageView *player2;
@property (nonatomic, strong) IBOutlet UIButton *btn2;


- (void)showItem2:(BOOL)show;

@end
