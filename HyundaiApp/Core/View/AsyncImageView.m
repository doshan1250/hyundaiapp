//
//  AsyncImageView.m
//  Postcard
//
//  Created by markj on 2/18/09.
//  Copyright 2009 Mark Johnson. You have permission to copy parts of this code into your own projects for any use.
//  www.markj.net
//

#import "AsyncImageView.h"

@implementation AsyncImageView

- (void)dealloc
{
	[connection cancel]; //in case the URL is still downloading
    connection = nil;

    savePath = nil;
    data = nil;
}

- (void)loadImageFromURL:(NSURL*)url savePath:(NSString *)path
{
    if ([[self subviews] count] > 0)
    {
        for (int i=0; i<[[self subviews] count]; ++i)
        {
            [[[self subviews] objectAtIndex:i] removeFromSuperview];
        }
    }
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    [loading setFrame:CGRectMake((self.frame.size.width-24)/2, (self.frame.size.height-24)/2, 24, 24)];
    [loading startAnimating];
    [self addSubview:loading];
    
	if (savePath)
        savePath = nil;
    
	if (connection)
        connection = nil;
    
	if (data)
        data = nil;
	
    if (path)
        savePath = [[NSString alloc] initWithString:path];
    
	NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
    
	//TODO error handling, what if connection is nil?
}


//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection
{
	connection=nil;
    
	if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
	
	//make an image view for the image
    UIImage *img = [[UIImage alloc] initWithData:data];
    
    if (savePath && img)
    {
        //NSLog(@"Save Image: %@", savePath);
        NSData *pngData = [NSData dataWithData:UIImagePNGRepresentation(img)];
        [pngData writeToFile:savePath atomically:YES];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
    imageView.alpha = 0.0f;
    //make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
    imageView.frame = self.bounds;
    
    [self addSubview:imageView];
    
    [UIView animateWithDuration:1.0 animations:^{
        imageView.alpha = 1.0f;
    }];
	
	[imageView setNeedsLayout];
	[self setNeedsLayout];
    
    img = nil;

	data = nil;
}

- (UIImage *)image
{
	UIImageView *iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}

@end
