//
//  UIImage+Plus.h
//
//  Created by Wei-Po Chuang on 12/4/22.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Plus)

- (UIImage *)cropImage:(CGRect)rect;
- (UIImage *)resizeImage:(CGSize)size;
- (UIImage *)maskImage:(UIImage *)maskImage;

@end
