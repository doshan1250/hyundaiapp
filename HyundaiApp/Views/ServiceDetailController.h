//
//  ServiceDetailController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>
#import "PlacePin.h"


@interface ServiceDetailController : HyundaiViewController{
    
}

@property (nonatomic, strong) NSArray *arrData;
@property (nonatomic, strong) CLLocation *curLocation;
-(void)setIsFactory:(BOOL)isFactory;
@end
