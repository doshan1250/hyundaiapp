//
//  NewsWebViewController.h
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/11/29.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsWebViewController : HyundaiViewController
-(void)setURL:(NSString *)urlString;
@end
