//
//  VideoViewCell2.h
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/28.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@protocol VideoViewCell2Delegate <NSObject>

- (void)clickVideo:(NSInteger)index;

@end

@interface VideoViewCell2 : UITableViewCell

@property (nonatomic, unsafe_unretained) id<VideoViewCell2Delegate> delegate;

@property (nonatomic, assign) NSInteger tag1;
@property (nonatomic, strong) IBOutlet AsyncImageView *pic1;
@property (nonatomic, strong) IBOutlet UILabel *title1;
@property (nonatomic, strong) IBOutlet UILabel *date1;

@end
