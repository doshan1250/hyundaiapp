//
//  ListResultViewController.m
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/14.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ListResultViewController.h"
#import "ListResultCell.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>

@interface ListResultViewController ()<UITableViewDataSource,UITableViewDelegate>{
}
@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation ListResultViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.table.dataSource=self;
    self.table.delegate=self;
}

- (void)dealloc
{
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_areaDatas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier1 = @"ListResultCell";
    ListResultCell *cell = (ListResultCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListResultCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ListResultCell *) currentObject;
                break;
            }
        }
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgView];
        
        NSDictionary *mNSDictionary=[_areaDatas objectAtIndex:indexPath.row];
        [cell.icon_type setImage: _image];
        NSString *name=[mNSDictionary objectForKey:@"name"];
        //這裡做判斷
        if(!name){
            name=[mNSDictionary objectForKey:@"siteName"];
        }
        [cell.label_name setText:name];
        [cell.label_dec setText:[mNSDictionary objectForKey:@"alladdress"]];
        //這裡有實做計算距離的方法
        if(_mCLLocation){
            CLLocation *orig=[[CLLocation alloc] initWithLatitude:[[mNSDictionary objectForKey:@"lat"] doubleValue] longitude:[[mNSDictionary objectForKey:@"lng"] doubleValue]];
            CLLocationDistance dis=[_mCLLocation distanceFromLocation:orig];
            double dis_o=(dis/1000.00);
            [cell.label_dis setText:[NSString stringWithFormat:@"%.2f",(dis_o>999)?999.00:dis_o]];
        }else{
            [cell.label_dis setText:@"0.00"];
        }
    }//enf is nil
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(delegate){
        NSDictionary *dic=[_areaDatas objectAtIndex:indexPath.row];
        [delegate clickedItem:CLLocationCoordinate2DMake([[dic objectForKey:@"lat"]doubleValue],[[dic objectForKey:@"lng"]doubleValue])];
    }
    
}

-(void)setAreaDatas:(NSArray *)areaDatas  andImage:(UIImage *)image{
    _image=image;
    _areaDatas=areaDatas;
}

-(void)setLocation:(CLLocation *)mCLLocation{
    _mCLLocation=mCLLocation;
}

-(void)reloadData{
    [self.table reloadData];
}

@end
