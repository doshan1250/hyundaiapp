//
//  VideoDetailViewController.h
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/29.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoDetailViewController : HyundaiViewController

- (void)setNewsData:(NSDictionary *)data;

@end
