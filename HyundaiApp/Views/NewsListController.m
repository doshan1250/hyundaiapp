//
//  NewsListController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/9.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "NewsListController.h"

#import "AppDelegate.h"
#import "NewsDetailController.h"

#import "NewsListCell.h"
#import "NewsWebViewController.h"

@interface NewsListController ()
{
    IBOutlet UITableView *_newsTable;
    NSArray *_arrData;
    NSMutableDictionary *alreadyReadDict;
}


@end

@implementation NewsListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
    _titleView.text = @"最新消息";
    if (IS_IOS7)
        [_newsTable setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    //將未讀通知設定為 0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    currentApp.reveal.recognizesPanningOnFrontView = YES;
    alreadyReadDict=[AppDelegate getAlreadyReadNews];
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [currentApp.dataMgr delObserverData:self];
    alreadyReadDict=nil;
}


#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SOPViewCell";
    NewsListCell *cell = (NewsListCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewsListCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (NewsListCell *) currentObject;
                break;
            }
        }
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgView];
    }
    
    NSDictionary *item = [_arrData objectAtIndex:indexPath.row];
    
    if(alreadyReadDict){
        //設定已讀標示
        NSString *key= [NSString stringWithFormat:@"%@", [item objectForKey:@"sno"]];
        NSString *value=[alreadyReadDict objectForKey:key];
        if(value&&[value isEqualToString:@"1"]){
            [cell showAlreadyReadIcon];
        }else{
            [cell hideAlreadyReadIcon];
        }
    }
    
    [cell.lbTitle setText:[item objectForKey:@"title"]];
    [cell.lbDate setText:[[[item objectForKey:@"disp_date"] componentsSeparatedByString:@"T"] objectAtIndex:0]];
    //[cell.lbDate setFont:[UIFont systemFontOfSize:11.0f]];
    //[cell.lbDate setTextColor:[UIColor colorHex:@"2689eb"]];
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger index=indexPath.row;
    
    NSString *url=[[_arrData objectAtIndex:index]objectForKey:@"otherurl"];
    NSLog(@"%@",url);
    
    if(url &&[url isKindOfClass:[NSString class]] && ([url hasPrefix:@"http"]||[url hasPrefix:@"https"]||[url hasPrefix:@"edge"])){
        /*
         ref:http://blog.csdn.net/yhawaii/article/details/7442529
         NSNull
         */
        if([self loadWithRequest:url]){
            NewsWebViewController *vc=[[NewsWebViewController alloc]initWithNibName:@"NewsWebViewController" bundle:nil];
            [vc setURL:url];
            [self.navigationController pushViewController:vc animated:YES];
            vc=nil;
        }
    }else{
        NewsDetailController *newsDetail = [[NewsDetailController alloc]initWithNibName:@"NewsDetailController" bundle:nil];
        [newsDetail setNewsData:[_arrData objectAtIndex:index] atIndex:index withList:_arrData];
        [self.navigationController pushViewController:newsDetail animated:YES];
        newsDetail = nil;
    }
    
}




-(void)reloadData{
    [_newsTable reloadData];
}

-(void)setContent:(NSArray *)datas{
    _arrData=[datas copy];
}

@end
