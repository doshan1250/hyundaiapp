//
//  DataObject.m
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 POSINCO. All rights reserved.
//

#import "DataObject.h"

@implementation DataObject

- (id)initWithJsonString:(NSString *)data isArray:(BOOL)isArray
{
    self = [super init];
    
    if (self)
    {
        id retData = isArray ? [Utilities convertJSONStringToArray:data] : [Utilities convertJSONStringToDictionary:data];
        
        BOOL parseError = [retData isKindOfClass:[NSError class]];
        
        // Based
        self.res = parseError ? false : true;
        self.msg = parseError ? @"資料解析錯誤" : nil;
        self.data = parseError ? nil : retData;
        
        retData = nil;
    }
    
    return self;
}

- (void)dealloc
{
    self.msg = nil;
    self.data = nil;
}

@end
