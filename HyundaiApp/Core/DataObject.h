//
//  DataObject.h
//  QCar
//
//  Created by Webber Chuang on 2015/4/7.
//  Copyright (c) 2015年 POSINCO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataObject : NSObject

@property (nonatomic, assign) BOOL res;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) id data;

- (id)initWithJsonString:(NSString *)data isArray:(BOOL)isArray;

@end
