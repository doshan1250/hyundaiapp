//
//  VideoViewController.m
//  hyundaiapp
//
//  Created by RHZ Webber on 2015/9/25.
//  Copyright © 2015年 LinkinEDGE. All rights reserved.
//

#import "VideoViewController.h"
#import "ProgressViewController.h"

#import "VideoViewCell2.h"

#import "VideoDetailViewController.h"
#import "YoutubeViewController.h"

@interface VideoViewController () <ProgressDelegate, UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_data;
    ProgressViewController *_mProgressViewController;
}
@end

@implementation VideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HYUNDAI_BG_COLOR];
    _titleView.text = @"影音專區";
    
    if (IS_IOS7)
        [_videoTable setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    [_videoTable setBackgroundColor:[UIColor colorHex:@"#EDEDED"]];
    
    [self clickTaiwan:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    
}

#pragma mark -
- (void)timeout
{
    [self hideLoading];
}

#pragma mark - 
- (void)showLoading
{
    _mProgressViewController = [[ProgressViewController alloc]initWithNibName:@"ProgressViewController" bundle:nil];
    [_mProgressViewController setDelegate:self];
    _mProgressViewController.view.frame = currentApp.window.frame;
    [currentApp.window addSubview:_mProgressViewController.view];
}

- (void)hideLoading
{
    if ([_mProgressViewController.view superview])
    {
        [_mProgressViewController.view removeFromSuperview];
        [_mProgressViewController setDelegate:nil];
        _mProgressViewController = nil;
    }
}

- (IBAction)clickWorldwide:(id)sender
{
    [self showLoading];
    
    [_btnTaiwan setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
    [_btnWorldwide setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    
    
    NSString *srcUrl = [NSString stringWithFormat:API_HOST, @"getVideo.ashx?cmd=get_list&cate=worldwide&limit=100"];
    [currentApp.dataEngine getDataFromUrl:srcUrl params:nil isPost:NO onCompletion:^(NSString *response) {
        
        [self hideLoading];
        _data = [Utilities convertJSONStringToArray:response];
        
        [_videoTable reloadData];
        
    } onError:^(NSError *error) {
        
        NSLog(@"ERROR: clickWorldwide");
        [self hideLoading];
    }];
}

- (IBAction)clickTaiwan:(id)sender
{
    [self showLoading];
    
    [_btnTaiwan setBackgroundImage:[UIImage imageNamed:@"btn_select.png"] forState:UIControlStateNormal];
    [_btnWorldwide setBackgroundImage:[UIImage imageNamed:@"btn_unSelect.png"] forState:UIControlStateNormal];
    
    NSString *srcUrl = [NSString stringWithFormat:API_HOST, @"getVideo.ashx?cmd=get_list&cate=taiwan&limit=100"];
    [currentApp.dataEngine getDataFromUrl:srcUrl params:nil isPost:NO onCompletion:^(NSString *response) {
        
        [self hideLoading];
        _data = [Utilities convertJSONStringToArray:response];
        
        [_videoTable reloadData];
        
    } onError:^(NSError *error) {
        
        NSLog(@"ERROR: clickTaiwan");
        [self hideLoading];
        
    }];
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 251.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_data count];//ceil([[NSNumber numberWithDouble:[_data count] / 2.0f] doubleValue]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"VideoViewCell2";
    VideoViewCell2 *cell = (VideoViewCell2 *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VideoViewCell2" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (VideoViewCell2 *) currentObject;
                [cell setDelegate:(id<VideoViewCell2Delegate>)self];
                break;
            }
        }
    }
    
    // item 1
    NSDictionary *item1 = [_data objectAtIndex:indexPath.row];
    cell.tag1 = indexPath.row;
    
    [cell.title1 setText:[item1 objectForKey:@"title"]];
    NSString *date1 = [[[item1 objectForKey:@"disp_date"] componentsSeparatedByString:@"T"] objectAtIndex:0];
    [cell.date1 setText:[NSString stringWithFormat:@"發佈日期：%@", [date1 stringByReplacingOccurrencesOfString:@"-" withString:@"/"]]];
    [cell.pic1 loadImageFromURL:[NSURL URLWithString:[item1 objectForKey:@"thumb"]] savePath:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 
- (void)clickVideo:(NSInteger)index
{
    NSLog(@"%d", (int)index);
    
    VideoDetailViewController *vc = [[VideoDetailViewController alloc] initWithNibName:@"VideoDetailViewController" bundle:nil];
    [vc setNewsData:[_data objectAtIndex:index]];
    
    [self.navigationController pushViewController:vc animated:YES];
    vc = nil;
}

@end
