//
//  ListResultCell.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/15.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListResultCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *icon_type;
@property (strong, nonatomic) IBOutlet UILabel *label_dec;
@property (strong, nonatomic) IBOutlet UILabel *label_name;
@property (strong, nonatomic) IBOutlet UILabel *label_dis;

@end
