//
//  ServiceDetailController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ServiceDetailController.h"
#import "MapForGoogleAPIViewController.h"

#import "ServiceItem.h"
#import <MapKit/MapKit.h>

#define DUMMY_ITEM_TAG      -999
#define LINE_HIGHT          1.0

@interface ServiceDetailController () <MKMapViewDelegate,ServiceItemDelegate,UIAlertViewDelegate>
{
    IBOutlet MKMapView *_map;
    IBOutlet UIScrollView *_scroll;
    //地圖點擊後產生的資料，在資料和點之間建立一個連結的索引表
    NSMutableDictionary *mapDicts;
    NSMutableDictionary *mapDicts2;
    BOOL _isFactory;
    CGFloat lockView;
    CGFloat touchMoveY;
    NSString *_tel;
}
@property (strong, nonatomic) IBOutlet UIButton *btn_nav;
@property (strong, nonatomic) IBOutlet UIView *item1contentView;
@property (strong, nonatomic) IBOutlet UIImageView *scrollUpItem;

@end

@implementation ServiceDetailController

@synthesize arrData = _arrData;
@synthesize curLocation = _curLocation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lockView=-1;
    _titleView.text = @"服務據點";
    [_map setShowsUserLocation:YES];
    [_map setMapType:MKMapTypeStandard];
    [_map setZoomEnabled:YES];
    [_map setScrollEnabled:YES];
    [_map setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!mapDicts){
        mapDicts=[[NSMutableDictionary alloc]init];
        mapDicts2=[[NSMutableDictionary alloc]init];
        if(self.arrData.count==0){
            [self.btn_nav setEnabled:NO];
        }
        CGFloat total_hight=0.0;
        //排序
        self.arrData=[self.arrData sortedArrayUsingComparator:^NSComparisonResult(NSMutableDictionary *item0, NSMutableDictionary *item1){
            double lat0=[[item0 objectForKey:@"lat"] doubleValue];
            double lng0=[[item0 objectForKey:@"lng"] doubleValue];
            double lat1=[[item1 objectForKey:@"lat"] doubleValue];
            double lng1=[[item1 objectForKey:@"lng"] doubleValue];
            int dis1=[[self getDistance:lat1 mylng1:lng1 mylat2:self.curLocation.coordinate.latitude mylng2:self.curLocation.coordinate.longitude]intValue];
            int dis0=[[self getDistance:lat0 mylng1:lng0 mylat2:self.curLocation.coordinate.latitude mylng2:self.curLocation.coordinate.longitude]intValue];
            
            return [[NSNumber numberWithInt:dis0] compare:[NSNumber numberWithInt:dis1]];
        }];
        for (int i=0; i<[self.arrData count]; i=i+1){
            ServiceItem *item = [[[NSBundle mainBundle] loadNibNamed:@"ServiceItem" owner:self options:nil] lastObject];
            //橫的條列方式
            [item setFrame:CGRectMake(0, total_hight, item.frame.size.width, item.frame.size.height)];
            [_scroll addSubview:item];
            total_hight=total_hight+item.frame.size.height+LINE_HIGHT;
            if(_isFactory){
                [item setIconImage:[UIImage imageNamed:@"service_detail_icon.png"]];
            }else{
                //展示中心要換圖
                [item setIconImage:[UIImage imageNamed:@"service_detail_icon2.png"]];
            }
            
            [item setDelegate:self];
            NSMutableDictionary *item1 = [[self.arrData objectAtIndex:i] mutableCopy];
            double lat=[[item1 objectForKey:@"lat"] doubleValue];
            double lng=[[item1 objectForKey:@"lng"] doubleValue];
            NSString *dis=[self getDistance:lat mylng1:lng mylat2:self.curLocation.coordinate.latitude mylng2:self.curLocation.coordinate.longitude];
            if(i==0){
                
                MKCoordinateRegion theRegion;
                //set region center
                CLLocationCoordinate2D theCenter;
                theCenter.latitude = lat;
                theCenter.longitude = lng;
                theRegion.center = theCenter;
                //set zoom level
                MKCoordinateSpan theSpan;
                theSpan.latitudeDelta = 0.025;
                theSpan.longitudeDelta = 0.025;
                theRegion.span = theSpan;
                //set map Region
                [_map setRegion:theRegion];
                [_map regionThatFits:theRegion];
                
                if(!self.curLocation){
                    self.curLocation =[[CLLocation alloc]initWithLatitude:lat longitude:lng];
                }
            }
            
            NSString *mString=[item1 objectForKey:@"name"];
            if(!mString||[mString isEqualToString:@""]){
                mString=@"無資料";
            }
            mString=[mString stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
            
            NSString *mString2=[item1 objectForKey:@"address"];
            if(!mString2||[mString2 isEqualToString:@""]){
                mString2=@"無資料";
            }
            mString2=[mString2 stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
            mString2=[NSString stringWithFormat:@"%@\n(%@, %@)",mString2,[item1 objectForKey:@"lat"],[item1 objectForKey:@"lng"]];
            PlacePin *placePin=[self putPoint:mString andSubTitle:[NSString stringWithFormat:@"電話: %@",[item1 objectForKey:@"tel"]] andLat:lat andLng:lng];
            [item setTitle:mString andSubTitle:mString2 andDis:dis  tag:i];
            [mapDicts setObject:[item1 copy] forKey:mString];
            [mapDicts2 setObject:placePin forKey:mString];
            item1 = nil;
            item.tag=(i+1)*100;//設定標示用途
            item=nil;
            if(i<1){//添加幾個代表的圖案to 主畫面
                ServiceItem *item2=[[[NSBundle mainBundle] loadNibNamed:@"ServiceItem" owner:self options:nil] lastObject];
                [item2 setFrame:CGRectMake(0,self.scrollUpItem.frame.origin.y+self.scrollUpItem.frame.size.height+i*item2.frame.size.height, item2.frame.size.width, item2.frame.size.height)];
                [item2 setDelegate:self];//讓他還是可以點擊
                [item2 setTitle:mString andSubTitle:mString2 andDis:dis  tag:DUMMY_ITEM_TAG];
                [item2 setTag:DUMMY_ITEM_TAG];
                [self.item1contentView addSubview:item2];
                
                [item2 setBackgroundColor:[[UIColor alloc]initWithRed:1.0 green:1.0 blue:1.0 alpha:0.7 ]];
                if(_isFactory){
                    [item2 setIconImage:[UIImage imageNamed:@"service_detail_icon.png"]];
                }else{
                    //展示中心要換圖
                    [item2 setIconImage:[UIImage imageNamed:@"service_detail_icon2.png"]];
                }
                item2=nil;
            }
            
        }//end for
        CGRect frame=self.item1contentView.frame;
        [self.item1contentView setFrame:CGRectMake(frame.origin.x, frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
        [_scroll setContentSize:CGSizeMake(_scroll.frame.size.width, total_hight)];
        [_scroll setHidden:YES];
        [_scroll setBounces:NO];//讓他沒有橡皮筋效果
        [_map selectAnnotation:[mapDicts2 objectForKey:[[self.arrData objectAtIndex:0] objectForKey:@"name"]] animated:YES];
    }
}//end function

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(BOOL)clickedHomeEvent{
    _tel=nil;
    _scroll=nil;
    _map=nil;
    self.arrData = nil;
    self.curLocation=nil;
    mapDicts=nil;
    mapDicts2=nil;
    self.btn_nav=nil;
    self.item1contentView=nil;
    self.scrollUpItem=nil;
    return NO;
}

- (void)dealloc
{
}

-(void)setIsFactory:(BOOL)isFactory{
    _isFactory=isFactory;
}

#pragma mark - Private API
- (NSString *)getDistance:(double)lat1 mylng1:(double)lng1 mylat2:(double)lat2 mylng2:(double)lng2
{
	float ran_lat1 = lat1 * 3.1415926 / 180;
	float ran_lat2 = lat2 * 3.1415926 / 180;
	float ran_lng1 = lng1 * 3.1415926 / 180;
	float ran_lng2 = lng2 * 3.1415926 / 180;
	
	float a = ran_lat1 - ran_lat2;
	float b = ran_lng1 - ran_lng2;
	
	float s = 2 * asin(sqrt(pow(sin(a/2), 2) + cos(ran_lat1) * cos(ran_lat2) * pow(sin(b/2), 2)));
	s = s * 6378.137;
	s = round(s * 10000) / 10;
	
    NSString *retStr = (s > 999000) ? @"0.00": [NSString stringWithFormat:@"%.2f", (float)(s/1000.0)] ;
	return retStr;
}



#pragma mark - ServiceItemDelegate 點擊畫面
- (void)clickItemWithIndex:(int)index{
    
    if(index<0){
        [_scroll setHidden:NO];
        [self animationForScrollViewShow];
        return;
    }
    [self animationForScrollViewHide];
    [self.btn_nav setHidden:NO];
    //-----
    [self itemClickEvent:index];
    //-----
    
    [_map setShowsUserLocation:YES];
    [_map setMapType:MKMapTypeStandard];
    [_map setZoomEnabled:YES];
    [_map setScrollEnabled:YES];
    
    MKCoordinateRegion theRegion;
    NSDictionary *item = [self.arrData objectAtIndex:index];
    //set region center
    CLLocationCoordinate2D theCenter;
    theCenter.latitude = [[item objectForKey:@"lat"] doubleValue];
    theCenter.longitude = [[item objectForKey:@"lng"] doubleValue];
    theRegion.center = theCenter;
    
    //set zoom level
    MKCoordinateSpan theSpan;
    theSpan.latitudeDelta = 0.025;
    theSpan.longitudeDelta = 0.025;
    theRegion.span = theSpan;
    
    //set map Region
    [_map setRegion:theRegion];
    [_map regionThatFits:theRegion];
    
    [_map selectAnnotation:[mapDicts2 objectForKey:[item objectForKey:@"name"]] animated:YES];
    item=nil;
    
}

/**底下的item點擊事件*/
-(void)itemClickEvent:(int)index{
    self.btn_nav.tag=index*1000;
    NSDictionary *item = [self.arrData objectAtIndex:index];
    ServiceItem *i_view=(ServiceItem *)[_scroll viewWithTag:((index+1)*100)];
    if(!i_view){
        NSLog(@"!i_view");
        return;
    }
    NSArray *array=[_scroll subviews];
    CGFloat total_hight=i_view.frame.size.height+LINE_HIGHT;//從第二個排起
    for(int n=0;n<array.count;n++){
        ServiceItem *mServiceItem=(ServiceItem *)array[n];
        if(mServiceItem!=i_view){
            [mServiceItem removeFromSuperview];
            [mServiceItem setFrame:CGRectMake(0, total_hight, mServiceItem.frame.size.width, mServiceItem.frame.size.height)];
            [_scroll addSubview:mServiceItem];
            
            total_hight=total_hight+mServiceItem.frame.size.height+LINE_HIGHT;
        }
    }//end for
    [i_view setFrame:CGRectMake(0, 0, i_view.frame.size.width, i_view.frame.size.height)];
    
    //更新地圖下面的資料
    NSString *mString=[item objectForKey:@"name"];
    if(!mString||[mString isEqualToString:@""]){
        mString=@"無資料";
    }
    mString=[mString stringByTrimmingCharactersInSet:
             [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *mString2=[item objectForKey:@"address"];
    if(!mString2||[mString2 isEqualToString:@""]){
        mString2=@"無資料";
    }
    mString2=[mString2 stringByTrimmingCharactersInSet:
              [NSCharacterSet whitespaceCharacterSet]];
    mString2=[NSString stringWithFormat:@"%@\n(%@, %@)",mString2,[item objectForKey:@"lat"],[item objectForKey:@"lng"]];
    double lat=[[item objectForKey:@"lat"] doubleValue];
    double lng=[[item objectForKey:@"lng"] doubleValue];
    
    ServiceItem *dummyItemView=(ServiceItem *)[self.item1contentView viewWithTag:DUMMY_ITEM_TAG];
    [dummyItemView setTitle:mString andSubTitle:mString2 andDis:[self getDistance:lat mylng1:lng mylat2:self.curLocation.coordinate.latitude mylng2:self.curLocation.coordinate.longitude]  tag:DUMMY_ITEM_TAG];
    i_view=nil;
    dummyItemView=nil;
    array=nil;
    item=nil;
}//end click event

#pragma mark - 地圖的相關操作
-(PlacePin *)putPoint:(NSString *)title andSubTitle:(NSString *)subTitle andLat:(CLLocationDegrees )latitude andLng:(CLLocationDegrees)longitude{
    //放置點
    CLLocationCoordinate2D pinCenter;
    
    pinCenter.latitude = latitude;
    pinCenter.longitude = longitude;
    
    PlacePin *annotation = [[PlacePin alloc]initWithCoordinate:pinCenter];
    
    annotation.title =title;
    annotation.subtitle = subTitle;
    [_map addAnnotation:annotation];
    
    return annotation;
}

#pragma mark methods for MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)map
            viewForAnnotation:(id <MKAnnotation>)annotation{
    
    static NSString *AnnotationViewID = @"annotationViewID";
    static NSString *currentloc=@"currentloc";
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:currentloc];
        if(!pinView){
            MKAnnotationView* customPinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:currentloc];
            [customPinView setImage:[UIImage imageNamed:@"icon_my_location.png"]];
            customPinView.canShowCallout = NO;
            return customPinView;
        }else{
            pinView.annotation = annotation;
        }
        return pinView;
    }
    
    MKAnnotationView* pinView =
    (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (!pinView&&![annotation isKindOfClass:[MKUserLocation class]])
    {
        //這裡針對大頭針做各種設定
        MKAnnotationView* customPinView =
        [[MKAnnotationView alloc] initWithAnnotation:annotation
                                     reuseIdentifier:AnnotationViewID];
        //顯示視窗
        customPinView.canShowCallout = YES;
        
        if(_isFactory){
            customPinView.image=[UIImage imageNamed:@"map_icon1.png"];
        }else{
            //展示中心要換圖
            customPinView.image=[UIImage imageNamed:@"map_icon2.png"];
        }
        
        //右邊的info內容
        UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [rightBtn setFrame:CGRectMake(0, 0, 30, 30)];
        [rightBtn setImage:[UIImage imageNamed:@"icon_tel.png"] forState:UIControlStateNormal];
        customPinView.rightCalloutAccessoryView = rightBtn;
        
        return customPinView;
    }
    else
    {
        pinView.annotation = annotation;
    }
    return pinView;
}

#pragma mark - AnnotationView's UIControl 被點擊後的動作反應
-(void)mapView:(MKMapView *)mMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSDictionary *dict=[mapDicts objectForKey:[[view annotation] title]];
    NSString *telString=[NSString stringWithFormat:@"tel://%@",[dict objectForKey:@"tel"]];
    telString=[telString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    _tel=telString;
    [[[UIAlertView alloc] initWithTitle:nil message:@"確定撥出電話" delegate:self cancelButtonTitle:@"不要" otherButtonTitles:@"好", nil] show];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    for(int n=0;n<self.arrData.count;n++){
        NSDictionary *item = [self.arrData objectAtIndex:n];
        NSString *mString=[item objectForKey:@"name"];
        item=nil;
        if([mString isEqualToString:[[view annotation] title]]){
            [self itemClickEvent:n];
            break;
        }
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_tel]];
        NSLog(@"tel: %@",_tel);
    }
}

#pragma mark - UITouch
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if (touch.view == self.item1contentView)
    {
        NSLog(@"touchesBegan");
        CGPoint location = [touch locationInView:self.view];
        
        lockView=location.y-self.item1contentView.frame.origin.y;
        touchMoveY=self.item1contentView.frame.origin.y;
        
        [_scroll setFrame:CGRectMake(0, touchMoveY+self.scrollUpItem.frame.size.height+self.scrollUpItem.frame.origin.y, _scroll.frame.size.width, _scroll.frame.size.height)];
        [_scroll setContentOffset:CGPointMake(0, 0)];
        [_scroll setHidden:NO];
        [self.btn_nav setHidden:YES];
        
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if(lockView>-1){
        NSLog(@"touchesMoved");
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:self.view];
        touchMoveY=location.y;
        [self.item1contentView setFrame:CGRectMake(0, touchMoveY-lockView,self.item1contentView.frame.size.width , self.item1contentView.frame.size.height)];
        [_scroll setFrame:CGRectMake(0, touchMoveY+self.scrollUpItem.frame.size.height+self.scrollUpItem.frame.origin.y-lockView, _scroll.frame.size.width, _scroll.frame.size.height)];
    }//end if
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if (touch.view == self.item1contentView)
    {
        if(touchMoveY<(self.view.frame.size.height/2)){
            //當位置在上方2分之一時候
            [_scroll setHidden:NO];
            [self animationForScrollViewShow];
        }else{
            [self animationForScrollViewHide];
        }//end if else
        touchMoveY=-1;
        lockView=-1;
    }
}

-(void)animationForScrollViewHide{
    CGRect frame=self.item1contentView.frame;
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.item1contentView setFrame:CGRectMake(0, self.view.frame.size.height-110,frame.size.width , frame.size.height)];
                         [_scroll setFrame:CGRectMake(0, self.view.frame.size.height+self.scrollUpItem.frame.size.height+self.scrollUpItem.frame.origin.y-110, _scroll.frame.size.width, _scroll.frame.size.height)];
                     } completion:^(BOOL finish){
                         [_scroll setHidden:YES];
                         [self.btn_nav setHidden:NO];
                     }];
    
    [UIView commitAnimations];
}

-(void)animationForScrollViewShow{
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [_scroll setFrame:CGRectMake(0, self.scrollUpItem.frame.size.height+self.scrollUpItem.frame.origin.y, _scroll.frame.size.width, _scroll.frame.size.height)];
                         [self.item1contentView setFrame:CGRectMake(0, 0,self.item1contentView.frame.size.width , self.item1contentView.frame.size.height)];
                     } completion:^(BOOL finish){
                         [self.btn_nav setHidden:YES];
                         [_scroll setHidden:NO];
                     }];
    
    [UIView commitAnimations];
}

- (IBAction)navWithmap:(UIButton *)sender {
    NSDictionary *item = [self.arrData objectAtIndex:(self.btn_nav.tag/1000)];
    double lat=[[item objectForKey:@"lat"] doubleValue];
    double lng=[[item objectForKey:@"lng"] doubleValue];
    
    if(!self.curLocation||self.curLocation.coordinate.longitude==0.00){
        UIAlertView *alertView;
        alertView=[[UIAlertView alloc]initWithTitle:nil message:@"目前尚無位置資訊，請稍候再嘗試" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    MapForGoogleAPIViewController *vc=[[MapForGoogleAPIViewController alloc]initWithNibName:@"MapForGoogleAPIViewController" bundle:nil];
    if(_isFactory)
        [vc setType:2];
    else
        [vc setType:3];
    [vc setOrigin:self.curLocation.coordinate andDestination:CLLocationCoordinate2DMake(lat, lng)];
    
    [self.navigationController pushViewController:vc animated:YES];
    vc=nil;
}

@end
