//
//  AppDelegate.m
//  Ma2Club
//
//  Created by Webber Chuang on 13/11/7.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "AppDelegate.h"

#import "PKRevealController.h"
#import "IndexViewController.h"
#import "SidebarViewController.h"
#import "NewsDetailController.h"
#import "DataManager.h"
#import "MaintenanceController.h"


@interface AppDelegate() <PKRevealing,UIAlertViewDelegate>
{
    IndexViewController *_indexVC;
    
    NSString *_deviceToken;
}
@end

AppDelegate *currentApp = nil;

@implementation AppDelegate

@synthesize reveal = _reveal;
@synthesize nav = _nav;
@synthesize dataMgr = _dataMgr;
@synthesize areaDataArray=_areaDataArray;
@synthesize detaildataFromAPNs=_detaildataFromAPNs;
@synthesize newsArray=_newsArray;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    currentApp = self;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    self.dataMgr = [[DataManager alloc] init];
    self.dataEngine = [[DataEngine alloc] initWithHostName:@"http://app.hyundai-motor.tw"];
    [UIImageView setDefaultEngine:self.dataEngine];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Step 1: Create your controllers.
    _indexVC = [[IndexViewController alloc] initWithNibName:@"IndexViewController" bundle:nil];
    
    
    self.nav = [[UINavigationController alloc] initWithRootViewController:_indexVC];
    [self.nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav.png"] forBarMetrics:UIBarMetricsDefault];
    self.nav.navigationBar.translucent = NO;
    
    // Step 2: Instantiate.
    self.reveal = [PKRevealController revealControllerWithFrontViewController:self.nav leftViewController:nil rightViewController:nil];
    
    // Step 3: Configure.
    self.reveal.delegate = self;
    self.reveal.animationDuration = 0.25;
    
    // Step 4: Apply.
    self.window.rootViewController = self.reveal;
    
    [self.window makeKeyAndVisible];
    [self checkNetworkAlive];
    
    // Facebook Init
    self.fbLoginMgr = [[FBSDKLoginManager alloc] init];
    
    // Intro
    NSString *isFirst = [Utilities userDefaultGetDataByKey:@"ShowIntro"];
    
    if (isFirst == nil)
    {
        self.introView = [[UIScrollView alloc] initWithFrame:self.window.bounds];
        [self.introView setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.8]];
        [self.introView setContentSize:CGSizeMake(640, self.window.frame.size.height-64)];
        [self.introView setPagingEnabled:YES];
        [self.window addSubview:self.introView];
        
        self.introImg1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.introView.bounds.size.width, self.introView.bounds.size.height)];
        [self.introImg1 setImage:[UIImage imageNamed:@"intro_01.png"]];
        [self.introImg1 setContentMode:UIViewContentModeScaleAspectFit];
        [self.introView addSubview:self.introImg1];
        
        self.introImg2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.introView.bounds.size.width, 0, self.introView.bounds.size.width, self.introView.bounds.size.height)];
        [self.introImg2 setImage:[UIImage imageNamed:@"intro_02.png"]];
        [self.introImg2 setContentMode:UIViewContentModeScaleAspectFit];
        [self.introView addSubview:self.introImg2];
        
        self.introBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.introBtn setImage:[UIImage imageNamed:@"intro_btn.png"] forState:UIControlStateNormal];
        [self.introBtn setFrame:CGRectMake(410, 380, 140, 37)];
        [self.introBtn addTarget:self action:@selector(closeIntro) forControlEvents:UIControlEventTouchUpInside];
        [self.introView addSubview:self.introBtn];
    }

    
    // Register Push Notification Jerry Edit 2015-06-20
    //獲取推播 Let the device know we want to receive push notifications
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    
    // apn 内容
    NSDictionary *remoteNotification = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    //處理未啟動時透過推播進入App產生的行為
    [self didReceiveRemoteNotificationWithDidFinishLaunching:remoteNotification];
    
    
    //-------測試-------
    //NSLog(@"1111111remoteNotification: %@",remoteNotification);

    //[self doAPNsMsg:nil withIsActive:NO];
    
    //-------END 測試------
    
    return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL wasHandled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    
    return wasHandled;
}


- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame
{
}

- (void)closeIntro
{
    [Utilities userDefaultSaveData:@"1" forKey:@"ShowIntro"];

    if ([self.introView superview])
        [self.introView removeFromSuperview];
}

//---------

- (void)exitApplication {
    [UIView beginAnimations:@"exitApplication" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.window cache:NO];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    self.window.bounds = CGRectMake(0, 0, 0, 0);
    [UIView commitAnimations];
}

- (void)animationFinished:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([animationID compare:@"exitApplication"] == 0) {
        exit(0);
    }
}

- (void)checkNetworkAlive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self.dataEngine getDataFromUrl:@"http://www.hyundai-motor.com.tw" params:nil isPost:NO onCompletion:^(NSString *response) {
        
        // 網路通！不做事～
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
    } onError:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"網路連線錯誤"
                                                            message:@"請確認您的網路已連線"
                                                           delegate:self
                                                  cancelButtonTitle:@"好，我知道了"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
    }];
}

// -----
// alertView:alertView clickedButtonAtIndex:buttonIndex
// -----
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
            [self exitApplication ];
			break;
		default:
			break;
	}
}

//---------

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self checkNetworkAlive];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)dealloc
{
    if (self.dataMgr)
        self.dataMgr  = nil;
    if (self.reveal)
        self.reveal = nil;
    if (_indexVC)
        _indexVC = nil;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken: (NSData *)deviceToken
{
    /* Get device token */
    NSString *strDevToken = [NSString stringWithFormat:@"%@", deviceToken];
    
    /* Replace '<', '>' and ' ' */
    NSCharacterSet *charDummy = [NSCharacterSet characterSetWithCharactersInString:@"<> "];
    strDevToken = [[strDevToken componentsSeparatedByCharactersInSet: charDummy] componentsJoinedByString: @""];
    
    _deviceToken = [[NSString alloc] initWithString:strDevToken];
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken App: DeviceToken:[%@]", strDevToken);
    
    //將Device Token傳給Provider...
    [self.dataMgr addObserverData:self callback:@"onDataReady:"];
    [self.dataMgr fireRequest:Reuqest_RegToken postData:[NSMutableDictionary dictionaryWithObjectsAndKeys: strDevToken, @"t", @"iOS", @"d", nil]];
}

#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    if ([[notify objectForKey:API_NotifyKey] intValue] == Reuqest_RegToken){
        //註冊完token接收回傳值
        NSLog(@"註冊訊息: %@",notify);
        [self.dataMgr delObserverData:self];
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError: (NSError *)err {
    //錯誤處理...取消註冊
    [self.dataMgr delObserverData:self];
    NSLog(@"err: %@",err);
}

#pragma 如果 App 状态为未运行，此函数将被调用，如果launchOptions包含UIApplicationLaunchOptionsLocalNotificationKey表示用户点击apn 通知导致app被启动运行；如果不含有对应键值则表示 App 不是因点击apn而被启动，可能为直接点击icon被启动或其他。
-(void)didReceiveRemoteNotificationWithDidFinishLaunching:(NSDictionary *)remoteNotification{
    if(remoteNotification!=nil){
        NSLog(@"remoteNotification: %@",remoteNotification);
        
        NSString *anpsID= [remoteNotification objectForKey:@"apsID"];
        [self doAPNsMsg:anpsID withIsActive:NO];
    }
}

#pragma 如果 App状态为正在前台或者后台运行，那么此函数将被调用，并且可通过AppDelegate的applicationState是否为UIApplicationStateActive判断程序是否在前台运行。此种情况在此函数中处理
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    // apn内容为userInfo
    
//    NSDictionary *apns=[userInfo objectForKey:@"aps"];
    NSString *anpsID= [userInfo objectForKey:@"apsID"];
    NSLog(@"apsID: %@",[userInfo objectForKey:@"apsID"]);
    [self doAPNsMsg:anpsID withIsActive:([application applicationState]==UIApplicationStateActive)];
}

- (NSString *)getDeviceToken
{
    return _deviceToken;
}

#pragma 操作接收到通知的時候內容
-(void)doAPNsMsg:(NSString *)dataID withIsActive:(BOOL) isActive{
    NSInteger apnsCount=[[UIApplication sharedApplication] applicationIconBadgeNumber];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:(apnsCount+1)];
    
    
    if(isActive){
        self.detaildataFromAPNs=nil;
    }else{
        
        if ([dataID intValue] < 0)
        {
            MaintenanceController *vc = [[MaintenanceController alloc] initWithNibName:@"MaintenanceController" bundle:nil];
            [self.nav pushViewController:vc animated:NO];
            vc = nil;
        }
        else
        {
            self.detaildataFromAPNs=dataID;
            //NSLog(@"a: %@",self.newsArray);
            //NSLog(@"b: %@",self.detaildataFromAPNs);
            //處理當從推播訊息來的時候，應該要直接跳轉到某個指定頁面
            if(self.detaildataFromAPNs && self.newsArray){
                NewsDetailController *newsDetail = [[NewsDetailController alloc]initWithNibName:@"NewsDetailController" bundle:nil];
                NSDictionary *data=nil;
                
                for(NSDictionary *obj in self.newsArray){
                    if([self.detaildataFromAPNs isEqualToString:[NSString stringWithFormat:@"%@",[obj objectForKey:@"sno"]]]){
                        data=[obj copy];
                        break;
                    }
                }
                
                [newsDetail setNewsData:data atIndex:-1 withList:nil];
                [self.nav pushViewController:newsDetail animated:NO];
                newsDetail = nil;
                currentApp.detaildataFromAPNs=nil;
            }
        }
    }
}//end function

#pragma mark - 獲取和設定訊息的preference
+(NSMutableDictionary *)getAlreadyReadNews{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data=[userDefaults objectForKey:@"news"];
    NSMutableDictionary *dict;
    if(data){
        dict=[NSKeyedUnarchiver unarchiveObjectWithData:data];
    }else{
        dict=[NSMutableDictionary new];
    }
    return dict;
}

+(void)setAlreadyReadNews:(NSMutableDictionary *)alreadyReadNews{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:alreadyReadNews];
    [userDefaults setObject:data forKey:@"news"];
}



@end
