//
//  Reservation2View.h
//  HyundaiApp
//
//  Created by FDT14009Mac on 2016/4/23.
//  Copyright © 2016年 Hyundai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reservation2View : UIView

@property (weak, nonatomic) IBOutlet UILabel *reverationPlaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservationTimeLabel;

@end
