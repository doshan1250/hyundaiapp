//
//  LSAreaOptionCell.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/15.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSOptionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *areaName;
@property (weak, nonatomic) IBOutlet UILabel *bookingAmountTitle;
@property (weak, nonatomic) IBOutlet UILabel *bookingAmountValue;

@end
