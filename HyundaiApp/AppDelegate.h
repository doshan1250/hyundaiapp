//
//  AppDelegate.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/9.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKRevealController.h"
#import "DataManager.h"
#import "DataEngine.h"

#define FB_APP_ID           @"165674503774681"
#define FB_SECRET_KEY       @"fe8f773452bd22b03817ab6262a6bdc2"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

+(NSMutableDictionary *)getAlreadyReadNews;
+(void)setAlreadyReadNews:(NSMutableDictionary *)alreadyReadNews;
- (NSString *)getDeviceToken;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *nav;
@property (nonatomic, strong) PKRevealController *reveal;
@property (nonatomic, strong) NSArray *areaDataArray;
@property (nonatomic, strong) DataManager *dataMgr;
@property (nonatomic, strong)NSDictionary *fids;//服務場代碼
@property (nonatomic, strong)NSArray *newsArray;
@property (nonatomic, strong)NSString *detaildataFromAPNs;;

@property (strong, nonatomic) DataEngine *dataEngine;
@property (strong, nonatomic) MKNetworkOperation *dataOperation;

@property (strong, nonatomic) UIScrollView *introView;
@property (strong, nonatomic) UIImageView *introImg1;
@property (strong, nonatomic) UIImageView *introImg2;
@property (strong, nonatomic) UIButton *introBtn;

@property (nonatomic, strong) FBSDKLoginManager *fbLoginMgr;
@property (nonatomic, strong) NSDictionary *fbShareData;

@end


// Extern variable
extern AppDelegate *currentApp;
