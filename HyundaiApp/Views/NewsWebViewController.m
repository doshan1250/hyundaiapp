//
//  NewsWebViewController.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/11/29.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "NewsWebViewController.h"

@interface NewsWebViewController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *_ciWeb;
    NSString *_urlString;
    NSString *_title;
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progress;
@end

@implementation NewsWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(_title != nil && [_title length] > 0){
            _titleView.text = _title;
    }else{
    _titleView.text = @"線上活動";
    }
    
    [_ciWeb setDelegate:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.progress startAnimating];
    [_ciWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
}




#pragma mark - webviewdeleatge
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //覆寫鏈結處理
    NSString *target = [[request URL] absoluteString];
    
    return [self loadWithRequest:target];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Disable 長按(右鍵)功能, WebView Default 會顯示 copy/paste 等預設功能
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    //source: http://blog.techno-barje.fr/post/2010/10/04/UIWebView-secrets-part1-memory-leaks-on-xmlhttprequest/
	[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [self.progress stopAnimating];
    [self.progress removeFromSuperview];
}



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

-(void)setTitle:(NSString *)titleString{
    _title = titleString;
}

-(void)setURL:(NSString *)urlString{
    _urlString = urlString;
}


@end
