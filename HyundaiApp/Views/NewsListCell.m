//
//  NewsListCell.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "NewsListCell.h"

#define DARK_BLUE   @"2689eb"

@implementation NewsListCell

@synthesize lbTitle = _lbTitle;
@synthesize lbDate = _lbDate;
@synthesize alreadyReadIcon=_alreadyReadIcon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)hideAlreadyReadIcon{
    [self.alreadyReadIcon setAlpha:1];
    [self.lbTitle setTextColor:[UIColor colorHex:DARK_BLUE]];
    [self.lbDate setTextColor:[UIColor colorHex:DARK_BLUE]];
}

-(void)showAlreadyReadIcon{
    [self.alreadyReadIcon setAlpha:0];
    [self.lbTitle setTextColor:[UIColor blackColor]];
    [self.lbDate setTextColor:[UIColor colorHex:DARK_BLUE]];
}


@end
