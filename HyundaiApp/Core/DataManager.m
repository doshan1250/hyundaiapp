//
//  DataManager.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "DataManager.h"

#import "ICommunication.h"
#import "CommunicationManager.h"
#import "CommunicationContext.h"
#import "ParserJSON.h"

@interface DataManager () <ICommunication>
{
    CommunicationManager *_connMgr;
    NSMutableDictionary *_requestPool;
    NSMutableDictionary *_requestCache;
}

@end

@implementation DataManager

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _connMgr = [[CommunicationManager alloc] init];
        _requestPool = [[NSMutableDictionary alloc] init];
        _requestCache = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    _connMgr = nil;
}

#pragma mark - Add/Remove Observer

- (void)addObserverData:(NSObject *)obj callback:(NSString *)func
{
    [[NSNotificationCenter defaultCenter] addObserver:obj selector:NSSelectorFromString(func) name:API_NotifyName object:nil];
}

- (void)delObserverData:(NSObject *)obj
{
    [[NSNotificationCenter defaultCenter] removeObserver:obj name:API_NotifyName object:nil];
}

#pragma mark - ICommunication
- (void)fireRequest:(EnumRequestAPI)action postData:(NSMutableDictionary *)data
{   
    NSString *api = @"";
    
    switch (action)
    {
        case Request_News:
            api = API_News;
            break;
            
        case Request_NewsDetail:
            api = API_News;
            break;
            
        case Request_GasStation:
            api = API_GasStation;
            break;
            
        case Request_GasPrice:
            api = API_GasPrice;
            break;
            
        case Request_ParkingArea:
            api = API_ParkingArea;
            break;
        
            
        case Request_ParkingData:
            api = API_ParkingData;
            break;
            
        case Request_ParkingDataMap:
            api = API_ParkingData;
            break;
            
        case Request_CarInfo:
            api = API_CarInfo;
            break;
            
        case Request_ServiceData:
            api = API_ServiceData;
            break;
            
        case Request_GasData:
            api=API_GasData;
            break;
            
        case Request_UserLogin:
            api=API_UserLogin;
            break;
            
        case Request_CarFix:
            api=API_CarFix;
            break;
            
        case Request_Reservation:
            api=API_Reservation;
            break;
            
        case Request_GetFixPlace:
            api=API_GetFixPlace;
            break;
        
        case Request_bookingInfo:
            api=API_BookingInfo;
            break;
        case Request_getfans:
            api=API_GetFans;
            break;
        case Reuqest_RegToken:
            api=API_RegToken;
            break;
        case Reuqest_GetNowBookingCarInfo:
            api= API_GetNowBookingCarInfo;
            break;
        case Request_GetBookingBalanceInfo:
            api= API_GetBookingBalanceInfo;
            break;
        case Request_BookingCarService:
            api= API_BookingCarService;
            break;
        default:
            break;
    }
    
    // load cache
    if ([self isCacheable:action] && [[_requestCache allKeys] containsObject:[NSString stringWithFormat:API_HOST, api]])
    {
        NSArray *arrData = [_requestCache objectForKey:[NSString stringWithFormat:API_HOST, api]];
        NSDate *date = [[NSDate alloc] init];
        
        if (([date timeIntervalSince1970] - [[arrData objectAtIndex:0] integerValue]) < 60)
        {
            //NSLog(@"直接回傳 Cache 資料");
            [self notifyData:[arrData objectAtIndex:1]];
            
            return;
        }
        else
        {
            [_requestCache removeObjectForKey:[NSString stringWithFormat:API_HOST, api]];
        }
    }
    
    CommunicationContext *pCtx = [[CommunicationContext alloc] init];
    
    pCtx.action = action;
    pCtx.delegate = (id<ICommunication>)self;
    
    if ([api.lowercaseString containsString:@"http"]) {
        pCtx.url = api;
    }
    else{
        pCtx.url = [NSString stringWithFormat:API_HOST, api];
    }
    
    
    pCtx.postData = data;
    
    ASIHTTPRequest *request = [_connMgr doRequest:pCtx];
    
    if (request != nil)
        [_requestPool setObject:request forKey:pCtx.url];
}

- (void)handleCommunicationResult:(CommunicationContext *)pCtx
{
    NSLog(@"url: %@",pCtx.url);
    [_requestPool removeObjectForKey:pCtx.url];
    
    BOOL bError = YES;
    ParserJSON *parser = nil;
    
    
	do {
        // 如果 Request 階段發生網路錯誤, [-1]網路不通 [200+]以上 Web StatusCode
        if (pCtx.errorCode != 0)
        {
            //NSLog(@"Network 錯誤");
            break;
		}

        
        if (pCtx.retData.length > 0) {
            //---------檢查特殊符號置換---------
            NSString *retData=[NSString stringWithUTF8String:[pCtx.retData bytes]];
            //        NSLog(@"retData1: %@",retData);
            NSString* retDataN=[retData stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
            if(![retDataN isEqualToString:retData]){
                //            NSLog(@"retData2: %@",retDataN);
                pCtx.retData = [retDataN dataUsingEncoding:NSUTF8StringEncoding];
            }
            //---------END 檢查特殊符號置換---------
        }

        
        
        @try {
            parser = [[ParserJSON alloc] initWithData:pCtx.retData];
            
            // 檢查 JSON Parser 是否正確解析
            if (parser.errParser)
            {
                //NSLog(@"ParserJSON 錯誤");
                break;
            }
        }@catch (NSException * exception) {
            //NSLog(@"exception: %@",exception);
            [self processCommunicationResult:pCtx data:nil];
            break;
        }
       
        
        bError = NO;
//        NSLog(@"%@",parser.dictRes);
        
        //NSLog(@"下載資料成功");
        [self processCommunicationResult:pCtx data:parser];
        
	} while (false);
    
    if (bError)
    {
        // TODO:
    }
	
	if (parser)
		parser = nil;
    
    pCtx = nil;
}

- (void)processCommunicationResult:(CommunicationContext *)pCtx data:(ParserJSON *)parser
{
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    // notify data
	[dict setValue:[NSNumber numberWithInt:pCtx.action] forKey:API_NotifyKey];
	[dict setValue:parser.dictRes forKey:API_NotifyData];
    [dict setValue:pCtx.postData forKey:API_NotifyParams];
    
    // save cache
    if ([self isCacheable:pCtx.action])
    {
        NSDate *date = [[NSDate alloc] init];
        NSArray *arrCache = @[[NSNumber numberWithInteger:[date timeIntervalSince1970]], [dict mutableCopy]];
        [_requestCache setValue:arrCache forKey:pCtx.url];
        
        arrCache = nil;
        date = nil;
    }
    
    [self notifyData:dict];
    
	dict = nil;
}

- (void)notifyData:(NSMutableDictionary *)dict
{
    [[NSNotificationCenter defaultCenter] postNotificationName:API_NotifyName object:nil userInfo:dict];
    
    NSLog(@"%@",dict);
}

- (BOOL)isCacheable:(EnumRequestAPI)action
{
    BOOL bCache = NO;
    
    switch (action)
    {
        case Request_ParkingArea:
            bCache = YES;
            break;
       
            
        default:
            bCache = NO;
            break;
    }
    
    return bCache;
}

@end
