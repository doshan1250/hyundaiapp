//
//  NewsListController.h
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/9.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsListController : HyundaiViewController
-(void)reloadData;
-(void)setContent:(NSArray *)datas;
@end
