//
//  VideoViewCell.m
//  HyundaiApp
//
//  Created by RHZ Webber on 2015/9/28.
//  Copyright © 2015年 Hyundai. All rights reserved.
//

#import "VideoViewCell.h"

@implementation VideoViewCell

- (void)showItem2:(BOOL)show
{
    [self.pic2 setHidden:!show];
    [self.title2 setHidden:!show];
    [self.date2 setHidden:!show];
    [self.bg2 setHidden:!show];
    [self.player2 setHidden:!show];
    [self.btn2 setHidden:!show];
}

- (IBAction)clickItem1:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickVideo:)])
        [self.delegate clickVideo:self.tag1];
}


- (IBAction)clickItem2:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickVideo:)])
        [self.delegate clickVideo:self.tag2];
}

@end
