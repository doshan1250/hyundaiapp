//
//  ICommunication.h
//  CodeEnv
//
//  Created by Webber on 12/9/9.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

@class CommunicationContext;

@protocol ICommunication
- (void)handleCommunicationResult:(CommunicationContext *)pCtx;
@end

@protocol ICommunicationReportDelegate <NSObject>
- (void)onRequestReport:(id)sender status:(BOOL)bBeginRequest;
@end