//
//  FansController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "FansController.h"

@interface FansController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *_fbWeb;
    NSString * _link;
    NSString * _viewTitle;
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progress;

@end

@implementation FansController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andLink:(NSString *)link andtitle:(NSString *)viewTitle{
    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _link=link;
        _viewTitle=viewTitle;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"粉絲團";
    if(_viewTitle!=nil && ![_viewTitle isEqualToString:@""]){
        _titleView.text=_viewTitle;
    }
    
    [self.progress startAnimating];
    if(!_link){
        _link=@"https://www.facebook.com/hyundai.tw";
    }
    
    [_fbWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_link]]];
    [_fbWeb setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [currentApp.dataMgr delObserverData:self];
}

#pragma mark - webviewdeleatge
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{//覆寫鏈結處理
    NSString *target = [[request URL] absoluteString];
    return  [self loadWithRequest:target];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Disable 長按(右鍵)功能, WebView Default 會顯示 copy/paste 等預設功能
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    //source: http://blog.techno-barje.fr/post/2010/10/04/UIWebView-secrets-part1-memory-leaks-on-xmlhttprequest/
	[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [self.progress stopAnimating];
    [self.progress removeFromSuperview];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

@end
