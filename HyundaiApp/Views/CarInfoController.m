//
//  CarInfoController.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/11.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "CarInfoController.h"

@interface CarInfoController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *_ciWeb;
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progress;

@end

@implementation CarInfoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _titleView.text = @"車款資訊";
    [self.progress startAnimating];
    [currentApp.dataMgr addObserverData:self callback:@"onDataReady:"];
    [currentApp.dataMgr fireRequest:Request_CarInfo postData:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [currentApp.dataMgr delObserverData:self];
}


#pragma mark - Data Notify
- (void)onDataReady:(NSNotification *)pNotify
{
    NSDictionary *notify = [pNotify userInfo];
    
    if ([[notify objectForKey:API_NotifyKey] intValue] == Request_CarInfo)
    {
        NSDictionary *data = [notify objectForKey:API_NotifyData];
        NSString *carInfoURL = [data objectForKey:@"carurl"];
        if (carInfoURL)
            [_ciWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:carInfoURL]]];
        _ciWeb.delegate=self;
    }
}

#pragma mark - webviewdeleatge
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Disable 長按(右鍵)功能, WebView Default 會顯示 copy/paste 等預設功能
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];

    //source: http://blog.techno-barje.fr/post/2010/10/04/UIWebView-secrets-part1-memory-leaks-on-xmlhttprequest/
	[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [self.progress stopAnimating];
    [self.progress removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

@end
