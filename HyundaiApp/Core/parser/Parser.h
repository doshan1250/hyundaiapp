//
//  Parser.h
//  CodeEnv
//
//  Created by Webber Chuang on 12/1/7.
//  Copyright (c) 2012年 柏欣資訊有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject
{
	NSString *errCode;
	NSString *errMsg;
	NSDictionary *dictRes;
	BOOL errParser;
}

@property (nonatomic, assign) BOOL errParser;
@property (nonatomic, strong) NSString *errCode;
@property (nonatomic, strong) NSString *errMsg;
@property (nonatomic, strong) NSDictionary *dictRes;

+ (id)nodeValue:(NSDictionary *)node ofPath:(NSString *)path;
+ (NSMutableArray *) arrValue:(NSDictionary *)node ofPath:(NSString *)path;
+ (NSDictionary *) dictValue:(NSDictionary *)node ofPath:(NSString *)path;
+ (NSString *) stringValue:(NSDictionary *)node ofPath:(NSString *)path;
+ (int) intValue:(NSDictionary *)node ofPath:(NSString *)path;

- (id)initWithData:(NSData *)data;
- (id)initWithString:(NSString *)data;
- (void)checkError:(NSError *)parseError;

- (id)nodeOfPath:(NSString *)path;
- (NSMutableArray *)arrOfPath:(NSString *)path;
- (NSDictionary *)dictOfPath:(NSString *)path;
- (NSString *)stringOfPath:(NSString *)path;
- (int)intOfPath:(NSString *)path;

+ (NSDictionary *)detectDictionaryData:(id)data dataType:(Class)aClass;
+ (NSArray *)detectArrayData:(id)data dataType:(Class)aClass;
@end
