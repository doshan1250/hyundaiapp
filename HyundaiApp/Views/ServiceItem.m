//
//  ServiceItem.m
//  HyundaiApp
//
//  Created by Webber Chuang on 13/11/12.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "ServiceItem.h"

@interface ServiceItem ()
{
    id<ServiceItemDelegate> _delegate;
}

@property (nonatomic, strong) IBOutlet UILabel *title1;
@property (nonatomic, strong) IBOutlet UIButton *btn1;
@property (nonatomic, strong) IBOutlet UILabel *dist1;
@property (strong, nonatomic) IBOutlet UILabel *subTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)clickButton:(id)sender;

@end

@implementation ServiceItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


- (void)dealloc
{
    _delegate=nil;
}

#pragma mark - Public API
- (void)setDelegate:(id<ServiceItemDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setTitle:(NSString *)titleString andSubTitle:(NSString *)subTitleStirng andDis:(NSString *)disString tag:(int)tag{
    if(titleString)
        [self.title1 setText:titleString];
    if(subTitleStirng)
        [self.subTitle setText:subTitleStirng];
    [self.btn1 setTag:tag];
    if(disString)
        [self.dist1 setText:disString];
}

-(void)setIconImage:(UIImage *)image{
    [self.imageView setImage:image];
}

- (IBAction)clickButton:(UIButton *)sender
{
    if (_delegate)
    {
        if ([_delegate respondsToSelector:@selector(clickItemWithIndex:)])
        {
            [_delegate clickItemWithIndex:[sender tag]];
        }
    }
}

@end
