//
//  MapForGoogleAPIViewController.m
//  hyundaiapp
//
//  Created by ShuHsien Lin on 2013/12/13.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import "MapForGoogleAPIViewController.h"
#import "MapViewTools.h"
#import "NavMapInfoCell.h"

@interface MapForGoogleAPIViewController ()<UITableViewDelegate, UITableViewDataSource,MKMapViewDelegate,ConnctionDelegate>{
    
    UIBarButtonItem *navSpace;
    MapViewTools *mapViewTool;
    CLLocationCoordinate2D _origin;
    CLLocationCoordinate2D _dest;
    
    MKMapView *mapView;
    NSInteger _type;
    NSArray *nav_info_Array;
    CGFloat lockView;
    CGFloat touchMoveY;
    int scroll_mode;
    int _lastPosition;
}

@property (strong, nonatomic) IBOutlet UIView *nav_info_view;
@property (strong, nonatomic) IBOutlet UITableView *nav_info_table;
//@property (strong, nonatomic) IBOutlet UIButton *nav_info_button;
@property (strong, nonatomic) IBOutlet UIImageView *nav_info_img;
@property (strong, nonatomic) IBOutlet UIView *nav_info_ctrl;

@end

@implementation MapForGoogleAPIViewController

@synthesize routeLineView;
//@synthesize lines;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _titleView.text = @"路徑規劃";
    self.navigationItem.rightBarButtonItem = nil;
    
    mapViewTool=[MapViewTools new];
    mapView= [mapViewTool getMapView];
    mapView.frame=self.view.frame;
    mapView.delegate=self;
    [self.view addSubview:mapView];
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getDirectionsData:_origin andDestination:_dest];
    [mapViewTool moveToPostionWithLat:_origin.latitude andLng:_origin.longitude];
    [self.view bringSubviewToFront:self.nav_info_view];
    [self nav_info_hide];
}


-(void)setOrigin:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest{
    _origin=origin;
    _dest=dest;
}

-(void)dealloc{
    routeLineView=nil;
    //NSLog(@"地圖畫面離開");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)getDirectionsData:(CLLocationCoordinate2D)origin andDestination:(CLLocationCoordinate2D)dest{
    ConnctionTool *mConnctionTool=[ConnctionTool new];
    
    [mConnctionTool setDelegate:self];
    /*&language=zh-TW"*/
    NSString *url=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%g,%g&destination=%g,%g&sensor=false&mode=driving&language=zh-TW",origin.latitude,origin.longitude,dest.latitude,dest.longitude];
    [mConnctionTool beginDownload:url];
    
}

-(void)getDataSuccess:(NSMutableData *)data{
    /*ref: http://blog.csdn.net/dqjyong/article/details/7861218*/
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    //NSLog(@"tag  data: %@",dic);
    NSArray *array=[dic objectForKey:@"routes"];//獲得一個route
    
    if(array.count==0){
        
        UIAlertView *alertView;
        alertView=[[UIAlertView alloc]initWithTitle:nil message:@"無法獲取路徑規劃！" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
        [alertView show];
        
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    //NSLog(@"tag routes:%@",array);
    dic=[array objectAtIndex:0];
    array=[dic objectForKey:@"legs"];
    dic=[array objectAtIndex:0];
    array=[dic objectForKey:@"steps"];
    NSMutableArray *summary_arr=[NSMutableArray new];
    for(int n=0;n<array.count;n++){
        dic=[array objectAtIndex:n];
        NSString *string=[dic objectForKey:@"html_instructions"];
        string=[self flattenHTML:string trimWhiteSpace:YES];
        [summary_arr addObject:string];
        dic=[dic objectForKey:@"polyline"];
        MKPolyline *polyLine=[self polylineWithEncodedString:[dic objectForKey:@"points"]];
        [[mapViewTool getMapView] addOverlay:polyLine];
        
    }
    
    
    [mapViewTool putPoint:@"" andSubTitle:@"" andLat:_dest.latitude andLng:_dest.longitude];
    [mapViewTool putUserPoint:_origin andTitle:@"now" andSubTitle:@""];
    
    nav_info_Array=[[NSArray alloc]initWithArray:summary_arr];
    self.nav_info_table.dataSource=self;
    self.nav_info_table.delegate=self;
    [self.nav_info_table reloadData];
}

#pragma mark methods for MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    if ([[annotation title] isEqualToString:@"now"]) {
        MKAnnotationView* pinView =
        (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:@"userAnnotationViewID" ];
        if(!pinView){
            MKAnnotationView* customPinView =
            [[MKAnnotationView alloc] initWithAnnotation:annotation
                                         reuseIdentifier:@"userAnnotationViewID"];
            [customPinView setImage:[UIImage imageNamed:@"icon_my_location.png"]];
            customPinView.canShowCallout = NO;
            return customPinView;
        }else{
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    
    MKAnnotationView* pinView =
    (MKAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (!pinView && ![annotation isKindOfClass:[MKUserLocation class]])
    {
        // if an existing pin view was not available, create one
        //這裡針對大頭針做各種設定
        MKAnnotationView* customPinView =
        [[MKAnnotationView alloc] initWithAnnotation:annotation
                                     reuseIdentifier:AnnotationViewID];
        //設置圖釘圖案
        UIImage *image;
        if(_type==0){
            image=[UIImage imageNamed:@"icon_map_p.png"];
        }else if(_type==1){
            image=[UIImage imageNamed:@"icon_map_g.png"];
        }else if(_type==2){
            image=[UIImage imageNamed:@"map_icon1.png"];
        }else{
            image=[UIImage imageNamed:@"map_icon2.png"];
        }
        customPinView.image=image;
        
        //顯示視窗
        customPinView.canShowCallout = NO;
        
        return customPinView;
    }
    else
    {
        pinView.annotation = annotation;
    }
    return pinView;
}

//去除ＨＴＭＬ
- (NSString *)flattenHTML:(NSString *)html trimWhiteSpace:(BOOL)trim {
    NSScanner *theScanner = [NSScanner scannerWithString:html];
    NSString *text = nil;
    
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        html = [html stringByReplacingOccurrencesOfString:
                [ NSString stringWithFormat:@"%@>", text]
                                               withString:@""];
    }
    
    // trim off whitespace
    return trim ? [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : html;
}


- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id )overlay
{
    MKOverlayView* overlayView = nil;
    
    self.routeLineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    self.routeLineView.fillColor = [[UIColor alloc]initWithRed:0 green:0 blue:1 alpha:0.3];
    self.routeLineView.strokeColor= [[UIColor alloc]initWithRed:0 green:0 blue:1 alpha:0.8];
    self.routeLineView.lineWidth = 5;
    
    
    overlayView =self.routeLineView;
    
    
    return overlayView;
    
}

-(void)getDataFail:(NSInteger )errorMsg{
    
}
-(void)getErrorMsg:(NSError *)error{
    
}
-(void)setType:(NSInteger)type{
    _type=type;
    //NSLog(@"type: %d",_type);
    
    
}

#pragma mark - UITouch
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"touchesBegan");
    
    UITouch *touch = [touches anyObject];
    
    if (touch.view == self.nav_info_ctrl)
    {
        
        //NSLog(@"touchesBegan1");
        CGPoint location = [touch locationInView:self.view];
        lockView=location.y-self.nav_info_view.frame.origin.y;
        touchMoveY=self.nav_info_view.frame.origin.y;
        [self.nav_info_table setBackgroundColor:[[UIColor alloc] initWithWhite:1 alpha:1]];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(lockView>-1&&touch.view == self.nav_info_ctrl){
        //NSLog(@"touchesMoved");
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:self.view];
        touchMoveY=location.y;
        if(touchMoveY>0&&touchMoveY<(self.view.frame.size.height-self.nav_info_ctrl.frame.size.height)){
            [self.nav_info_view setFrame:CGRectMake(0, touchMoveY-lockView,self.nav_info_view.frame.size.width , self.nav_info_view.frame.size.height)];
        }
    }//end if
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    
    UITouch *touch = [touches anyObject];
    
    if (touch.view == self.nav_info_ctrl)
    {
        //NSLog(@"touchesEnded, %f",touchMoveY);
        if(touchMoveY<(self.view.frame.size.height/3)){
            //NSLog(@"self.view.frame.origin.y: %f",self.nav_info_view.frame.origin.y);
            [self nav_info_show];
        }else{
            
            [self nav_info_hide];
        }//end if else
    }
}



-(void)nav_info_hide{
    CGRect frame=self.nav_info_view.frame;
    CGFloat hh=self.view.frame.size.height*7/10;
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.nav_info_view setFrame:CGRectMake(0,hh-self.nav_info_ctrl.frame.size.height , frame.size.width, frame.size.height)];
                         
                     } completion:^(BOOL finish){
                         scroll_mode=0;
                         [self.nav_info_table setBackgroundColor:[[UIColor alloc] initWithWhite:1 alpha:0.7]];
                     }];
    [UIView commitAnimations];
}
-(void)nav_info_show{
    CGRect frame=self.nav_info_view.frame;
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.nav_info_view setFrame:CGRectMake(0,0 , frame.size.width, frame.size.height)];
                     } completion:^(BOOL finish){
                         [self.nav_info_table setFrame:CGRectMake(self.nav_info_table.frame.origin.x, self.nav_info_table.frame.origin.y, self.nav_info_table.frame.size.width, self.view.frame.size.height-self.nav_info_ctrl.frame.size.height)];
                         [self.nav_info_table setBackgroundColor:[[UIColor alloc] initWithWhite:1 alpha:1]];
                         scroll_mode=1;
                     }];
    [UIView commitAnimations];
}


#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"number: %d",[nav_info_Array count]);
    return [nav_info_Array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"NavMapInfoCell";
    NavMapInfoCell *cell = (NavMapInfoCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NavMapInfoCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (NavMapInfoCell *) currentObject;
                break;
            }
        }
    }
    [cell.lb_title setText:[nav_info_Array objectAtIndex:indexPath.row]];
    [cell.lb_number setText:[NSString stringWithFormat:@"%d.",(indexPath.row+1)]];
    
	return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    int currentPostion = scrollView.contentOffset.y;
    if (currentPostion - _lastPosition > 20) {
        _lastPosition = currentPostion;
        //NSLog(@"ScrollUp now");
        if(scroll_mode!=1){
            //準備展開
            [self nav_info_show];
        }
    }
    else if (_lastPosition - currentPostion > 20)
    {
        _lastPosition = currentPostion;
        //NSLog(@"ScrollDown now");
    }
}

#pragma mark -解析google 折線
- (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:coordIdx];
    free(coords);
    
    return polyline;
}

@end
