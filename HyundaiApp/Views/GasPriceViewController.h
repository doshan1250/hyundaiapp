//
//  GasPriceViewController.h
//  HyundaiApp
//
//  Created by ShuHsien Lin on 2013/11/14.
//  Copyright (c) 2013年 LinkinEDGE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GasPriceViewController : UIViewController
//設置牌價
-(void)setGasPrice:(NSString *)gas92 andGas95:(NSString *)gas95 andGas98:(NSString *)gas98 andGasSuper:(NSString *)gasSuper;
-(void)setGAsPrice:(NSString *)gasPrice withIndex:(NSInteger)row;
-(void)stopProgress;
@end
